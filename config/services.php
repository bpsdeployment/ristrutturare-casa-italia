<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

        /*
    |--------------------------------------------------------------------------
    | Third Party Authentication Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party authentication services such
    | as Google and Facebook. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */
    'google' => [
        'client_id' => '598008051961-8n46jm5vtuoio7sn7q3bn2i2n4funqtu.apps.googleusercontent.com',
        'client_secret' => 'sAgV1JsYeiaL08iJHNyhQ6gP',
        'redirect' => '/auth/callback/google',
    ],

    'facebook' => [
        'client_id' => '389799599194252',
        'client_secret' => '555ad5db159a8a394282f059f20a2e31',
        'redirect' => 'http://localhost:8000/auth/callback/facebook',
    ],

];

