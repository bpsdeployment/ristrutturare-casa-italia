$('#addToCartForm').on('submit', function(e) {
    e.preventDefault();
    let id = e.target.name;
    let qty = $('#quantity_field').val();
    $.ajax({
        url: "{{ route('addToCart') }}",
        type: "POST",
        data: {
            "_token": "{{ csrf_token() }}",
            id: id,
            qty: qty,
        },
        success: function(response) {
            if ($("#cart_title").text() == "Il tuo preventivo è vuoto") {
                $("#cart_title").html(
                    "<h3 id= 'cart_title'>Il tuo preventivo (<span id=\"SidebarCounter\"></span>)</h3>"
                )
                $("#sidebar_button_cart").html(
                    "<a href={{ route('cart') }} class=\"btn btn-primary\">Riepilogo</a>"
                )
            }
            if (response.new_item) {
                $("#SidebarCart").append(response.html);
            } else {
                $("#qty_" + response.id).html(response.qty);
            }
            $("#SidebarCounter").html(response.cart_count);
            $("#CartCounter").html(response.cart_count);
            toastr.success('Aggiunto al preventivo!');
        },
        error: function(response) {
            $('#nameErrorMsg').text(response.responseJSON.errors.id);
            $('#emailErrorMsg').text(response.responseJSON.errors.qty);
            toastr.error('Prodotto non aggiunto al preventivo.');
        },
    });
});
