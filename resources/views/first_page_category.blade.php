@php
    $name = $category->name;
    $id = $category->id;
    $path = $category->thumbnail_path;
@endphp

<div class="col-lg-4 col-sm-12">
    <h4 class="text-center">{{$name}}</h4>
    <img src="{{ asset($path) }}"
        class="img-fluid" alt="">
    {!! Form::open(['method' => 'POST', 'route' => 'showNewFirstPageCategory']) !!}
    {!! Form::select('new_cat_id', $categories, ['class' => 'form-control', 'placeholder' => $id]) !!}
    <input name="old_cat_id" value="{{$id}}" hidden>
    <button type="submit">Conferma</button>
    {!! Form::close() !!}
</div>

