<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>Ristrutturare Casa Italia  - Admin panel</title>
    <!-- plugins:css -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Inserimento nuovo coupon</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'storeCoupon']) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Nome
                                        {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'coupon_name']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('coupon_name', null, ['class' => 'form-control', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Data di scadenza
                                        {!! Form::label('Data di scadenza', null, ['class' => 'sr-only', 'for' => 'due_date']) !!}
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input class="date form-control" name="due_date" type="text" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Valore dello sconto (%)
                                        {!! Form::label('Sconto', null, ['class' => 'sr-only', 'for' => 'discount']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::number('discount', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Inserire 0 per creare un coupon solo per la spedizione gratuita',  'min' => '0', 'max' => '100']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Spedizione gratuita?
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::select('free_shipping', ['0' => 'SI', '1' => 'NO'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => '0']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Codice (8 caratteri)
                                        {!! Form::label('Codice', null, ['class' => 'sr-only', 'for' => 'coupon_code']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('coupon_code', null, ['class' => 'form-control', 'placeholder' => 'Inserisci il codice che verà usato dagli utenti come coupon',  'minlength'=>"8", 'maxlength'=>"8"]) !!}
                                    </div>
                                </div>
                            </div>
                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary">Inserisci</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $('.date').datepicker({
            format: 'mm-dd-yyyy'
        });
    </script>
    <!-- End custom js for this page -->
</body>

</html>
