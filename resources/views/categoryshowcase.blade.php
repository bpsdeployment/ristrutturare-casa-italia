@php
    $path = $category->thumbnail_path;
    $name = $category->name;
@endphp

<div class="item col-lg-4">
    <!--<p>MS-RCI-94</p>-->
    <a class="category-box" href={{ route('products', ['category'=>$name]) }}>
        <img src={{$path}} alt="" class="img-responsive">
        <div class="category-text">
            <h4 class="text-uppercase">{{$name}}</h4>
            <!--<p>Lorem ipsum dolor</p>-->
        </div>
    </a>
    <!--/category-box-->
</div>
<!--/item-->
