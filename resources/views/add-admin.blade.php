<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Aggiungi nuovo admin</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </div>
                        @endif
                        @if (Session::has('message'))
                        <div class="alert alert-info" role="alert">
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        <div class="card-body">
                            {!! Form::open(['method' => 'POST', 'route' => 'store-new-admin']) !!}
                            <div class="row">

                                <div class="col-lg-8">

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Nome
                                                {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'name']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'true']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Cognome
                                                {!! Form::label('Cognome', null, ['class' => 'sr-only', 'for' => 'surname']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('surname', null, ['class' => 'form-control', 'required' => 'true']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Email
                                                {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'email']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'true']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Telefono
                                                {!! Form::label('Telefono', null, ['class' => 'sr-only', 'for' => 'phone']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'false']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Indirizzo
                                                {!! Form::label('Indirizzo', null, ['class' => 'sr-only', 'for' => 'addr']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('addr', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Codice fiscale
                                                {!! Form::label('Codice fiscale', null, ['class' => 'sr-only', 'for' => 'codice_fiscale']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('codice_fiscale', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Partita IVA
                                                {!! Form::label('Partita IVA', null, ['class' => 'sr-only', 'for' => 'p_iva']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('p_iva', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                N° iscrizione albo
                                                {!! Form::label('Numero iscrizione albo', null, ['class' => 'sr-only', 'for' => 'n_iscr_albo']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::text('n_iscr_albo', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                Commissione percentuale
                                                {!! Form::label('Percentuale commissione di vendita', null, ['class' => 'sr-only', 'for' => 'selling_commission_perc']) !!}
                                            </div>
                                            <div class="col-lg-9">
                                                {!! Form::number('selling_commission_perc', 0, ['class' => 'form-control', 'required' => 'false']) !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-1"></div><!-- spacing -->

                                <div class="col-lg-3">
                                    <div class="row">
                                        <div class="card mb-3 p-0">
                                            <div class="card-header">
                                                <strong>Prodotti</strong>
                                                <div style="float:right;display:none;"><a href="#">ALL</a></div>
                                            </div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">Lettura<div style="float:right">{{ Form::Checkbox('r_products_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Modifica<div style="float:right">{{ Form::Checkbox('u_products_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Eliminazione<div style="float:right">{{ Form::Checkbox('d_products_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Creazione<div style="float:right">{{ Form::Checkbox('c_products_auth', null, true)}}</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="card mb-3 p-0">
                                            <div class="card-header">
                                                <strong>Categorie</strong>
                                            </div>
                                            <ul class="list-group">
                                                <li class="list-group-item">Lettura<div style="float:right">{{ Form::Checkbox('r_categories_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Modifica<div style="float:right">{{ Form::Checkbox('u_categories_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Eliminazione<div style="float:right">{{ Form::Checkbox('d_categories_auth', null, true)}}</div>
                                                </li>
                                                <li class="list-group-item">Creazione<div style="float:right">{{ Form::Checkbox('c_categories_auth', null, true)}}</div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="card mb-3 p-0">
                                            <div class="card-header">
                                                <strong>Prima pagina</strong>
                                            </div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">Modifica<div style="float:right">{{ Form::Checkbox('u_first_page_auth', null, true)}}</div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-lg btn-primary" style="margin-bottom:10px;">Aggiungi</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>


    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>