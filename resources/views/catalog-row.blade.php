@if ($cat1 != 'none')
    <div class="row">
        <div class="col-sm-3">
            <ul class="mega-menu-list list-unstyled nav">
                <li><a href={{ route('products', ['category' => $cat1]) }}>{{ $cat1 }}</a></li>
            </ul>
        </div>
        @if ($cat2 != 'none')
            <div class="col-sm-3">
                <ul class="mega-menu-list list-unstyled nav">
                    <li><a href={{ route('products', ['category' => $cat2]) }}>{{ $cat2 }}</a></li>
                </ul>
            </div>
        @endif
        @if ($cat3 != 'none')
            <div class="col-sm-3">
                <ul class="mega-menu-list list-unstyled nav">
                    <li><a href={{ route('products', ['category' => $cat3]) }}>{{ $cat3 }}</a></li>
                </ul>
            </div>
        @endif
        @if ($cat4 != 'none')
            <div class="col-sm-3">
                <ul class="mega-menu-list list-unstyled nav">
                    <li><a href={{ route('products', ['category' => $cat4]) }}>{{ $cat4 }}</a></li>
                </ul>
            </div>
        @endif
    </div>
@endif
