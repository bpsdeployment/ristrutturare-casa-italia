<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ristrutturare Casa Italia  - Admin panel</title>
        <!-- plugins:css -->
        <link href={{asset('plugins/font-awesome/css/font-awesome.min.css')}}rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href={{asset('js/vendor.bundle.base.js')}}>
        <!-- endinject -->
        <!-- inject:css -->
        <link rel="stylesheet" href={{asset('css/adminstyle.css')}}>
        <!-- endinject -->
        <link rel="shortcut icon" href={{asset('images/Favicon_RCI.png')}} />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      </head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="page-header">
                        <h3 class="page-title">Inserimento nuovo prodotto/materiale</h3>
                    </div>
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                                @endif
                                @if(count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error )
                                    <li>{{$error}}</li>
                                    @endforeach
                                </div>
                                @endif
                                {!! Form::open(['method' => 'POST', 'route' => 'storeCategory', 'files' => 'true']) !!}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            Nome
                                            {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'product_name']) !!}
                                        </div>
                                        <div class="col-lg-7">
                                            {!! Form::text('category_name', null, ['class' => 'form-control', 'required' => 'true']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3">Copertina
                                            {!! Form::label('Immagini', null, ['class' => 'sr-only', 'for' => 'product_images']) !!}
                                        </div>
                                        <div class="col-lg-7">
                                            <input required type="file" name="images[]">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3">Descrizione
                                            {!! Form::label('Altre informazioni', null, ['class' => 'sr-only', 'for' => 'product_description']) !!}
                                        </div>
                                        <div class="col-lg-7">
                                            {!! Form::textarea('category_description',null,['class'=>'form-control', 'rows' => 4, 'placeholder' => 'Informazioni aggiuntive relative a questa particolare categoria (è possibile lasciare vuoto e modificare in futuro)']) !!}
                                        </div>
                                    </div>
                                </div>
                                <!--//form-group-->
                                <button type="submit" class="btn btn-lg btn-primary">Inserisci</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                @include('adminfooter')
            </div>
            <!-- content-wrapper ends -->

            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{asset('js/vendor.bundle.base.js')}}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{asset('js/off-canvas.js')}}></script>
    <script src={{asset('js/hoverable-collapse.js')}}></script>
    <script src={{asset('js/misc.js')}}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{asset('js/file-upload.js')}}></script>

    <!-- End custom js for this page -->
</body>

</html>
