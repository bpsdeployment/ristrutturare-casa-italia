<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="container bootstrap snippet">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="space-30"></div>
                            <!--Message-->
                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            @if (Session::has('errormessage'))
                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('errormessage') }}
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif

                        </div>
                        <div class="col-sm-10"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3">
                            <!--left col-->

                            <div class="text-center">
                                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                                    class="avatar img-circle img-thumbnail" alt="avatar">
                                <h3>{{ Auth::user()->name }} {{ Auth::user()->surname }}</h3>
                            </div><br>


                            @if (Session::get('is_site_master') || App\Http\Controllers\AuthorizationController::HasAnyAuth(Auth()->user()->id))
                                <div class="panel panel-default">
                                    <div class="panel-heading">Entra nel pannello admin</div>
                                    <div class="panel-body"><a href={{ route('admin.page') }}>Admin Page</a></div>
                                </div>
                            @endif


                            <div class="panel panel-default">
                                <div class="panel-body"><a href={{ route('order.history') }}>Storico ordini</a>
                                </div>
                            </div>


                            <!--
                            <ul class="list-group">
                                <li class="list-group-item text-muted">Attività <i class="fa fa-dashboard fa-1x"></i>
                                </li>
                                <li class="list-group-item text-right"><a href="#" >Storico ordini</a></li>
                            </ul>
                            <div class="panel panel-default">
                                <div class="panel-heading">Social Media</div>
                                <div class="panel-body">
                                    <i class="fa fa-facebook fa-2x"></i> <i class="fa fa-github fa-2x"></i> <i
                                        class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i
                                        class="fa fa-google-plus fa-2x"></i>
                                </div>
                            </div>
                            -->
                        </div>
                        <!--/col-3-->
                        <div class="col-sm-9">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#info">Informazioni Generali</a>
                                </li>
                                <li><a data-toggle="tab" href="#password">Password</a></li>
                                <li><a data-toggle="tab" href="#privacy">Privacy</a></li>
                            </ul>


                            <div class="tab-content">
                                <div class="tab-pane active" id="info">
                                    <hr>
                                    <!-- laravel collective -->
                                    {!! Form::open(['method' => 'POST', 'route' => 'updateUser']) !!}
                                    @include('address-input')
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <br>
                                                <br>
                                                <button class="btn btn-lg btn-primary" type="submit"><i
                                                        class="glyphicon glyphicon-ok-sign"></i>Salva</button>
                                                <button class="btn btn-lg" type="reset"><i
                                                        class="glyphicon glyphicon-repeat"></i>Annulla</button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                    <hr>

                                </div>
                                <!--/tab-pane-->

                                <div class="tab-pane" id="password">
                                    <hr>
                                    @if (Auth::user()->password_hash == 'null')
                                        @include('updatepass_nooldpassword')
                                    @else
                                        @include('updatepass')
                                    @endif
                                    <hr>
                                </div>

                                <div class="tab-pane" id="privacy">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="space-20"></div>
                                                <p> Per eliminare il profilo clicca il bottone sottostante.<br>
                                                    Verranno cancellati tutti i dati in nostro possesso generati dal tuo
                                                    account.<br>
                                                    Si ricorda che l'azione è irreversibile.
                                                </p>
                                                <br>
                                                {!! Form::open(['method' => 'POST', 'route' => 'deleteUser']) !!}
                                                <button class="btn btn-lg btn-danger"
                                                    onclick="return confirm('Sei sicuro di voler eliminare il tuo profilo?')"
                                                    type="submit"><i class="glyphicon glyphicon-remove"></i> Rimuovi
                                                    Profilo</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-pane-->
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
