<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Tabella prodotti</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="container">
                            @if (Session::has('message'))
                                    <div class="alert alert-info" role="alert">
                                        <div class="row">
                                            <div class="col-lg-4"> {{ Session::get('message') }} </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <h5>Inserisci i dati per filtrare:</h5>  
                                <input class="form-control" id="myInput" type="text" placeholder="Filtra per..">
                                <br>
                                <table id="datatable" class="table table-bordered yajra-datatable">
                                    <thead>
                                        <tr>
                                            <th>Seleziona</th>
                                            <th>id</th>
                                            <th>Nome</th>
                                            <th>Categoria</th>
                                            <th>Prezzo di acquisto(€)</th>
                                            <th>Prezzo di listino(€)</th>
                                        </tr>
                                    </thead>
                                        <tbody id="tableBody">
                                            @foreach($products as $product)
                                                @include('bulk-edit-product-row', 
                                                ['id'=>$product->id, 'name'=>$product->name , 'category'=>$product->type, 'buyPrice'=>$product->buy_price, 'listingPrice'=>$product->listing_price ])
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br>
                                    <!--
                                <button type="submit" id="selectAll" class="btn btn-lg btn-primary">Seleziona Tutti</button>
                                -->
                            </div>
                    <div class="container">
                        <hr>
                        <h9>%</h9>  
                        <input type="number" id="increment" name="tentacles" min="-10" max="10" value= "0">
                        <h9>+ Incrementa</h9>  
                        <h9>- Decrementa</h9>  
                            <br>
                            <br>
                            <button type="submit" id="btnIncrement" class="btn btn-lg btn-primary">Incrementa/Decrementa</button>
                    </div>
                </div>
            </div>
        </div>
        @include('adminfooter')
    </div>
    <!-- content-wrapper ends -->

    <!-- partial -->
    <!-- main-panel ends -->
    <!-- page-body-wrapper ends -->

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- End custom js for this page -->
</body>
    <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#datatable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
    </script>

    <script>
         $( "#btnIncrement" ).click(function() {
            var selected = [];
            var mult_value = $('#increment').val();
            $('#tableBody input:checked').each(function() {
                selected.push($(this).attr('name'));
            });
            if(mult_value != 0 && selected[0] != null){
                let text = "Sei sicuro?";
                if (confirm(text) == true) {
                    $.ajax({
                            type: 'POST',
                            url: "{{ route('bulk-edit-product-price') }}",
                            data: {
                                products_id: selected,
                                multiplier: mult_value, 
                                "_token": "{{csrf_token() }}",
                            },
                            success: function (data) {
                            }
                    });         
                }
                alert('Aggiornato con successo');
            }else{
                if(mult_value == 0){
                    alert('% di incremento è uguale a 0');
                }else{
                    alert('Seleziona almeno un prodotto');
                }
            }
            

        });
    </script>

<!--
    <script>
        //Selezionare tutte le righe che non hanno style="display: none;"
        //Attenzione se una righe viene nascosta e poi rivisualizzata rimane il campo 'style'
        /*
        $('#selectAll').click(function() {
            $('#tableBody').each(function() {
                if($(this).attr('style') == "display: none")
                {
                    alert($(this).find('#tableBody').val());
                }
                else
                    $(".custom-control-input").prop("checked", true);
            });
        });
        */
        /*
        if($(this).is(":visible"))
            {
                $(".custom-control-input").prop("checked", true);
            }
        if($(this).attr('style') == "display: none;")
            {
                $(".custom-control-input").prop("checked", true);
            }
        */
    </script> 
    -->
</html>
