<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 margin-b-30">
                <h4>Chi siamo</h4>
                <p>
                    {{$frontpage_texts->footer_text}}
                </p>
            </div>
            <div class="col-md-3 margin-b-30"></div>

            <div class="col-md-3 margin-b-30">
                <ul class="list-unstyled link-list">
                    <li><a href={{$frontpage_texts->t_t_link}}>Termini & Condizioni</a></li>
                    <li><a href={{$frontpage_texts->cookie_policy_link}}>Cookie policy</a></li>
                    <li><a href={{$frontpage_texts->privacy_info_link}}>Informativa Privacy</a></li>
                </ul>
            </div>
            <div class="col-md-3 margin-b-30">
                <p class="lead"><small>Supporto: </small> <br>{{$frontpage_texts->support_phone}}</p>
                <p class="lead">{{$frontpage_texts->support_mail}}</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="container text-center">
        <span>&copy; Copyright <?php echo date('Y'); ?>. Tutti i diritti riservati.</span> <br>
        <span>Powered by BPS Deployment srl</span>
    </div>
</footer>
