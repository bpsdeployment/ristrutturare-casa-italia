<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 hidden-xs">
                                <!--
                                    MS-BPS-07062022
                                    <span>Placeholder text</span>
                                -->
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-inline pull-right">
                                    @if (Session::get('is_admin'))
                                        <li><a href={{ route('admin.page') }}><i class="material-icons">account_box</i>
                                                {{ Session::get('admin_email') }}</a></li>
                                        <li><a href={{ route('logout') }}><i class="material-icons">logout</i>Logout</a>
                                        </li>
                                    @else
                                        <li><a href={{ route('login') }}><i
                                                    class="material-icons">perm_identity</i>Login</a></li>
                                    @endif
                                    <!--
                                    MS-BPS-07062022
                                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><img
                                                src={{ asset('images/flags/italy.png') }} alt="">Italiano</a>
                                    -->
                                    <!--
                                    <ul class="lang-dropdown dropdown-menu">
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/spanish.png" alt="Spanish">Spanish</a></li>
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/italy.png" alt="Italian">Italian</a></li>
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/german.png" alt="German">German</a></li>
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/fr.png" alt="French">French</a></li>
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/jp.png" alt="Japanise">Japanese</a></li>
                                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/in.png" alt="Hindi">Hindi</a></li>
                                    </ul>
                                    -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end top bar-->
                <!-- Static navbar -->
                <nav class="navbar navbar-default navbar-static-top yamm sticky-header">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="" style="float: left;" href={{ route('index') }}><img
                                    src={{ asset('images/logo_header.png') }} alt=""></a>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container-fluid -->
                </nav>
                <!--page start-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 margin-b-60">
                            <div class="login-register-box">

                                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\AdminController@authenticate']) !!}
                                <div class="form-group">
                                    <i class="material-icons icon">email</i>
                                    {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'login-email']) !!}
                                    {!! Form::email('mail', null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Email',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                    ]) !!}
                                </div>
                                <!--//form-group-->
                                <div class="form-group">
                                    <i class="material-icons icon">lock</i>
                                    {!! Form::label('Password', null, ['class' => 'sr-only', 'for' => 'password']) !!}
                                    {!! Form::password('password', [
                                        'class' => 'form-control',
                                        'placeholder' => 'Password',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                    ]) !!}
                                </div>
                                <!--//form-group-->
                                <div class="form-group">
                                    {!! app('captcha')->display() !!}
                                </div>

                                <button type="submit" class="btn btn-lg btn-block btn-primary">Login</button>
                                {!! Form::close() !!}

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger" role="alert">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                @endif

                                @if (Session::has('message'))
                                    <div class="alert alert-info" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif

                                <div>
                                    <div style="margin-top: 2%;"><a href={{ route('password.request') }}>Password
                                            dimenticata?</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--page end-->
                </div>
            </div>
            <!--footer-->
            @include('public-footer')
            <!--/footer-->

        </div>
        <!--Common plugins-->
        @include('plugins')
</body>

</html>
