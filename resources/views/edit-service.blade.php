<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .img-wraps {
            position: relative;
            display: inline-block;

            font-size: 0;
        }

        .img-wraps .closes {
            position: absolute;
            top: 5px;
            right: 8px;
            z-index: 100;
            background-color: #FFF;
            padding: 4px 3px;

            color: #000;
            font-weight: bold;
            cursor: pointer;

            text-align: center;
            font-size: 22px;
            line-height: 10px;
            border-radius: 50%;
            border: 1px solid red;
        }

        .img-wraps:hover .closes {
            opacity: 1;
        }
    </style>

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica servizio</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'edit-service', 'files' => 'true']) !!}
                            {{ Form::hidden('id', $id) }}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Nome
                                        {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'product_name']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('product_name', $product_name, ['class' => 'form-control', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Categoria
                                        {!! Form::label('Categoria', null, ['class' => 'sr-only', 'for' => 'product_type']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::select('product_type', $categories_list, [
                                            'class' => 'form-control',
                                            'required' => 'true',
                                            'placeholder' => $product_type,
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Descrizione
                                        {!! Form::label('Descrizione', null, ['class' => 'sr-only', 'for' => 'product_description']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('product_description', $product_description, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                            'required' => 'true',
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Cancella immagini
                                    </div>
                                    <div class="col-lg-7">
                                        @if (count($images) > 0)
                                            @foreach ($images as $file)
                                                <div class="img-wraps">
                                                    <a class="closes"
                                                        href={{ route('deleteImage', [$folder_name, $file]) }}
                                                        style="display: inline;">&#215;</a>
                                                    <img class="img-responsive"
                                                        src={{ Thumbnail::src(public_path() . '/images/products_images/' . $folder_name . '/' . $file)->smartcrop(100, 100)->save()->url() }} />
                                                </div>
                                            @endforeach
                                        @else
                                            {{ 'Non sono presenti immagini per questo prodotto' }}
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Aggiungi immagini
                                        {!! Form::label('Immagini', null, ['class' => 'sr-only', 'for' => 'product_images']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="file" class="form-control-lg form-control" name="images[]"
                                            multiple>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Prezzo di acquisto (€)
                                        {!! Form::label('Prezzo di acquisto', null, ['class' => 'sr-only', 'for' => 'product_images']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::number('product_buy_price', $buy_price, ['class' => 'form-control', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Prezzo di listino (€)
                                        {!! Form::label('Prezzo di listino', null, ['class' => 'sr-only', 'for' => 'product_images']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::number('product_listing_price', $listing_price, ['class' => 'form-control', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Sconto massimo (%)
                                        {!! Form::label('Sconto massimo', null, ['class' => 'sr-only', 'for' => 'product_images']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::number('max_discount', $discount, [
                                            'class' => 'form-control',
                                            'required' => 'true',
                                            'min' => '0',
                                            'max' => '60',
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary">Aggiorna</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
