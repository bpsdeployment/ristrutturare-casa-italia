
<div class="row">
    <div class="form-group">
        <div class="col-xs-12">
            {!! Form::label('Indirizzo', null, ['for' => 'address_r1']) !!}
            {!! Form::text('address_r1', Auth::user()->address_r1, ['class' => 'form-control', 'placeholder' => 'Inserisci il tuo indirizzo', 'required' => 'true']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-xs-12">
            {!! Form::text('address_r2', Auth::user()->address_r2, ['class' => 'form-control', 'placeholder' => 'Scala, piano, interno, azienda (facoltativo)', 'required' => 'true']) !!}
        </div>
    </div>
</div>
<div class="space-20"></div>
<div class="row">
    <div class="form-group">
        <div class="col-xs-6">
            {!! Form::label('CAP', null, ['for' => 'CAP']) !!}
            {!! Form::text('CAP', Auth::user()->CAP, ['class' => 'form-control', 'placeholder' => 'Inserisci il tuo CAP', 'required' => 'true']) !!}
        </div>
        <div class="col-xs-6">
            {!! Form::label('Città', null, ['for' => 'city']) !!}
            {!! Form::text('city', Auth::user()->city, ['class' => 'form-control', 'placeholder' => 'Inserisci la tua città', 'required' => 'true']) !!}
        </div>
    </div>
</div>
<div class="space-20"></div>
<div class="row">
    <div class="form-group">
        <div class="col-xs-6">
            {!! Form::label('Provincia', null, ['for' => 'province']) !!}
            {!! Form::text('province', Auth::user()->province, ['class' => 'form-control', 'placeholder' => 'Inserisci la tua provincia', 'required' => 'true']) !!}
        </div>
    </div>
</div>
<div class="space-20"></div>
<div class="row">
    <div class="form-group">
        <div class="col-xs-6">
            {!! Form::label('Telefono', null, ['for' => 'phone']) !!}
            {!! Form::text('phone', Auth::user()->phone, ['class' => 'form-control', 'placeholder' => 'Inserisci il tuo numero di telefono', 'required' => 'true']) !!}
        </div>
    </div>
</div>
