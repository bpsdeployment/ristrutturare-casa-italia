@component('mail::message')
    Promemoria: {{ $mailData['title'] }}
    <br>
    Ora di inizio: {{$mailData['start']}}
    <br>
    Ora di fine: {{$mailData['end']}}

    Ristrutturare Casa Italia
@endcomponent
