<li class="clearfix">
    <img src={{ asset('images/avtar.png') }} alt="" class="img-circle pull-left">
    <div class="overflow-hidden">
        @include('rating', ['rating' => $rating])
        <h5>{{ $name }}</h5>
        <span>{{ $date }} alle {{ $time }}</span>
        <p>
            {{ $text }}
        </p>
    </div>
</li>
