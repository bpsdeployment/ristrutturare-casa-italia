<tr id="cart-item_{{$Id}}" value="{{$Id}}">
    <input type="hidden" id="itemId" name="items[{{$counter}}][id]" value="{{$Id}}">
    <input type="hidden" id="itemName" name="items[{{$counter}}][name]" value="{{$name}}">
    <input type="hidden" id="itemQty" name="items[{{$counter}}][qty]" value="{{$qty}}">

    <td class="item-thumb">
        @php
            $product = ('App\Models\Product')::find($Id);
            $thumbnail = 'App\Http\Controllers\ProductController'::GetThumbnailImage($product->photos_folder);
        @endphp
        <img src={{ asset('images/products_images/' . $product->photos_folder . '/' . $thumbnail) }} alt=""
            class="text-center" width="120">
    </td>
    <td class="item-name">
        <h4><a href={{ route('showProduct', ['product_id' => $Id]) }}>{{ $name }}</a></h4>
    </td>
    <td class="item-price">
        <h4><span class="price" id= "TablePrice">{{ $price }} €</span></h4>
    </td>
    <td class="item-count" style="width: 120px">
        <div class="count-input">
            <!--<button class="incr-btn" id="{{$Id}}" data-action="decrease" type='submit'>–</button>-->
            <input type="number" value="{{$qty}}" class="form-control quantity cart_update" min="1" />
            <!--<button class="incr-btn" id="{{$Id}}" data-action="increase" type='submit'>+</button>-->
        </div>
    </td>
    <td class="item-remove">
        <button id="{{$Id}}" class="deleteButton btn btn-dark btn-lg" type='submit'>Elimina Prodotto</button>
    </td>
</tr>
