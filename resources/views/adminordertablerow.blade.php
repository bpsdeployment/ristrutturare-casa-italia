@php
if ($id != -1 && $id != -2)//Free row or empty row
{
$product = ('App\Models\Product')::find($id);
$name = $product->name;
$UM = ('App\Http\Controllers\ProductController')::GetMeasureUnitName($product->measure_unit_id);

if (!is_numeric($qty)){
    Log::debug('qty is not numeric: '.$qty);
        //Comma to dot?
        if (preg_match("/^[a-zA-Z]+$/", $qty)) {
            Log::debug('item contains letters: (' . $qty . ')');
        }
        if (strpos($qty, ',') != false) {
            $nqty = str_replace(',', '.', $qty);
            Log::debug('Changed price from '.$qty.'to ' . $nqty . ')');
            $qty = $nqty;
        }
}
if (!is_numeric($price)){
    Log::debug('price is not numeric: '.$price);
        //Comma to dot?
        if (preg_match("/^[a-zA-Z]+$/", $price)) {
            Log::debug('item contains letters: (' . $price . ')');
        }
        if (strpos($price, ',') != false) {
            $nprice = str_replace(',', '.', $price);
            Log::debug('Changed price from '.$nprice.'to ' . $price . ')');
            $price = $nprice;
        }
}
if (!is_numeric($discount)){
    Log::debug('discount is not numeric: '.$discount);
}
if (!is_numeric($IVA)){
    Log::debug('IVA is not numeric: '.$IVA);
}
if (!is_numeric($UM)){
    Log::debug('UM is not numeric: '.$UM);
}

}
else
{
Log::debug('Frew row: id: '.$id.' name: '.$name.' qty: '.$qty.' price: '.$price.' discount: '.$discount.' IVA: '.$IVA);
}





@endphp
@if ($id == -1)
<!--Free row-->

<tr>
    <td>
        {{ $name }}
    </td>
    <td>
        {{ $qty }}
    </td>
    <td>
         @if (is_numeric($price)) {{ number_format($price,2,",",".") }} € @else {{ $price }} @endif
    </td>
    <td>
        {{ $discount }} @if (is_numeric($discount)) % @endif
    </td>
    <td>
        {{ $IVA }} @if (is_numeric($IVA)) % @endif
    </td>
    <td>
        @if (is_numeric($price) && is_numeric($qty) && is_numeric($discount) )
        {{ number_format(($price * $qty - (($price * $qty) / 100) * $discount), 2, ',','.') }} €
        @else
        <span style="color:#bbb;">N/D</span>
        @endif
    </td>
</tr>
@elseif ($id == -2)<!--Empty row-->

@else
<tr>
    <td>
        {{ $name }}
    </td>
    <td>
        @if (is_numeric($qty))
            @if (floor($qty) - $qty != 0) 
                {{number_format(($qty), 2, ',','.')}} 
            @else 
                {{$qty}} 
            @endif
            @if($UM != "€") 
                {{$UM}} 
            @endif
        @endif
    </td>
    <td>
        @if (is_numeric($price))
            {{ number_format(($price), 2, ',','.') }} €
        @endif
    </td>
    <td>
        {{ $discount }} %
    </td>
    <td>
        {{ $IVA }} %
    </td>
    <td>
        @if (is_numeric($price) && is_numeric($qty) && is_numeric($discount) )
        {{ number_format(($price * $qty - (($price * $qty) / 100) * $discount), 2, ',','.') }} €
        @else
        <span style="color:#bbb;">N/D</span>
        @endif
    </td>
</tr>
@endif