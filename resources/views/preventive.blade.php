<?php
$colspan = 7;
?>

<!doctype html>
<html>

<head>
    <title>RCI preventivo - {{$preventive->preventive_number}}</title>
    <style>
        .invoice-box {
            margin: auto;
            padding: 0px;
            border: 1px solid #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #1F8B8F;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background-color: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            font-size: 15px;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl td {
            text-align: right;
        }


        .rtl table tr td:nth-child(2) {
            text-align: right;
        }

        .table-cell {
            font-size: 13px;
            text-align: right;
            border: 1px solid #f2f2f2;
            padding: 10px !important;
            padding-top: 0px !important;
            white-space: nowrap;
        }

        .blank-row td {
            height: 10px;
            background: #eee;
            font-size: 5px;
        }

        .thankyou {
            text-align: right;
            font-size: 15px;
            font-style: italic;
        }

        .totals td {
            background: #eee;
            padding-right: 10px !important;
        }

        .totals tr.item.last {
            padding-bottom: 10px !important;
        }

        .heading th {
            background-color: #eee;
            font-size: 14px;
        }


        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            /*background-color: #03a9f4;*/
            color: black;
            text-align: justify;
            size: 3;
        }
    </style>
</head>

<body>

    <footer>
        <hr>
        <p style="font-size:10px">Trattiamo i Vostri dati per finalità amministrative e contabili. Specifiche misure di sicurezza sono osservate per prevenire la perdita dei dati, usi illeciti, o non corretti, e accessi non autorizzati. A richiesta forniamo Informativa completa ai sensi e per gli effetti di cui all’art. 13 e ss. del Regolamento UE n. 679/2016 del 27 aprile 2016.</p>
    </footer>

    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAfwB/AAD/7QC0UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAHscAVoAAxslRxwCAAACAAIcAgUATkNvcGlhIGRpIENvcGlhIGRpIEJsdSBFZGlmaWNpbyBNb2Rlcm5vIE11c2VvIGQnQXJ0ZSBDbHViIFJhY2NvbHRhIEZvbmRpIFBvc3RlchwCUAAUUmlnZXJzIEFkcmlhbm8gTGFtYWoAOEJJTQQlAAAAAAAQaU4fcUPos9kngOuJv2TMIf/hAORFeGlmAABNTQAqAAAACAAHARIAAwAAAAEAAQAAARoABQAAAAEAAABiARsABQAAAAEAAABqASgAAwAAAAEAAgAAATEAAgAAAAYAAAByATsAAgAAABUAAAB4h2kABAAAAAEAAACOAAAAAAAAAH8AAAABAAAAfwAAAAFDYW52YQBSaWdlcnMgQWRyaWFubyBMYW1hagAAAAaQAAAHAAAABDAyMTCRAQAHAAAABAECAwCgAAAHAAAABDAxMDCgAQADAAAAAQABAACgAgAEAAAAAQAABdygAwAEAAAAAQAAA+YAAAAA/+ELhmh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOkF0dHJpYj0iaHR0cDovL25zLmF0dHJpYnV0aW9uLmNvbS9hZHMvMS4wLyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXA6Q3JlYXRvclRvb2w9IkNhbnZhIj4gPGRjOmNyZWF0b3I+IDxyZGY6U2VxPiA8cmRmOmxpPlJpZ2VycyBBZHJpYW5vIExhbWFqPC9yZGY6bGk+IDwvcmRmOlNlcT4gPC9kYzpjcmVhdG9yPiA8ZGM6dGl0bGU+IDxyZGY6QWx0PiA8cmRmOmxpIHhtbDpsYW5nPSJ4LWRlZmF1bHQiPkNvcGlhIGRpIENvcGlhIGRpIEJsdSBFZGlmaWNpbyBNb2Rlcm5vIE11c2VvIGQnQXJ0ZSBDbHViIFJhY2NvbHRhIEZvbmRpIFBvc3RlcjwvcmRmOmxpPiA8L3JkZjpBbHQ+IDwvZGM6dGl0bGU+IDxBdHRyaWI6QWRzPiA8cmRmOlNlcT4gPHJkZjpsaSBBdHRyaWI6VG91Y2hUeXBlPSIyIiBBdHRyaWI6Q3JlYXRlZD0iMjAyMS0wNy0yMiIgQXR0cmliOkV4dElkPSI0MGFiM2NiOS0yNDk0LTQ1NDAtYjM1Ni03NzEwNGRlZWE3Y2YiIEF0dHJpYjpGYklkPSI1MjUyNjU5MTQxNzk1ODAiLz4gPC9yZGY6U2VxPiA8L0F0dHJpYjpBZHM+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw/eHBhY2tldCBlbmQ9InciPz4A/9sAQwABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/9sAQwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/8AAEQgAggDDAwEiAAIRAQMRAf/EAB8AAQACAQUBAQEAAAAAAAAAAAAJCggBAgUGBwMEC//EAFkQAAAGAQMCAwIHCgYJFQAAAAECAwQFBgcACBEJEhMUIRUxFhkiQViSlgoXGlFSYYHR1NUYIzI4U5ElJnF1d6Gxt/AnKDM0NTY3OUJIVVZXcnSVsrO0tvH/xAAeAQEAAQQDAQEAAAAAAAAAAAAABwQFBggCAwkBCv/EAEYRAAICAgAFAgIFBgoIBwAAAAIDAQQFBgAHERITFCEIFRYiMWHwI0FRUmaRCRcYJDJFVGRlcSUzNGKBlZehNkJEZ3aWsf/aAAwDAQACEQMRAD8Av8aaaacOGmmmnDhppppw4aaaacOGmmunX7INJxbUpq95EtELTqhXWgvJmwT75GPjWSImBJIp1ljAKrl0udNqwYtiLPpF4qiyYNnLtZJE/IANhitYkbDKAAAGSMyKegiIjEyRFMxERETMzPSI46bFhFVDrVpya1aupj7FiwwEoQlQybXOawhWpSwGTYwyEAGJIpiImeO4605D19eRD3gHqP8AUHrqKXNm+i1xcElZiy1H2j4kliKGr2T9xkLLz+ZcgMuDAWSxNtagncfagZKCQDs5fJElFO/DXbuHdHI2OUxokshdS3CTh+p52xb59ySwlMDx3Ys5MtrtEeOQ7gMaJo2C2EU9bRhueEUZcp34JD2rKipyIZbjNKzGTHvUlhRE9JFCvMQlE9JEnmdfHiwZ6wxJ3xeoo7WqCfbiB9y+Izl/pzfBcyFVZkImtuTufLltWUQQtXjq9fJ7U2s0JhlXII1l2LuqmG07z1+82xgMAjxwb9JDAH9YhxrUBAeeBAePQePm/u6pyMeo/ggroTn2t5srphMBiTVM6gO5FtYG5ygIFXKeWfrMV1ie8CrEBMw+8QDWZOFepFR5B0yZ0LdzljFsyfjsoW+SrQWZMUyj0xwIjHoZwx+MFkapoqFN2+2rRLvWzcQ8dePdCHhauFzl3nKq5bNe0IxHWZbWUyP0/ZirmWYP3kxawH3kjEY68Yrgfiz5b5q2uoOTwjTafYsKOYt1GyU9Ij/xvgtGqsn9Cqtu1ZZMwCK7mSIFZT01hTjDeEyfzlVoOd6q2w7eLt4SWO7Kys7C8YGzOoomJ0fvQ5jj0GUVJyz1Ivmm1ItbGsXZRFQox0VNIkO71msA8+of6f6fOHzawm1TsUzhdhfZJR3AUELFNGJkZNLlkamjBRIySzKBMSAuhCURsfhc/idhrFaxVqHio4VZQxTqt2m4gFopvULa03KTiSxbgXaQomIap64NLVsJpppqm4vPDTTTThw00004cNNNNOHDTTTThw00004cNNNNOHDTTTThw00004cdOyBfani6lWjId6mmldqNOhX9gsEy9MYEGMbHIisucqZAMs6cqiBG7Ji2Iq8kHqzZizSWdOUUj14N6e9KTxw/r+TckwTOW3CzbMto23bcrOCMnT9qNLkiKpQOZsxwCap2Vo3IWll3P4GEkPEbY7bKjFRgsys5eUsmfm+jM9dj7Q+aWlEsriLahR4nctlqCExRZXzKUrLu4Ha5iOQDkpVGz66R0nkKVYr+KmdWApq67dRqdQp6eOSciW/Ld+t2TL9LKTdyu84+sNhklBHtWfPlOfAapiIg2jY9uVCNiWSfaixi2bNmiUqaBQ1MHLvU135m/cHqkBGWx7wRi4e5NISjpK4ciYtXSGYNlR9OsBwmzdAtAvi056W9XBWta+8RyLnOXRL6pqQ2g2FZDYnKKCC0zHZEWYXXUtgkVM7jc/lnoO/h9fsI5i1z2X852qdyFa3F+yjbJZ0AztnXj5uzPDKqAKiDJRePZOG0Y0SSHiPhmqbKPZNilTj2KDcoFDz9KMkl5JKGQjpBaYXfJxaEQkxdKyq0os4KzRjEY1NEz5WSWdmK0SYJtzPFHRityImWECanZ6UG6aq7bcC5qb+xpfIuXchZiq8Dh3C1R4c3PI1l+AyYFSbtyeKaIrMccSr2O1PkBjIlmVQB8w9Mg1UyYqeJ8XbAZRfcXnuusc17/NwltslixTgXH4HnTV61XiYfSLiDoiJyP3QFYvpNVhZcqP0FvKNSLQ1RTcKmdLzEhWtvPF3chjIw3T0xLq4RFdogWSZCRY0hTK1rp4+kM/zi5JTXUIGMFJgYDqhg+QyN217VtzLmB3Tl128zzJyOVpusI1CqzINrUllfC3Ys57adhYHXF6+tYZS4bkOMF17CHtg+z9sU3L7Z6JRsj5ZoSkPVbwzbnM8j3hJZSlzLwxxZVPIBGqfh1myPWoJO2jZRZyxcGVPFpyJppk+jW+IPqH5h/q1bPm8o7vsART6z9Q2l03Mu1bPSIIZSiKNAEnUtq7qbMSPZ1maiUUFj2vGwMgYJS8okvKOGNiK7ex8yrK+TJZIlt+PTiPhaGT3F7bJQcqbVLU2Qn2ktDvfhE/xwwkzAdqMnItzLHnaIoY4IxdwERcxR+2JtwJO00paQ469uM2mKp5o6AutNMKGSxzSZirjYKO+hDT6Ei9XkoXCmzHqQ7HJkhakndvNj4fV4Wpc2Hl3W2ZuPwtOs/adP2uoutu+vVDDpX2U6daPFktbygBNk7lGDnD2Js0MgCm0boU8WdtO721YLbv8AHVvhWuYtttxUBvkXA1uWFxXZBqsqU6s7SXK4mUol5j1AJIQ8/CKMyDJtmq74nmUWskwtK7P9y7FufHFBkMhSWU8KZjj3q21jN9nWA1vXkIJuu4se27Nzg49xczUNmzdq12ediVfIddi35Hijmzwjp3M0qtSW9OnJxpW0T20a1WFxBVDcE7jnONLMQ4g6xJudqhk5jDmTK6qBFFmL5adjGMDLEZ+CaYKMMydmO1K4TV6931WnkKFm+pQraoSfchYde8Bj694AGOvrKoR5DkY7rtZZ1G9x+lbWq/ht527Bqu0YfWLt1luncYrG4ErT+k17DW9a+tWLDS6RgMzYMqqBfJK1zMWkZ2n4q/zqnlrtYDz6h6gPqAh8+msf9sGXnub8KU+8T0enC3YhZWp5KrpBAPg5k+iy76oZChASERUSbtLVDySkcCoAdSKcMF+BKsURyA1rQ9LKz3V2x2tQw1MiJiYg1lIl0mPaY6xPSY9pjpMe08ex2LyVTMY2hlqByylkqda/VMhlZlXtpB6ZNZdCWzxsHvWcQay6gUQUTHDTTTXVxXcNNNNOHDTTTThw00004cNNNNOHDTTTThw00004cNaD7h4HgR9AH8Qj6B/j1rrQfUP0l/xGAdOHFUfqaX+QPhu/PPEDzO4ffflo7p6QT+I8oW1GvxeGapDc+gDHtp+OPNppcCQr1wdXgFDCOobMQYfyNnjINfxfiqsvbXcrI4FJlHte1Ju0apdovpmZkFeGkNAxSJgcysu+Om1Zo9ocquFW7deUHqUMF08G4ESUUVUWqe6TqGVCa8T+UnMOM9K2Ft4/AiUFnUWoVcvzmSADByUfSLDF+UsgYXvVfyVi+0SNOu1YdGdQ87GCkZREVUjt3LV00dJrsJONfNlFG0hFSTZ3HP25zIu2ypOADazU0tVrBRQlEWydkgry+Dmv5a1h1Kn5oXPklUV61YZ7PreOI7fzRx4cc871K1zlrltY5M8GvH6bYyq8aaIyfo8ziqGxbFGOKz1rRcZlcxmWr8/5L1bC8sx9aeLImNsbY26aLaBxJhyrsd0PUlzBDkbN2LBERi6RFvkyqrv5RdUyStBxTD/7eeLv3EbYr2Dbz8q5iYMqAREj207ZifEk/M56zpaQzRuzyA3/ALc8nyKQmjaoxXJx8BMWx6yaSdaqMakPkAWaNWTqVQT7Rbx8YdOLTjz6Eble6QW6HKVuVPZck2PJtcbzt8nB9p22WbOqx7YctXs86BSQVZrSipnx2XjFa+YKiYEQK3bFRsCag3b792nksjimWDfcKVBmMoU9H5AiUt0VUxHSKmLr94gqkr2bIQ2wR9FKT6W8hNZ1vYtQ1Pd6uLRjdeTFx2g6YkeuN1dKLlvHFmr5kRFnd0yUIa+7sF3uZTCwVPGAjvvW8h+GTjI2Zjn0RMMGUpFSjNxHSUZItUHsfIMHiRm7ti+ZOk1Wzxm7QOdBy1cJKIOETnSVTOQwlGG+6YoyX04pqyZDwVXJXMGx+0upCTzLtnDmbnsPoSYH9v3jD7eQMuWQqXgKqq2WluxUagyBQr/vj+ydgsnuphHbknu1W0vNrk9ZoO/wE1CWGbJR112l3mKHGecPZYupO2YefSlimUj5ZRvGnSk5ONi38WwMo4eEauYvOknlffRuNzxP5NyTk672nA9YpTuo2ctoMmSpTFtbMI9jW4urxabNswLcY4yCk7cJuORI7Eiq6NkcKvZ1qhrjgcbYHBZLN+vxjMYkpTksNba4WWCDxFV8cAufDaZLGehtKKGqYshgiWT18dvNDb8U7mXqHLmNZ3GruWRUF7UuYGCpUW08OuxNheYK0diyEZHCVAq1fpNhLiW07lNyzNS7ScdYnF3eV086JcMfG3k7BnrW/wCDLA0dWG1Y3rYLO5OiAmArzEjU4sSmkghYc/ihZqE7SLYKOdNVWPbuoAgtYiF+DnpKrTMNaYRydrL1mVjLJEO0T9qiEnAvW8xHOElC88GTds0FCHKPzAIDrPTddlvJGDdzG93EeH7dK44xlkTKFpgLbR6idKHrkhFpvWrsUWsc1SIlCqrlOvHvHcGEc7ewzx9Bul1oh0ozGPRYQBBcfQAKgsPHuAAKkYeAAOOAAA4AA4APcHGthNZTkBxYDkLY5Cq5SG45jhmb3obFdbBRkWey3vT3ynzLiJcAQxnQzkR8p+ceQ1Ru7OdquBbq2bx17IU9rp0GAOuTsuLytiqeS1Ov1K5jsdfiuF/0Flkxj7DpqVpJKBc28ns4szd1mLdXCxaYNa5dX2CdzteZd3JG4bg8Ssgs/l/QOEXVux/Kyigcj3OpJdQwmVUUOaQnUXmxxi5RzFcW6wqEWqeyrYLTJtM3oBbAjVcpzSyapfeDpFhINBOUQASpuExEAAwalD1q5ngFeSMA+yKuO9/1v9HVfrzM+8yz/WTM+8yUzPHtTyse6zp1dr46MLN7b1Hp2iEfSvNfkACIiFrqzM1VrGIFYJEBiBiOGmmmrNxInDTTTThw00004cNNNNOHDTTTThw01s8RP8sn1i/r08RP+kJ9Yv69OHX8fj/OON+mtniJ/wBIT6xf16eIn/SE+sX9enDrH6fx+Jj9/G/Q
                                Q5AQ/H6a2eIn/SE+sX9eniJ/0hPrF/Xpw6x+n8fiY/fxXD6kOFHkrUt4eO2DADy1KvlI3745QImsq9laHdK+TFG4Jmw8IpkzErFog1bfLo+h0kZqOWNwLsgnrTfpAfzgPID+cB+cPz6vl7vMZWCZjKjm7G1dTuOTMIqz7lShgVup9+HEFwjkonMOGFiL8t13VsrzZtM1FN0mq3Je6zXCKARu7dG1T+3h7a4/C1mhb/i505tO2bMyLm04PvAJqmKhHKKKKSWNbOJygpE3vHzkHEFKxEiVvJKt4/zCrZN81mWkfsPy02BVip8tcyIYUjKe4v8A1KULU+v7/wDmelAZBUz7vNmQEO70bJ48mfjE5WXsZnp22hVNtRAuC/KlzPXD38m65i8n9WJ/I4y/k7Gq3YiZHHV6uqnYkCz9WCnD+5/f+CXcP/hTqv8A9GT1LHvL3Nt9omBbJm5xTnF7LAS1WiU623mka8Z4rZ59nBkVPKrR0qVskz80Lk4Fj3B1vDKiXw/EFVOtt0r+oLgzZpSMt1nLsfkFy9utzgLHCLU2tsZ9r5NhXDQ7tF6LmciVWrkjhMiiZQIsmsipyU5TkOTXu3UR6pG2rdNtdtmHsaMMot7bN2KiyjJS005hDQ/la7aWEzIgu+QskkqkqLNsoDYnlTgqv2JiJCmE5cYzeqZTJb8xzsTcfhrWUoeosCtgoKpK6oWC8wSJCMDBwRiUSPbPSYmOJo5a88NL074XKeOx+96/jeYGH0zZixWKdZqtyKc7FrMWMWr5faWxbnMcdc1Icpi3QYRIGB9J91xX12Y7JeT8c44PtmkYYmQL3UqV7ZJlVjImiTWmeYQacl7PNR2IPgYqPiOTtQeNTLkSMmRdM5imCfWOioyHamZxMcxjGgrvHQto9o3ZNxdP3Sz1+5FBsmkl5h68XWdu1uzxXLlZVwuY6yhzj/PGwnb4nH2ZsSX6eK8PB0fJ1Ct8ySPQI6fnia1aomakSsWyiqBHDwzNksDZA66JVVhImZZMDCctrAeujst4EAis5hz3cf6nMWPHIjwI/wBuXzAPI+nP5ufTXzeNDKpYoBq2FusQxDJuenK1bHyiwYV3y5reyewi6QMx1jrPSZ45fDT8Ta85idmfzq5h4CtlKuRppwJZRWGwjioNqkVzwhj6dMXLmwtXeTBIoKIiJ6e0VwuoH/Pd3T/4abb/AOtprzLbLhmT3CZ8xTh6MRUUC7XGKZTKyZDmLHVNkqMtcZdcSgPY3i6uwlnahzCUvcRNPnuUKA/fdPk+u5o3G5ty1VEZNrV8g5FsNqgkpxuiylkoqQVTBr7Sat3LxBq6MmkCiqCbpwCQnBMVTGKOpb9ku0m40KvMqcVF7A7o92lQOwcG8qUJfa1s8kHaKWQMp2AFQFSAyLlVMiVSx1EuCtZEj5yyUA6QxVobtpRu5L5Fq9IWmNa9OLrVki32lDl01i+w0Y9/FQGCe/p7z4xSEy5ygPSvXdPnmdzq2F1FDMtrK90ymYyD6n9DJY+znrB43F0nFEL9ds7iTjMb3fVX6pmQsQFCjdeicbY+ihcGGctwzdAicTnvM80/oChCAkRXDuKYqMw5jByggHqmzmo6mSlqYevaqysTdcvIK9586NdZp1VrlCqdapFTj20NV6hAxNarsQ14K3jISDYIRsWxRATGHsbM2yKXcYxjHEoqHMY5zCPZO8n5RfrB+vWrV+zFu254QQrmRBAFPUgrpAU1llMfaS0LWEz+eR68e2Wr4huDwOOxtgwdcWo7GSekZFVnLZB7Mhl7SgKIIFWsnatWFgXuANEJ+zjdprb3k/KL9YP16d5Pyi/WD9eqTi/dJ/R+PxMfv43aa295Pyi/WD9eneT8ov1g/Xpw6T+j8fiY/fxu01tAxRHgDFEfxAIc63acOGmmmnDhry3OErJQeF8uTUM+cRkvD4xv8pFSTM/hO4+SjqlMPGD5qoIG8Ny0doIuEFO0exVMhuB4416lro+Taq5vOOL/AEpk6QYvLfSbXV2r10mqs2ZubDASEQg6cJIiVZVBsq8IssmkYFTpkMVMQOIa63QUqbAde+VnAdJ6T3SM9vSfbpPXp0nrHTijyAOOheCv3eoOnZBHYXYfmJJivtLqPaXfI9C7o6T79Y4oWNuonvsM2anNuyzaJjtWxzD8KkhETHQTMYREY7kREREREfUR19vjEd9f0ss2/apL93ak/S+5585JIopfwisTD4KKKPPwIvI8ikkRPn0kfcPbzzxr6fg9WcvT/XFYk9fxUm8D+ngJLnj8/u1BEYLe+kdRzHX2/rX/AC/v33f9v3+YX8WfxO+3WtvXX/5mr7v2h+/8e3WLz4xHfX9LLNv2qS/d2nxiO+v6WWbftUl+7tShD9z15xAOR3FYlAPf/vJvPoHziP8AZL0APnEeAD3jrYn9z35uVKBktx2IVSD7jpUy7KEEQ94d5JMxeQH0EOeQH0EORDX35Fvf6uY/N/Wv+X9++7/tP/H5/Fp8TfXp6feus/m+miuv5v2h+/8AHWOsYHxiO+v6WWbftUl+7tPjEd9f0ss2/apL93alC/B685fSJxN9h71+8dPwerOX0isS/wBwKTeRH9IBJchp8i3v9XMf81/y/v33R+79/wB/iz+Jz+zb1/8Ac1fd+0P3/j26x9Yp6gG9yYyni+Ild02Z5CLlcl49i5Ng5tCZmz6OkrnBsH7NwQrApjoO2jhZusQDFEyShy9wc6tIbltoK4t8hzuN8fxGWsWZWkFJ7PW1OVkEYFpa7IUQ7sx4Gsq6zdrjbOzbwyOZJE7mMreRF0U3D+RgLMAzT6J+idArNdSvVItbrcDit22qtzqNncNG9MuqK7tCu2OMm1mqCy0gZFNZykwOikdQpiEUOU5wEoDq0qHHHA+vIm+YePlGH0H3h8/A/NqUOXR7HgfXNyU24YxtaVruWzsCxa4IpgSXZNiSWyAMGpYl6mQDUsFgwUbGcleWW6W9f27F82q2bBtq1jjwVjLZWtmrVYSp5GtkfSw+zl6hVbCbAVsjjr6HY7KVmFVv0rlUjVNJ3J/TqsMy4slh2izcjm+Ar66nwqw5Px4UvdPiBfkgmgsiYmmvZsnLqNTKA2b2CtNjIzvhmdxsW4aGI9WjksVenqhKOIS2wkxVZpooZJ1EWaKf1+UbKkHg6azCYbsnaZyjyBgMl6Dr+gLmDbNhTOS8bKZApaDi2QJRCt5CrshL03JVWEO4xBruQ6k9h7dEpEVN4wskZYY5VQAM4ZLF5KOOVk2b5bXZhD1/dhZLXW0REzSt7l8M4n3GNWwGD1bhYJCKpV0dI8gAeJJ2CRdCHylF1lBMobZnEc1JBQKvwDZGIHvty5ToiIiI62qNS4Fovb7SoUZiJiCJpRJnC++/BGNm6+5q5WaQtMjhOBjH3qJkU90zGF2XOYKxhFR17ZFG07KJHEkitSRIVU0axWQD3roB+cVkgD+sTca95w3tkz9uBkko3D+JrndinUIReZZRKzGpxpTm48eWuMv7Pq8Y3J6mOd1LFP2gYU01TB2jbgY7G8wN3JV2uQNn1UXTU7iTNL2BUlpYfT3LJuZrJcowQdAbgQU8gukT1EUjega9WS2OxtxTSS3D5zzbn+MIVMp6DMT0ZjDD6hUePARcYvw5FUqImWaQhwDG0P7AzUJ8hZBUom77nb5rUhXPpV1e/p9sFftMGf8AdrnQxyW/5FkEdI/P14w3A/A9nnW1/ObWbKvBR3KlOs4SuyOoyUNy1fZNtvU/b2gk6rk4nr1jpEdeIPtpOyKAotyQLTWdR3aboIB+3BRwxO7e7O9rswTtOWeyhf00CFyzkWBWEriKxxUCLvk5BuPmGMP4aFpb5odSOt3jY/06815Rxflm7pbj7hkLEExkzcO1dIw+QLpNyd8g4N2iiZqVZpW6dGQrtxX6lRooPYtYr5vJNvHduJGQezdVOo1SiQEbVKTW4KpViGblaxFerMSxg4OMbFERBBhFxjdsxap8iJjFRRJ3GETHExhERw16kG0yzb2tqVz291G2wVJm7RYKHMN7DZI+TlIlqlUbdGWRyis0iTpvTqO0Y8zZAyZwKmqoU6nyAHUGb9sWa2TEZtVU7PqrOMuJrdjhVac/wM9KsGBKE1Fg/tNKU+IAb2OsOsWFxZ49RPg/5V8suTXNblHk9pr4aNYwfMHU8lsMWsY2/gqOGXmaHz+3coPXk7uetvxI2a+QyF8LlqxRKzjsVRxWKtsw80QPjPOod9MzP/21H9h0+M86h30zM/8A21N+xal7/BlNwX0ncM/oo1/EP6we8a0D7mU3BD/zncM/po2QA/yvdaifRjmr+psP/Px+7/FPv/8A3/j+mP8AlEfwbXt/OeS3v/7PWfu/YP744iF+M86h30zM/wD21N+xafGedQ76Zmf/ALam/YtS9D9zK7gA9B3PYXAfz0e/ft2tfwZTcF7/AOE7hn7D3/8AbtPoxzV/U2L/AJ+P3f4p+Ok/f1fyiP4Nr+1clf8Ao/Z+79g/vjiIT4zzqHfTMz/9tTfsWnxnnUO+mZn/AO2pv2LUvX4MpuB+k7hr7C5A/bda/gym4L3/AMJ3DP6KNfx/yPh0+jHNX9TYfzf1+P3f4p+Ok/f1fyiP4Nr+08lv+j1r8/T9g/vjjo/Rt3z7xc1dQjDeOMt7lMuZFok1AZZdS1TtdnNJQcivDY0sMpFqO2flkgVMwkm7d82MJgFNygkco8AYprtWq1HTs6HeXtl+7XHW4m2ZyxrdYOlxV8jnddrtWt0XLPTW+mS9YbqNnks4VYplaOZFF0sCpeToJKJpgKpyCWyvqb+XNHPY/AORsUXIvlk7DQ9bbi670pVqYq6Oh9jovyA7oHk+qXdPbHd1nyC+PDcOS27858TmORB6yemK5fYOhbLVNdPWMb9IE5zZ33YZjjxWHI7fobOMltr0pQxUoX5j8MgtppprPeNLOGsZd56qqG0zcesgqqgslhfIiiSyCqiKySha0/EqiSqRiKJqFEAEp0zFOUQASiAhzrJrWNu8SPfyu1XcRGxbF7JyT/DmQWjGOjWjh+/eul64+TQas2TRJZ07crHECIt26SiypxAiZDGEAGuxcxGSx0l0iIvVJmZ+yI9Qvr16+3Tp9vXjGN2Ey03bBXBEwtazggIRMmRzjLUDAQP1pKZ6QMD7zPTp78VK64yqMbt1lsxI7j7dVs/QmTW0BWcXRtpkUH0tUwbwi7i2JLsn6E7DAxF5KuPayrv2Sc0QEeCPm3BDlkm3m37Klv6YW0m65LkZ1HIE9eo9WWll1XUPMzkcSs5GQr01KFaCyUM5na62iJdyochQerOQfmIB1w4xkpOx61XXZBa8ywVFuEJnLFuYJh+rDvoGxMZ63YyZ1+prmbRdYlmJfOSFak3DyxQy7ONOpKlbzMQr7QUM1boZPbwMqZJ3V7CcQPZHF+QmmWajmSJgcjVtvjq2NV1X7HHtvSRtsREEhSuPg1Y26jZ4msg38nFyi7uCOoRRqiC0+ZCxUt5nDGk0Wgo7ZYVdsOmqD8cPpYFFRIrGJZjbElLRsNYc+UVrPoXaIeW2qYfN4Pl/zBrZKrkMI/ZeR2Ju6/iscOZsY3bGxn1tyWcyB3HsCrt+MFA0m4ypUQPy9tm0iJTDH2egb9L7lOL28bBquafuMHhez4HpTy1u666doGsdqbQ9WI6YyjoHTVOYkYysqrSkDCSz9Nk/euXjtcqnlTOWeQezvaHt0yXGZPe4G3e5Gs8BdMQWjHs/QZVMYG40OYuZWLVK1z9abyEUzkVItmg/bwwu4FWNJLOPOR9gcmbFLruuT9wl3wjgrbBjS6bP3m4LCtl2x42Nb2shXLEK8FdmEYlHrwkh4tXscUzfNI5Fo8TZSUdETDByoou3fgPKaPgfTWwhkCa3izmf6lhy24JwHHRF1bx9ftLifMkdvZo9swi6TESVhZR0taUWUqQbG5eCzUjogkezZ+bUc+T8TH5e+NUvSDowo0jyVqvaS/FWKmaYWQNgVrFc++8q93DC0kMQMgPeRSEgqZSXjsY3njrK30W8wm7EnUMLmMHksdu+LzfLyqrVKdWzl8Rl60I1q9rHgay7kFtYTJezwrSFlbbvGKmQ9pJqVvopW0VDMGQH8HbHNLQXuq7g6M2xC1QcpLrijFoyIxigs1I8qCPiGHvTVMdTuMUvd7p1LMOOdr+E9ouI4W/220owktnp+aySbpeLlpEbBJ06bBB4nGPTJnTjzPTtWvcqoIIl7uExOcuvd85Um6POsTiW1M6dbXdWbSGKDObM1rE85rjYG1QsSLkXE8hHqRCAN1VE0nAqvSAgoommqJDnKUed62FNuNub7by1Ko2u1Gj1MpC/CsVqcsQsActqMDYXoQ0e+8p5kUFgb+YBPxxRVBLv8M/bW1szYt53Q1Wrimofh/mN2CisAzkCx+RVLWkIDKz7TkezqIRMxMB3dJ4x/M8u8VhuWPxP3cNgLlbK4vmIOqa8a25drl6qratLuBQqKdYbFivDa/m9XK3We1ZxNmV948YZYrl3uCd8GDKJtN3DXDOFSuMpQUbozLMLy0A7SnXy5L3WJZu0WPXJZCvVxJSxGnWzJBzXFewq7sjtg4Mpky7kpL48FvH+0pHyHtdqHkAkXoMP5uiivqxBfynqqAKCHg/7KPify+B11614ruvTn3i0LMOHcaW224IyRWGrSwVis1ywXOQrkS8bxKF7rSbhFnKS7GSiJAjG61JV44SUkUFHVaWcuUUHXHKbwafmXBe+Gn77MZ41nssY3sDKq2YvsSJm1/LKBRQpU3AWFsxi303V1pSCURlYSUfwyrVF25M0dpi7YOWR+s31shcA0NrtHM6Pk8fRv2DqIs38oTVSVS7CxShFtHbKxjqQkPcQuZ7wNRUx2Z1fXnJydPK0ncvviT0ratk1fEVs5k8Nq+kqp2PHm9bK229k8ng8mBjZsGIraphKB1Ct1E3chvUk5Jt1XtqrZCQkkWh3e3s6rNCReoM1+cmWQFAWapLkbKgsUoJq+IifvIAFOBihxrGfqX7TjbWncHkaFy5fLU4zDfslSbuGk1jRTGAAO62EbRysZIiquiRWVOwJ5gpRK2bpnApDiJde1Y2hs679d+2NNxklhiy4kxljF5S3zx5ZG0okzQjceupCcjolvMS0PXxsdjsVikOxRvDxnlYqMMZZ2qQGvjO/eutvT7fb6LgdCp1O0WpdnZMiKPEaxXJqxKtE16rGpIqOk4Zi+O2TWVAU0TrlTKqoAkTExgEA4Y26eLz+l4abVdZBh5rZtIMquWBiNywpDnjLABiygSmFsGfceskJRJVO3a9W3PlZ8QvMMcPlraX8wwzHLnIvr5jH2XVbFnXMXfyOPxzPSvdUs15agCtVDAZW7tBT0N7P20fpMw01iFCPU3HZVRC+OccX5d0VjHKOIpaNrVhTGKaCeT/jGbkbosZdRX+NA0W04Ip3nMEZe0LaMfdBm7NWHpfMOQKizxkxn1GU5ELqSL6WGNvLmlkB21kJMrVt4rdMH5vLn+S4EUimFIA5tZ4yRWb42x+3cIqoLoUuoJLILpnRWRVSgYwiqSyKhSqJKpnKYiiahSnTOUxTlAwCAQd9Lak3Su7xN1UtYabbq/FyUfbixsnPVidho6RFXMrt2kDB/Jx7Vm9FVoIOkwarKio2EHBAMiIH1ZsRtOYZi9xsOyMTZqpptomSakEDjtjXYSx8MQczXWISMiQxEd0RBT1mRN+5J6DT3b4fsVj9UaGIzt7PUdkQrIZ9in062DnKVE2nTkTZVEMnasPAwagzM/ERksRWPWZEZCG609MrJJmWcR0WWlRXgqyDwrVyWO22ma+OswK48n4jhRuDpUPCMHmDCfkxg7x5HfxJSTfqe7TGjeSkm7RZPAvjtG8i9btFvEzBYU1PGaouE263iJgBFPFSP4iYAQ/cQALr7b5cb5x2/wC+qp71qFjWayhTRLWJF8hAMJOTLHykLVV6NPV6fLDMZSShE5eCOlIQlgNGOo0rtdRFXuWZGaOOjUmLzr1At9uLM+SGFrPiXGOLnVJdP31jbSybRtEY/lX9nbxyU3LQkAFhs1ksL3yyTOGjDIRccbzD05U2ijhxdq0JJeIz8vqfLaeiMx1t82awkGUBTlzVmvLIcVgjMYARXPdPtHvIQWCZib9e1vfK0cbn/pjn/ibqbdhMcvE5Y0WdNsXsbaXmgyo1ix68YmtWYbzZbCUh9dgwAPNXj+6KDp176neUKRlXLkxiHGsjJszTVzQsPsttXzMMPQcnG9ntFU8SiWRmG7ViJFG/Kx3YkRErhQhwyj6P+Rcnv8v51xkhfLVkvBdVhHbyvTk+tKO2DWZRuBYqqv4oZhZ45gDXWsFkpZ1XCOiodrFB35dNRudVboGSNuP38OrFeqzkLH9yf4ptykk3e2RGDs8fBouW+BWgw8qytTZojFldRNkbNV2YGfKNHEozJHO0HJDrND8jtPydmvYVJ7kdud+xte7JCwrS7WfF1rrmObNMxMvkSOg+6vt0pOIg3SLuCySwShHbQyzlYK9OIOIxcW5XjgjfvyJpvasrF1STbvzq2uOXRdNYFqGWeN92mfaTmXldojYVLFQCfGyIYUmE2zUq2Q1vnZc3fNqvYLVl86+bdCzstEcw21ecFRdnGa5nKvnDG19cvQ9tjGXwqW2OvjcrtKuoUWVeC7wnNt3cbt91bynS0qet7b8Q2l0wQjpB4DV0xxItGMpxumVosREwzNom7WsRUoCoojHpgBzAl6SU4GzS4y/0n8hyDqTcOrPjvB2V8ZzzzzSnnvO0yoyCEE/O4Kp5kF3tWcQD0HJlPFOsdVQqgmARCNfZ3sU3b5dq2Qcg1bK07t9VsUw/pNsaWWHu8DN39o6aIzU2u/apBFOHldXezrhE6L1JRs9eC/4NwmPPbtoUHl3EFP3+7aLVRbuk0m8LZVWrz4tMtXsOXuuPYqbqzpCuPjRXlH5rZAvWjyGRaLLryiMQ3TbAuqBSj2Ziti349eLqZGjYbq9rBMUhfcL1qXKqmR8rDAFN8pHNovCx3bK+jO0/bjo5fZjc8dtdndM7q2zYqjzqwfM6rfyN2Uuxli9b9VmtTGjWrvddpeiTWjCKjIVafki51qQxXcXGC1Vj6c026WLMH8I221TPVeyNHQNQxdH2mQSfWCsHbwK7qypKs36E/D+z/OzDkZkzv2R3QoR5kfOOiKBJPu5yBla59LPatdclyc4F7m8kxhn004WdRU1PwyELk1rWpuUFqLNVVxN15rEya7hQoe0TLkkjlMdyBtYxY92NWm/7Ib5luIodwhM44sy2/kEoqQg7HHTlwxmyqtWcvYyLrcqzS85IQMi6fWSEcs4868iszlIU5nx12rdvlBuvytkrdP0/sVHlsYZBaZcpWaK3AZCrzfHVtaOHi7Gg3JFndIeILBkW+D1hbHbuVFGrfyURMHeQhhS8FoC10yNmpbzGHJJotDR2xi7r3zWB+OCaUCisgVBBMxtiSlnnYZz5xFZe8iK8M1HEZzB6DvqMlWyGEfsvIqpa13FY4MxYx+2vXsHkyOZyBW3sXV27FhXilOMp1UiWNa62n8n5G29emvSttbjN2HLPWdzGULXmptTJycm8TzFTnG9UayTmmuWVrjgsj1oVm7b15SVVUj3BXawvztkTJHV7hEtjzUBvT0ytAI5LwxjEdhKuNLe1pj+szW4h3V5SHk3TqFprlxKysku8x3FmTWuTqLK3dIurKY5nD4Ciu8OUqSk+Woo3wnnnIl/qf9lHw+qsY6wcp9Ta7ZWeNEVCmZ7vGDomzEdZbMxI8bx/C6nFp5aSGN+T9fnTZv8AyXGbbikxkPk+Fh4207i5152RgYVFu1jyHDumAimsTB/DTTTWFcbHcNaDxwPPu/Tz+jj15/Fx68+711rrHfdreZjGu2XO95ryqjedreLLlIQztExiLMpT2O4bMHyRicCVRi5cJvEzf8k6BTcDxrurIO1Yr1l9PJYcpAdfs73
                                MFY9enWencUden5uLbmcmjCYjKZm1BlVxOOu5OzC46slFGsy06AifaTlai7Y/PPSOMI9x3VxwVg+3y1DqNbnsw2KuPHEbZHsDKxcDT4iTaKCg8ii2iSI+PKSDBwQ7d/7Ii3ce0ckUamkTOkl0UuT2l9Umi7pcmReJm+JbzULNNspeQYP0ZmDt1ZRawTBSQkFpaVaGh38WiCRCIIrGiXKKz5y0aCcijghtRndHHBWPspZUyjdMgV+HuA4xrlYSrUTYmTeXjUZy3P5tNzYl458Rdo9kGLCBUbsF3aK4NV5Jy8TAHhUVkrFddwZhunXVzkip4yo9WuzqBXrb2y12txcHIu4Nd41kV2Ts8W3aouSndMWiorLInc8IESBfweUxkDZaepa+dnBBjb1zKV6apnLFdNYesekHBPpYnwygRYJFED3dZlXUpHyzqnydz3PrmqjCcznblrGB0jKZ63H0FVriLby1/G5F1CyIZwwm+vINbUelZE7wlIjckFAcURx93p71Kps0rtHl52pSt5lL1OSMVFwEPMMYZ0kxho0H0pMru36ThMzVos5jGPggl4ijiSSMU4FTOA+a7NOo3TN319s2PGGPZzHk3A1YtrYhOWKJmyzrFGUbxkqi08i2bKIuIs7+NcKkU8UFW7sTkAngKCMSHUrtL7clvzqOCa658wyqbykYeYlROYyJLNdpePlbm94MPaVVgjKRrF0YQL4YwCgCIAmOuOtDhpsl6qyMowBOJoTu+Qbs6aAAVinjXMMO0jJYhC8gmdrBSL547L69hFYJM4CAkKbV2p6biXa7XSxDJ2a9gr2drH5nQIrS5E1Ueng/FMvS8A6kuZgvLPtIjxg2wfEJvdDm1lL9TI1Q5O63zN1zlpmaxY3HmxlrI47ILzOR+aFXm6CsZkMbYsz47IhKppLj6jWQyf3dVu4xVtHp0facjKS0hJWF26jqjUK6gi5sFlfM0COHotiO3DRgyjI5JZuaUl5Byi0aC5aoFBw8dNmi0YUX10MeLSoITmArlHxBTiLh9E32rzMui3J6qLHhFo+FRUOmQBMZEkvyHHaCg8865brK7eMmZGiMX5eokFL22Gx5G22BukTBNHEjKQ0dNOomUaWlGLaAq8dxiKsY5ZzK7NBZWOTMweqk8iDlw2w92zb8Ntzuj0/Be7LbljSRqEAxjK7HZQr9MhXh2LeO8JBvJ3GC8l7ZRepGSBabs1Wk1Xi6gOHjyC71XRjdev61hLOsoyvym1sV0mvHIV6eR9LZxwAxgr8NUSH1DCWINhR9zGQcEuJD2ir5qc4+ZGH5y5PR/pzheUeuopYx2q5TYNRHM4fbH2atNtmcjmnqf8srrtus1CuV4VVqelJNowsRLCslJZLr58VEy++RlIOr/AL74rpCcbFjpmLgAr/wkVJLsSrrFZSDWO582zFwr5dwBkRUMIc6hviuuNjx/IRTd/gO7RMa+fxzd5LK3WuLhFsHjpuk6klmqUcRVcse1VO9XapqlUUKgZIpwMYptZEdVnNsbSNnD6IrUozXc5yfQNHgHMa5KdJ3U5BuWyWORj1W4+CvHuavHBHlWSMKR0pxv4fJVC6g4tG3EY7py44z8din7Ul8/WtWSWAoeYPQ7FFp0SDBUQ+UVmSyUrzCHdwUTTZ1C+h+R6dQ1rB3MfF7O12zGUzI4jFDFhySUcIayT/JkEN72x4PygzEMTPtHUomu5+84+ZWu7UWs8s8tRFmmcvGb7urzxVC+u8ichjqopiLSbU0oRTZGRmK7BI62QiZYfjXI3DWyyDlBFy3VIug4SIsgumcFU1kViFUSVSUATAZNVMSKEEoiBimAQ9+vFtyGa4/brhS9ZolICQtDGjs4t24got61jn8gWTnoqBIRu8egZqiZFSVI5MKxRAyaJyF+WYuvCunJmI2aNouKpl67F3YajGq40tChuPFGYoopxDZwsbkTHWkq+EHLHOcAOcXwnHu57h/B1O/5iu4D+8tU/zh1HWFVsX4tlrYa8MkI5qvjrIxJB3rm6FdnaQzBDDAmZEhmJ6FBDMe08bH5jd5u8nMxzE1twpa3lzktuwzTWp8V7I64/K0/KpgmlpVbAgDksAgk1GpgTHcPHjtQ6pdPtm3TMW4dPEdsj4rEFrplVfVtzZq6eRmlri4i26DxpIESBg0RZDKJmWTdFMosCRgSHuOXWPBeuZjs3yibfr+cOfeW7VA4c/3QRMAD+YNfg6LVYrdww5uGgLbX4O0QTvJ1SM7hbFEsJuJdGa1Fq6bGcxsm3dM1zN3KabhAVUTiiummqn2qEKYMEuq3UKnS93yEFTqxXanCDjbHDsYetQsbBRXmnUtYk3TkY+LbNWguHJEkiOFhR8RYqaZVDGAhACScfruqO2nL62/E2DOsRWa9kchZEAqjUon6eVwzvM/Kxp+Ujmeh9vSYEeNPNr5tc9MfyU0Pm/jt6xVdOYUjD5bEnquHdYs5h2d2FXzRdk6spRXihTo1ZqLSMd6DdE97TkpuNmnUXqe8e/WmhwGNbFSnNXp5betITFjgZdB23NOMITySKEQUjhJbxH5VxWVEUexIScd5g1w273qZUjadlJlitxjeeyLMnqkbaJZ1DWSJh0YMZh4/RjopwlJNHSyzxwyYe0zGSEiabVy0AQMZXkM76fjDFePSHmqdjygUh64iUm0lLVipVytO1o4nhO1EXr6Kj2SyrNNVErs6K6hkAVRKsJe8hTBWTwk2Z75OplYLTPENI0iUn8i2xwRzwqklQapXHdNpDY/fwQCHBzWViB8kvmFFDgHPcOsdweN17L5DNZMsbYra9hsUVo6R3HMadqA6LX6kSBnV0reYRBREFADMSMzEy3zJ3HmxoOqcudNXuGKzPNbmBvCMLV2JOv0KtJOFJ4lasziGqdUiKA28bXstJRESmufEgYwQz67NN39V3i0OyXKBrUjTH9VtSlZmKxLSrOXetyqRrKVi5UHUeig2M0lG7lcqJfDBRNdg7TOJhKBh8D3ZdTCp7U8ukxLM4otlxeqVivWf21C2OCjGQJWB3KNEmgtJEhnHjNzRZzqqlHw1AWIBA7gNqMXpM3eSwbu7yFt5srgzclyZ2SlKt1VASSNfsSykotHK8H+SKshBo2lInA96wmalIJvkgPQesN/PNbce771ONeP/OLdq8VdNxP04sYhyTbh3YwsnQGHuCZUwU9kw0WeUhBsuCO4ykhEZKZnjAcv8Qe9T8NWM3yhka9Lf8duatP2lx4zHuAL1axb84lQbWKmllmhOOsnC0BCmNMFQIx04tXNVQcNm7gCiUF0Ulu0R7hKCpCqAUR5HkSgbj8Xzh79Rhbj+pvUtuufHGCJTE1ss8igWkCNijLJAx8ab4akanbgVi+SO8L5DzRSrjz/ABokHweOQ1JtF/7msP8AwTT/AOMlqqT1Ov8AjEJD/uYE/wDaidYxo2FoZzMWqeRUTUKxlqyAi1iphym1wAu5ZCUxEMLqMz0nr7x1iJiafiV5hbVy45f4XPaleVj8nc3DB4iw9tKndEqN2nk22FQm4l6RImVklDBCGD2dolAkUTaRyJd4XGdBueQrCoKcHSKxO2qVN4hSqGZQMc5kV0kTKmAvjrlb+A3KI/KcKplABE3Aw2VTrb47n7JWISVwXcq3HTs9BxEhPOrtXnreAbS0g0YOZV01Qj0VnTeKI6F27QRVTUOigp2GA3br1/rEZe+Ae19vjti58KazTbWFZOkX5Kg1SvGTs9pUA3ID4SyjSDh1+A7Tpy5kzDwYSjCnlLbeetdPzbjnTyZE5C2ZOyN8IVyABXnwbvZCs6QK/qBwbphjpVZmcQ4KpYDdo8ql1fdP1jCXcWi3nUsM8xlixeLIHuT45XVeyXQKzEWQx6iREHBRBgMe0FMTFvxAc6eZGu7tksByzv00VtB0ZG6bmp2Mo3/VjbzmKpxj5bbrvZU9NjLyMiRVySZVbDyme5SzC4iXtEAMAiID6gPcYQEPmEOREBAfeA/i4EB1v1iHsRzJ9/PariG7O3RHM+hW0qlbR7xOsFppZxrUsq555EFZAWDeXABH5SUkkf3GDWXmo2u1W0blqk+Ojqlh1Zsf76WEsun3TIzMT+eJieNxddzdLZcBhdixpd9DO4rH5emXWJn02RqKtpgun2GK2iJx7SJRIzETHThpppqm4vPDXn+V8fxuVsZX/GkwoKMbfadYqk7cFT8U7ROeinUcD1JMTEA6rJRcjtIgmADKIFL8+vQNNc1sNTFtWUixRiwCj7RMCghKPvgoiY4p7dSvfq2aNtQvqXK7qtlB9ex1ewslOUXSYntYsyAukxPSZ9+KgGJMmZ96XGd7S1uePReoyjD4NWSFmln8PW7zDxsgo8hLVS7ekydsjiRYzhywckbvwTayL6JmGDV4BvKSn7duqxas8ZJtJJPE8NQ8QUHEeQ8k2qaRl5i2ShVacxYvWTNWfJGwldikXxVnaaTRRi6eyC5UU2qxBKoQ8z0nDRMy3FpLxkfKNRN3+WkWTV+37/UO/wAB2ksl3eo/K7OfUfXXzZwULHsjRzCJjGUefnvYs2DRqzPzwA97Vuim3P7g/lJj7tZ1lduxGbWdjI6yk8y2uFdmSTkHpCewRCHDUhRL8kBEivySyQjtjuIQEY1i0fkJvnLm2jF6pzmv1uXlLK2MrU1G9qmMvWB8zCfOPdnCuLtzTZYmGWoqLphZKXn6dTbDWlTT274DyPv4z1fkmVnY1awSze05atdomWUjItWbiYsbcSsAQj3LN2Ll2/m/KMxK4ICLWPXN2HKmUmu0bzthV+2fxNFnLRfYa/Rl7kpuAI/hImai1IOQi2DeRQbuzSz5+KvtFms9WYmQUS8JSNcgYhhMQ2rhrWNj2RzqM2LNqc5QIc7Zqggc5QHuApjIpkExQN68GEQ59eOdb3TJm+KUjxq2dkIbvIRygk4KU3Al7ilWIcpTcCIdwAA8CIc8COrt/Gnkgydd6KSkYhCwVOJg0kRCKiD2v+kFy+hyJgIh2DAQEiUEUzgo/BFqDtPyWOymxXchvuQuPujvRJyCUpY+6mwUHrQ50qNqSQD0Oa6zLmMtMsCwCBQhX9nuqPlzF2Jtql9h6PAX6o3DE76HyQaaJPRqzTJuPrH8EJgja5RxXTCMkJJqyJMGiZKOei4ayDV8gRFJTuPG/uIy4ffjl2puMK7aGFPvT9m4i5iNoTn4TS98mHztFRvL2ZzGQUDFskIlMiqRpuSbgqm0cul5iX8q1bJN7jR4aJVZqx6kZHnYLCJlmR2TY7NUw8cmUamSFucw8ByY6QiIAHI+ga/PE1yvwJVSwkJEQ5VxAVixUYxjgW493igyQQBT8fywN66oMZumIxDCuUdXBWSGbkIsxlbIqlVlrDWu1VWoE2IrrZComYAjhYHEqZ2kGT7h8Ou/b3URr+y86G3tPYvAHkcOekYll0buHo4+tbtYfMWb9i/iiydqm68Qg1q0FcdXYu5Xli3VNeozOzcTYtvW1o8iedebbcH0OnThWawuW73JdnhodSWSZmAv8YCEWxrUcyE4eKBHJwOUgnMmHuNm6M+aK7QJ20jmOoSbqBqUjZPge3rloI4fO4uGWlz15q6NMKsCvFl0Tx7Z2Zn4AuhIsZAhTCUtmFSHiVljOFYyPVXOcFDrKMmqip1C9vBzKnSFQxg7S8GEwiHaXgQ7Q45EQAwCUwAJTAICAhyAgIcCAh84CHoIa+xzGydWjjaeKrqoellrLpl4LUX3vbD2sgGVh9JBPKwfasjKIdA9/wBSOvwvhD0zNbJuOwbtlLmyFnIo1dbroLJYadXxuNpTjKVVj6uYZ88YnHV8VXltpVdZljzZNf8AnZiFcLoi5jCNuuVcISLsE2lvg4/JlWQXOcoe2K94EJZmzZP+T472EkYN6oQA7hThFjiP8WbUpXU7/mK5/wD7y1T/ADh1HWcbeIimqhVm0awbqlAQKogyaoqFAwdpu06SRDhyXkogBuBKIgICGv1rt0HSR0HKKThFTjvRXTIskftMBg7k1CmIbgwAYO4o8CACHqADqyZPY1X9nTsasf6bstULbanqYZDW0iTJTDoQvs8wpGJ/JF2lJHPfM9OJJ0zlFf1jkvkuUV7ahzEWsJs+Co575PNMqVLYkXRWLMfOTtep+XOvvJcRdRDa4prx4YDvmDbocAIYzz9z/wBplZ/H/wBS0A+fWC/V/wDXek3459MXYx+Yf+mLMPv44/8A0NWrWrFkxA5WbRq0KoIGUBs3RbgcxQ7SicESEAwlD0ATAIgHoHpr5OIqMdqeM6j2LlYSlKKrhm2WUEpee0onVSOftLyPaHPAcjwHrq41d19PtV7Zvl3f61Bp9H6vt8Xequru9R6ae/p4OvTwj17unWOnWcQzXw5FmOR+s8nPpcNctdySMhOwfI5bFvw3snclXyv5uuUdfmXZ3/MHdPD3dk+TtDCvqB5hNhXZ5k+ws3XlJ+yV1vjqrHDkFfbt7IEF4zcwepV4yHXl5gpigJiBH+IHAl5Cuvs06feQt3VVt9vq99hcewtRsDKqIrzEPNyR5p8eJRlXZGikU/YCijGNnUaRcFfG8VV2QCgQUR77gTlk0eJlSdtW7pIhgOVNwgkuQpgASgYpFSHKUwFMIAYAAQARDngR0as2jIgptGzdqmJhOJG6KSBBMPACYSIkIUTCAAHcIdwgAAI+gap8JuVnX8PcoY2qKr9y4Ng8kZrbAqAViKIqMrkBdIhvQyZPbLjkQiekxdeZHw94rmtv2A2bccy65q2AwT8WjUKyLdE33LDHtbkiztPKosJIjOpJIRUHyDj0Ax8gTBKmbl7E2QenvugoXtOeZ2icpTikZZhbDFMX8exnI8kq69osAbP3DtyJzBFS8G/BRwqVwVzyIFBYUw9p6s9hjLRunrtsglvNwthwXiWxwzkgcldRcqtaJSPWKJe4O1doukcB5EPlenPGrYjqMjnpyqvGDJ0oUvYVRy0brnKTkTdpTrJnMBeREe0BAOREeORHXyUhYhXs8WLjlPDTIin4jFqfsSSAQTSJ3om7E0wEQImXghAEQKUOdX1XMgvW4vJ28TFm/Rx9qhZeFsK8XRsEgxPxhTIUeIlGUAPeMy5nTsjpERrc+EBY63u2m4LeixOr7PtOD2fE46xgn5NuvNxC76WV/Vu2BTcn6xNxCjsOiu4Rx9bv85SZTX6qnWztMrNVitjt2gUE5WartfM7DJcqcyBJOSYRBnRURpBCqGRK4FcqQqlA4kBMVC8icMU+p36dRCS9/BSYF5HgePRGJH38ce7Vq4IGEKIGCHiwEBAQEI5kAgICAgICCACAgIAICHqAhyHrr7LxMW6WFw5jmDhcezlZdm2WVHw+AJyookZQezgO35XyeA7eOA1b6G4YjEZOMhitZ9EJULVJ6Iy1h/mKwyqYO8liuzs8PgOPGIflPL1Io7Y65Vs3ILf9505mq7xzi+kbV7PgNhxmRPRsZjIxysRUytezRipjMpWiz8wLIV2TZc+ZrRS7QSfnKRqy9WvJj3Lm7qMxVXDHkWuMIiBoMczbmOoR1e7y9Zy80QhQAQByAyFXhlOwoiRRgqQeRA2u2ZK6OmYcf41ut5+/LUbOFJqM3azVWPrlobuJQtejHEq5i2Dh1MLsknJ0my6bJQzQxDqgQoJE8T5Fmw0PEnWFweMjzuDKAsZczJqZYyoGAwKiqZIVBU7igbvE3cAgAgPIBrkDFKcpiHKU5DlEpymADFMUwcGKYB9BKICICA+ggPA65p5h5ChQw2OxFZdFGMVAWYZKbU3zkgNh9WVRmr5Gec5hZGUS/wDpz4x4pch8Jmq7VtHMLbd+y9rZcpuF47OFKoOQwYavWhT69Wt21My0M16WtGOQBXFqVI40f5uI2GjxXv6IWYuxfL+CJB2IpuE43K9SQOoBSDyDSsXFJumf1MYxQqcgYiXrwdysYB4OIWEtce3iYtoqCzWOYtlQKJQUbs2yCgFNxyAHSSIftEAABL3dogAch6BrkNYvsWWVnMvayiqfofV+ImV/ND4hoKBZsg4Un/WyEGUdn9MinrPXia+UmiX+WmhYXSb+fjZZwfq01Mp8vnGEVJ9t1qvVOtN7I/7HDyrKOLHSa60hCx7PdpppqycSTw00004cNNNNOHDTTTThw00004cNNNNOHDTTTThw00004cNNNNOHDTTTThw00004cNNNNOHDTTTThw00004cf//Z" alt="" />
                            </td>
                            <td style="text-align: right;">
                                <strong>Preventivo # {{ $preventive->preventive_number }}</strong><br>
                                Creazione: {{ date('d/m/Y', strtotime($preventive->creation_date)) }}<br>
                                Scadenza: {{ date('d/m/Y', strtotime($preventive->validity_end_date)) }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="6">
                    <table>
                        <tr>
                            <td>
                                Ristrutturare Casa Italia<br>
                                Via Gramsci Antonio, 6 Int. 4 <br>
                                17100 Savona (SV) <br>
                                P. IVA 01797970090
                            </td>

                            <td style="text-align: right;">
                                {{'App\Models\User'::find($preventive->customer_id)->name}} {{'App\Models\User'::find($preventive->customer_id)->surname}}<br>
                                {{'App\Models\User'::find($preventive->customer_id)->mail}}<br>
                                @if('App\Models\User'::find($preventive->customer_id)->phone != '')
                                {{'App\Models\User'::find($preventive->customer_id)->phone}}
                                <br>
                                @endif
                                {{'App\Models\User'::find($preventive->customer_id)->address_r1}}
                                @if('App\Models\User'::find($preventive->customer_id)->address_r2 != '')
                                <br>
                                {{'App\Models\User'::find($preventive->customer_id)->address_r2}}
                                @endif

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <!-- Headers -->
            <thead>
                <tr class="heading" style="padding:0px;background-color:#eee">
                    <th>Descrizione</th>
                    <th>Quantità</th>
                    <th>Prezzo</th>
                    <th>Sconto</th>
                    <th>IVA</th>
                    <th>Subtotale</th>

                </tr>
            </thead>
            <!-- Main content -->
            @foreach (json_decode($preventive->item_list, true) as $item)
            <tr style="border:1px #eee solid;">
                @if ($item['id'] == -1)
                <td class="table-cell" style="white-space:normal;text-align: justify; text-justify: inter-word;">
                    {{ $item['name_text'] }} @if($item['description'] != '') - {{ $item['description'] }} @endif
                </td>

                <td class="table-cell">
                    {{ $item['qty'] }}
                </td>

                <td class="table-cell">
                    @if (is_numeric($item['UM']))

                    {{ number_format($item['UM'], 2, ",", ".") }} €

                    @else

                    {{$item['UM']}}

                    @endif
                </td>
                <td class=" table-cell">
                    @if (is_numeric($item['discount']))
                    {{ $item['discount'] }}%
                    @else
                    {{$item['discount']}}
                    @endif
                </td>

                <td class="table-cell">
                    @if (is_numeric($item['IVA']))
                    {{ $item['IVA'] }}%
                    @else
                    {{$item['IVA']}}
                    @endif
                </td>

                <td class="table-cell">
                    
                    @if (is_numeric($item['UM']) && is_numeric($item['qty']) && is_numeric($item['discount']))
                        {{number_format($item['UM'] * $item['qty'] - (($item['UM'] * $item['qty']) / 100) * $item['discount'], 2, ',', '.')}}€
                    @else
                    @php

                    if (preg_match("/^[a-zA-Z]+$/", $item['UM']) || preg_match("/^[a-zA-Z]+$/", $item['qty']) || preg_match("/^[a-zA-Z]+$/", $item['discount'])){
                        echo 'N/D';
                    } elseif ($item['UM'] == "" || $item['UM'] == "" || $item['UM'] == "") {
                        echo ' ';
                    }
                    else {
                        if (strpos($item['UM'], ',') != false) {
                            $item['UM'] = str_replace(',', '.', $item['UM']);
                        }
                        if (strpos($item['qty'], ',') != false) {
                            $item['qty'] = str_replace(',', '.', $item['qty']);
                        }
                        if (strpos($item['discount'], ',') != false) {
                            $item['discount'] = str_replace(',', '.', $item['discount']);
                        }
                        echo number_format($item['UM'] * $item['qty'] - (($item['UM'] * $item['qty']) / 100) * $item['discount'], 2, ',', '.');
                    }
                    @endphp
                    @endif
                </td>

                @elseif ($item['id'] == -2)
                <td class="table-cell" style="height:20px !important;" colspan="6"></td>

                @else
                <td class="table-cell" style="white-space: normal;text-align: justify; text-justify: inter-word;">

                    {{ ('App\Models\Product')::find($item['id'])->name . ' - '}}

                    @php
                    if (array_key_exists('description',$item)){
                        echo $item['description'];
                    } else {
                        echo ('App\Models\Product')::find($item['id'])->description;
                    }
                    @endphp
                </td>

                <td class="table-cell">
                    {{ $item['qty'] }}
                    @if (('App\Http\Controllers\ProductController')::GetMeasureUnitName(('App\Models\Product')::find($item['id'])->measure_unit_id) != "€")
                    {{ ('App\Http\Controllers\ProductController')::GetMeasureUnitName(('App\Models\Product')::find($item['id'])->measure_unit_id) }}
                    @endif
                </td>

                <td class="table-cell">
                    @if (is_numeric($item['UM']))
                    {{ number_format($item['UM'], 2, ",", ".") }}
                    @else
                        {{$item['UM']}}
                    @endif
                    €
                </td>


                <td class=" table-cell">
                    {{ $item['discount'] }}%
                </td>

                <td class="table-cell">
                    {{ $item['IVA'] }}%
                </td>

                <td class="table-cell">
                    @if (is_numeric($item['UM']) && is_numeric($item['qty']) && is_numeric($item['discount']))
                        {{number_format($item['UM'] * $item['qty'] - (($item['UM'] * $item['qty']) / 100) * $item['discount'], 2, ',', '.')}}€
                    @else
                    @php

                    if (preg_match("/^[a-zA-Z]+$/", $item['UM']) || preg_match("/^[a-zA-Z]+$/", $item['qty']) || preg_match("/^[a-zA-Z]+$/", $item['discount'])){
                        echo 'N/D';
                    } else {
                        if (strpos($item['UM'], ',') != false) {
                            $item['UM'] = str_replace(',', '.', $item['UM']);
                        }
                        if (strpos($item['qty'], ',') != false) {
                            $item['qty'] = str_replace(',', '.', $item['qty']);
                        }
                        if (strpos($item['discount'], ',') != false) {
                            $item['discount'] = str_replace(',', '.', $item['discount']);
                        }
                        echo number_format($item['UM'] * $item['qty'] - (($item['UM'] * $item['qty']) / 100) * $item['discount'], 2, ',', '.');
                    }
                    @endphp
                    @endif
                </td>
                @endif

            </tr>
            @endforeach

            <!-- Blank row for spacing -->
            <tr class="blank-row">
                <td colspan="6"></td>
            </tr>

            <!-- Total amounts rows -->
            <tr class="totals">
                <td></td>
                <td></td>
                <td colspan="4"><span>Imponibile:</span> <span style="float:right;">
                        {{number_format($preventive->taxable, 2, ',', '.')}}€</span></td>
            </tr>

            <tr class="totals">
                <td></td>
                <td></td>
                <td colspan="4"><span>Iva:</span><span style="float:right;">
                        {{number_format($preventive->taxes, 2, ',', '.')}}€</span></td>
            </tr>
            <tr class="totals">
                <td></td>
                <td></td>
                <td colspan="4"><span>Totale:</span> <span style="float:right;">
                        {{number_format($preventive->total, 2, ',', '.')}}€</span></td>
            </tr>
            <tr class="thankyou">
                <td colspan="6"> <br>Grazie per aver scelto la nostra azienda.<br>Rigers Lamaj</td>
            </tr>
        </table>
    </div>
</body>

</html>