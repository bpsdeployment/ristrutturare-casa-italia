<!--footer-->
<footer class="footer">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-2">
                <ul class="list-unstyled link-list">
                    <li><a href="#">Termini & Condizioni</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <ul class="list-unstyled link-list">
                    <li><a href="https://www.iubenda.com/privacy-policy/54804280/cookie-policy">Cookie policy</a></li>
                </ul>
            </div>

            <div class="col-lg-2">
                <ul class="list-unstyled link-list">
                    <li><a href="https://www.iubenda.com/privacy-policy/54804280">Informativa Privacy</a></li>
                </ul>
            </div>
            <div class="col-lg-3"></div>
        </div>

        <span>&copy; Copyright <?php echo date('Y'); ?>. Tutti i diritti riservati.</span> <br>
        <!-- MS-BPS-07062022 Added following span -->
        <span>Powered by BPS Deployment srl</span>

        <!--
        <ul class="list-inline">
            <li><img src="images/flags/visa.png" alt=""></li>
            <li><img src="images/flags/paypal.png" alt=""></li>
            <li><img src="images/flags/mastercard.png" alt=""></li>
            <li><img src="images/flags/fedex.png" alt=""></li>
        </ul>
        -->
    </div>
</footer>
