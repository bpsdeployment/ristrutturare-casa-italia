<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header', ['NoShow', true])
                <!--page start-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 margin-b-60">
                            <p>Pagamento di {{$total}} €</p>
                            <div class="login-register-box">
                                <!-- Set up a container element for the button -->
                                <div id="paypal-button-container"></div>

                                <!-- Include the PayPal JavaScript SDK -->
                                <script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_SANDBOX_CLIENT_ID') }}&currency=EUR"></script>

                                <script>
                                    paypal.Buttons({
                                        // Sets up the transaction when a payment button is clicked
                                        createOrder: (data, actions) => {
                                            return actions.order.create({
                                                purchase_units: [{
                                                    amount: {
                                                        value: {{ $total }}
                                                    },
                                                    invoice_id: {{$checkout_id}},
                                                    custom_id: {{$checkout_id}}
                                                }],
                                            });
                                        },
                                        // Finalize the transaction after payer approval
                                        onApprove: (data, actions) => {
                                            return actions.order.capture().then(function(orderData) {
                                                $.ajax({
                                                    url: "/checkout/success-transaction",
                                                    type: "POST",
                                                    data: {
                                                        data: orderData,
                                                        checkout_id: "{{$checkout_id}}",
                                                        save_user_info: "{{$saveinfo}}",
                                                        _token: "{{csrf_token()}}"
                                                    },
                                                    success: function(response) {
                                                        window.location.href = "{{ route('landingPaypalPayment')}}";
                                                    },
                                                });
                                            });
                                        }
                                    }).render('#paypal-button-container');
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>

<!doctype html>
