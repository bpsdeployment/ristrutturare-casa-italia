<div class="card">
    <div class="card-header" id="headingOne">
        <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#note{{ $note_id }}"
                aria-expanded="true" aria-controls="note{{ $note_id }}">
                {{ $type }} del {{ date('d-m-Y H:m:s', strtotime($note_date)) }}
            </button>
        </h5>
    </div>

    <div id="note{{ $note_id }}" class="collapse" aria-labelledby="headingOne">
        <div class="card-body">
            {{$note}}
        </div>
    </div>
</div>
