<span class="pull-right rating">
    @for ($i = 0; $i < $rating; $i++)
        <i class="material-icons">grade</i>
    @endfor
    @for ($i; $i < 5; $i++)
        <i class="material-icons">star_border</i>
    @endfor
</span>
