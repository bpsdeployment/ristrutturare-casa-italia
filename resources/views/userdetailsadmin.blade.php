<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">
        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Dettagli utente</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-1">
                                @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                                @endif
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <a class="btn btn-primary btn-lg mb-1" href={{ route('insert-user-story', ['id' => $id]) }}>Aggiungi nota</a>
                                    <a class="btn btn-primary btn-lg mb-1" href={{ route('edit-user-data', ['id' => $id]) }}>Modifica dettagli</a>
                                    <a class="btn btn-danger btn-lg mb-1 float-end" id="deleteUserBtn" href={{ route('delete-user', ['id' => $id]) }}>Elimina cliente</a>

                                </div>
                            </div>

                            <div class="row" style="padding-top: 3%;">
                                <div class="col-lg-2"> <b>Nome</b> </div>
                                <div class="col-lg-4">{{ $name }} {{ $surname }}</div>
                                <div class="col-lg-2"> <b>Data registrazione</b> </div>
                                <div class="col-lg-2">{{ date('d-m-Y  H:m:s', strtotime($registration_date)) }}
                                </div>
                            </div>
                            <div class="row" style="padding-top: 3%;">
                                <div class="col-lg-2"> <b>Email</b> </div>
                                <div class="col-lg-4">{{ $mail }}</div>
                                <div class="col-lg-2"> <b>Data ultimo acquisto</b> </div>
                                @if ($last_order_date == 'Mai ordinato')
                                <div class="col-lg-2">{{ $last_order_date }}
                                    @else
                                    <div class="col-lg-2">{{ date('d-m-Y  H:m:s', strtotime($last_order_date)) }}
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 3%;">
                                    <div class="col-lg-2"> <b>Tel.</b> </div>
                                    <div class="col-lg-4">{{ $phone_number }}</div>
                                    <div class="col-lg-2"> <b>Importo totale acquisti</b> </div>
                                    <div class="col-lg-2">{{ $total_spent }} €</div>
                                </div>
                                <div class="row" style="padding-top: 3%;">
                                    <div class="col-lg-2"> <b>Dom.</b> </div>
                                    <div class="col-lg-4">
                                        @if (strlen($addr_1) != 0)
                                        {{ $addr_1 }} <br>
                                        @endif
                                        @if (strlen($addr_2) != 0)
                                        {{ $addr_2 }} <br>
                                        @endif
                                        {{ $city }} {{ $prov }} {{ $cap }} <br>
                                    </div>
                                    <div class="col-lg-2"> <b>Venditore di riferimento</b> </div>
                                    <div class="col-lg-2">
                                        @if ($referent_admin)
                                        {{ $referent_admin->name . ' ' . $referent_admin->surname }}
                                        @else
                                        <p>Da assegnare</p>
                                        @endif
                                    </div>
                                </div>
                                @if ($from_siteground)
                                <div class="row" style="margin-top: 1%;">
                                    <div class="col-lg-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Metratura
                                                    </th>
                                                    <th>
                                                        # bagni
                                                    </th>
                                                    <th>
                                                        Stato cucina
                                                    </th>
                                                    <th>
                                                        Stato bagno
                                                    </th>
                                                    <th>
                                                        Stato riscaldamento
                                                    </th>
                                                    <th>
                                                        Stato condizionatore
                                                    </th>
                                                    <th>
                                                        Stato controsoffitto
                                                    </th>
                                                    <th>
                                                        Stato pavimento
                                                    </th>
                                                    <th>
                                                        Stato budget
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        {{ $mq }}
                                                    </td>
                                                    <td>
                                                        {{ $nr_bagni }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_cucina }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_bagno }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_riscaldamento }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_condizionatore }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_controsoffitto }}
                                                    </td>
                                                    <td>
                                                        {{ $stato_pavimento }}
                                                    </td>
                                                    <td>
                                                        {{ $budget }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                                <div class="row" style="padding-top: 3%;">
                                    <div class="col-lg-2"> <b>Storico ordini</b> </div>
                                    <div class="col-lg-10 col-sm-12">
                                        <div class="accordion" id="accordion_orders">
                                            @if (count($orders) > 0)
                                            @foreach ($orders as $order)
                                            @include('orderhistorycard', [
                                            'order_date' => $order->created_at,
                                            'order_id' => $order->id,
                                            'order_total' => $order->total,
                                            'items' => json_decode($order->item_list),
                                            ])
                                            @endforeach
                                            @else
                                            {{ 'Questo utente non ha mai effettuato un ordine.' }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 3%;">
                                    <div class="col-lg-2"> <b>Note</b> </div>
                                    <div class="col-lg-10 col-sm-12">
                                        <div class="accordion" id="accordion_notes">
                                            @if (count($notes) > 0)
                                            @foreach ($notes as $note)
                                            @include('notehistorycard', [
                                            'note_date' => $note->contact_datetime,
                                            'type' => $note->type,
                                            'note_id' => $note->id,
                                            'note' => $note->note,
                                            ])
                                            @endforeach
                                            @else
                                            {{ 'Questo utente non ha note.' }}
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include('adminfooter')
            </div>
            <!-- content-wrapper ends -->

            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

<script>
    /*
    $('#deleteUserBtn').click(function() {
        alert('Sei sicuro di voler eliminare il cliente?');
    })
    
    */
</script>

</html>