@php
Log::Debug('editing ' . $admin);
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica dati admin</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">

                        @if (Session::has('error_message'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('error_message') }}
                        </div>
                        @endif
                        @if (Session::has('message'))
                        <div class="alert alert-info" role="alert">
                            {{ Session::get('message') }}
                        </div>
                        @endif

                        <div class="card-body">
                            <form action={{ route('save-edited-admin-data', ['id' => $admin->id]) }} method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Nome
                                            {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'name']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('name', $admin->name, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Cognome
                                            {!! Form::label('Cognome', null, ['class' => 'sr-only', 'for' => 'surname']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('surname', $admin->surname, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Email
                                            {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'email']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::email('email', $admin->mail, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Telefono
                                            {!! Form::label('Telefono', null, ['class' => 'sr-only', 'for' => 'tel']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('tel', $admin->tel, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Indirizzo
                                            {!! Form::label('Indirizzo', null, ['class' => 'sr-only', 'for' => 'address']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('address', $admin->address, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Codice Fiscale
                                            {!! Form::label('Codice Fiscale', null, ['class' => 'sr-only', 'for' => 'codice_fiscale']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('codice_fiscale', $admin->codice_fiscale, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            P. IVA
                                            {!! Form::label('P. IVA', null, ['class' => 'sr-only', 'for' => 'p_iva']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('p_iva', $admin->p_iva, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Numero iscrizione albo
                                            {!! Form::label('Numero iscrizione albo', null, ['class' => 'sr-only', 'for' => 'n_iscr_albo']) !!}
                                        </div>
                                        <div class="col-lg-10">
                                            {!! Form::text('n_iscr_albo', $admin->n_iscr_albo, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Prodotti
                                                    </div>
                                                    <ul class="list-group list-group-flush">
                                                        <li class="list-group-item">
                                                            Leggi
                                                            <div style="float:right;">{{Form::Checkbox('r_products_auth', null, $admin->r_products_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Scrivi
                                                            <div style="float:right;">{{Form::Checkbox('c_products_auth', null, $admin->c_products_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Elimina
                                                            <div style="float:right;">{{Form::Checkbox('d_products_auth', null, $admin->d_products_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Crea
                                                            <div style="float:right;">{{Form::Checkbox('u_products_auth', null, $admin->u_products_auth)}}</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Categorie
                                                    </div>
                                                    <ul class="list-group list-group-flush">
                                                        <li class="list-group-item">
                                                            Leggi
                                                            <div style="float:right;">{{Form::Checkbox('r_categories_auth', null, $admin->r_categories_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Scrivi
                                                            <div style="float:right;">{{Form::Checkbox('c_categories_auth', null, $admin->c_categories_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Elimina
                                                            <div style="float:right;">{{Form::Checkbox('d_categories_auth', null, $admin->d_categories_auth)}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            Crea
                                                            <div style="float:right;">{{Form::Checkbox('u_categories_auth', null, $admin->u_categories_auth)}}</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Prima pagina
                                                    </div>
                                                    <ul class="list-group list-group-flush">
                                                        <li class="list-group-item">
                                                            Scrivi
                                                            <div style="float:right;">{{Form::Checkbox('u_first_page_auth', null, $admin->u_first_page_auth)}}</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-lg btn-primary">Salva</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>


    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>