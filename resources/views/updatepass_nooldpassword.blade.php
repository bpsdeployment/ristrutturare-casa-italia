{!! Form::open(['method' => 'POST', 'route' => 'updatePassword_noOldPassword']) !!}
    <div class="row">
        <div class="form-group">
            <div class="col-xs-6">
                <!--
                                            <i class="material-icons icon">lock</i>
                                            -->
                {!! Form::label('Password', null, ['for' => 'newPassword']) !!}
                <input type="password" name="newPassword" minlength="8" class="form-control" required>
            </div>
            <div class="col-xs-6">
                {!! Form::label('Verifica Passord', null, ['for' => 'newPasswordCheck']) !!}
                <input type="password" name="newPasswordCheck" minlength="8" class="form-control" required>
            </div>
        </div>
        <!--//form-group-->
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-xs-12">
                <br>
                <button class="btn btn-lg btn-primary" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Salva</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
