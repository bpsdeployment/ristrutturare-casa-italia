<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="space-50"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 margin-b-60">
                            <div class="login-register-box">
                                <div>
                                    <p>Se hai dimenticato la tua password puoi inserire qui la tua email. <br>
                                        Riceverai una mail con le istruzioni per sceglierne una nuova.</p>
                                </div>
                                {!! Form::open(['method' => 'POST', 'route' => 'password.email']) !!}
                                <div class="form-group">
                                    <i class="material-icons icon">email</i>
                                    {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'email']) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'true']) !!}
                                </div>
                                <!--//form-group-->
                                <button type="submit" class="btn btn-lg btn-block btn-primary">Recupera
                                    password</button>
                                {!! Form::close() !!}
                                @if (Session::has('message'))
                                    <div class="alert alert-info" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger" role="alert">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        @include('footer')
        <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>
</html>
