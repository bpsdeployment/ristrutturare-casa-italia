@php
$admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
$view_products = false;
$add_product = false;
if ($admin) {
$view_products = $admin->r_products_auth;
$add_product = $admin->c_products_auth;}
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <style>
        
        button {
            white-space: nowrap !important;
        }

        @media (max-width: 750px) {
            button {
                white-space: normal !important;
            }
        }
    </style>
</head>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Tabella prodotti</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if($view_products == 1)
                            <div class="row" style="padding-bottom: 1%;">
                                @if (Session::has('message'))
                                <div class="row">
                                    <div class="alert alert-info" role="alert">
                                        <div class="col-lg-4"> {{ Session::get('message') }} </div>
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-2">
                                            @if (Session::has('id'))
                                            <a href={{ route('undoDeleteProduct', Session::get('id')) }}>Ripristina</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="col">
                                    <form action={{ route('change-price-multiple') }}>
                                        <button class="btn btn-outline-warning btn-block mb-1">Modifica prezzi</button>
                                    </form>
                                </div>

                                <div class="col">
                                    <form action={{ route('insert-product') }}>
                                        <button <?php if ($add_product == 0) echo ' disabled'; ?> class="btn btn-primary btn-block mb-1">Aggiungi prodotto</button>
                                    </form>
                                </div>

                                <div class="col">
                                    <form action={{ route('insert-service') }}>
                                        <button class="btn btn-primary btn-block mb-1">Aggiungi servizio</button>
                                    </form>
                                </div>

                                <div class="col">
                                    <form action={{ route('measure-unit-view') }}>
                                        <button class="btn btn-primary mb-1 btn-block ">Unità di misura</button>
                                    </form>
                                </div>

                                <div class="col">
                                    <form action={{ route('material-types-view') }}>
                                        <button class="btn btn-primary btn-block mb-1">Materiali</button>
                                    </form>
                                </div>

                            </div>
                            <div class="row">

                                <div class="table-responsive">

                                    <table id="datatable" class="table table-bordered yajra-datatable" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Categoria</th>
                                                <th>Prezzo di acquisto(€)</th>
                                                <th>Prezzo di listino(€)</th>
                                                <th># Articoli venduti</th>
                                                <th>Descrizione</th>
                                                <th>Azioni</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @else
                            Non sei autorizzato a vedere i prodotti.
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- End custom js for this page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(function() {

        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('fillTable') }}",
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'type',
                    name: 'type'
                },
                {
                    data: 'buy_price',
                    name: 'buy_price'
                },
                {
                    data: 'listing_price',
                    name: 'listing_price'
                },
                {
                    data: 'units_sold',
                    name: 'units_sold'
                },
                {
                    data: 'description',
                    name: 'description',
                    visible: false,
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Italian.json'
            }
        });

    });
</script>


</html>