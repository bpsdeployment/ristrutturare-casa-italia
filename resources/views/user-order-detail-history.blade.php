<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="container">
                    <div class="space-20"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="cart-totals">
                                <div class="cart-totals-title">
                                    <h3>Elenco prodotti ordine #{{$order_id}} del {{$date}}</h3>
                                </div>
                                <div class="cart-totals-fields">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><strong>ID</strong></td>
                                                <!--
                                                MS-BPS-09062022
                                                <td><strong>Indirizzo spedizione</strong></td>
                                                <td><strong>Stato ordine</strong></td>
                                                -->
                                                <td><strong>Nome</strong></td>
                                                <td><strong>Prezzo</strong></td>
                                                <td><strong>Quantità</strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                            @foreach ($items as $item)
                                                <tr>
                                                    <td><span id="ItemId">{{ $item->id }}</span></td>
                                                    <td><span id="ItemName">{{ $item->name }}</span></td>
                                                    <td><span id="ItemPrice">{{ $item->price}}€</span></td>
                                                    <td><span id="ItemQuantity">{{ $item->qty}}</span></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="space-10"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    Totale: {{$order_amout}}€
                                </div>
                            </div>

                            <div class="space-30"></div>
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
