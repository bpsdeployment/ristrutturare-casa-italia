<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">{{ $order_memo }}</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                            <div class="alert alert-info" role="alert">
                                {{ Session::get('message') }}
                            </div>
                            @endif

                            <div class="row mb-2" id="customer-details">
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-lg-3"><b>Data preventivo</b> </div>
                                        <div class="col-lg">{{ date('d-m-Y H:i', strtotime($order_date)) }}</div>
                                    </div>

                                    <div class="row pt-2">
                                        <div class="col-lg-3"><b>ID ordine</b> </div>
                                        <div class="col">{{ $id }}</div>
                                    </div>

                                    <div class="row pt-2">
                                        <div class="col-lg-3"> <b>Nome</b> </div>
                                        <div class="col">
                                            <a href={{ route('show-user-details', $user_id) }}>{{ $name }} {{ $surname }}</a>
                                        </div>
                                    </div>

                                    <div class="row pt-2 ">
                                        <div class="col-lg-3"> <b>Indirizzo email</b> </div>
                                        <div class="col">{{ $mail }}</div>
                                    </div>

                                    @if ($accepted_by_customer)
                                    <div class="row pt-2">
                                        <div class="col-lg-3"> <b>Data accettazione</b> </div>
                                        <div class="col">{{ $acceptance_date }}</div>
                                    </div>
                                    @endif

                                </div>
                                <div class="col-lg-2 mt-2">

                                    <div class="row">
                                        <a href="{{ route('edit-preventive', $id) }}" class="btn btn-lg btn-primary btn-block">Modifica</a>
                                        @if (false)
                                        <a href="{{ route('download-preventive', $id) }}" target="_blank" class="btn btn-lg btn-primary btn-block">Esporta PDF</a>
                                        @endif
                                        <a href="{{ route('print-preventive', $id) }}" target="_blank" class="btn btn-lg btn-primary btn-block">Stampa PDF</a>
                                        @if (!$accepted_by_customer)
                                        <a href="{{ route('accept-preventive', $id) }}" class="btn btn-lg btn-primary btn-block">Segna come accettato</a>
                                        @endif
                                        <a href="{{ route('duplicate-preventive', $id) }}" class="btn btn-lg btn-primary btn-block">Duplica</a>
                                    </div>

                                </div>
                            </div>

                            <div class="row" style="padding-top: 3%;">
                                <div class="col-lg-2"> <b>Prodotti</b> </div>
                                <div class="col-lg-10 col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Nome
                                                    </th>
                                                    <th>
                                                        Quantità
                                                    </th>
                                                    <th>
                                                        Prezzo unitario
                                                    </th>
                                                    <th>
                                                        Sconto
                                                    </th>
                                                    <th>
                                                        IVA
                                                    </th>
                                                    <th>
                                                        Subtotale
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($items as $item)
                                                @if($item['id']!=-1)
                                                @include('adminordertablerow', [
                                                'id' => $item['id'],
                                                'qty' => $item['qty'],
                                                'discount' => $item['discount'],
                                                'IVA' => $item['IVA'],
                                                'price' => $item['UM'],
                                                ])
                                                @else
                                                @include('adminordertablerow', [
                                                'id' => $item['id'],
                                                'name' => $item['name_text'],
                                                'qty' => $item['qty'],
                                                'description' => $item['description'],
                                                'discount' => $item['discount'],
                                                'IVA' => $item['IVA'],
                                                'price' => $item['UM'],
                                                ])
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="row" style="padding-top: 2%;">
                                        <div class="col-lg-6">Imponibile presconto</div>
                                        <div class="col-lg-6"><span style="float: right;">{{ number_format($taxable + $discount , 2, ',','.')}}
                                                €</span></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-6">Sconto</div>
                                        <div class="col-lg-6">
                                            <span style="float: right;"> {{ number_format($discount, 2, ',','.') }} €</span>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-6">Sconto %</div>
                                        <div class="col-lg-6">
                                            <span style="float: right;"> {{ number_format($perc_discount, 2, ',','.') }} %</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">Imponibile scontato</div>
                                        <div class="col-lg-6"><span style="float: right;">{{ number_format($taxable, 2, ',','.') }} €</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">Iva</div>
                                        <div class="col-lg-6"><span style="float: right;">{{ number_format($taxes, 2, ',','.') }} €</span>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-6">Commissione</div>
                                        <div class="col-lg-6">
                                            <span style="float: right;"> {{ number_format($abs_commission, 2, ',','.') }} €</span>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-6">Commissione %</div>
                                        <div class="col-lg-6">
                                            <span style="float: right;"> {{ number_format($perc_commission, 2, ',','.') }} %</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6"> <b>Totale</b> </div>
                                        <div class="col-lg-6"><span style="float: right;"> <b>{{ number_format($total, 2, ',','.') }}
                                                    €</b></span> </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div id="container"></div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        // Data retrieved from https://netmarketshare.com
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Composizione totale ordine'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Componenti',
                colorByPoint: true,
                data: [{
                    name: 'Margine',
                    y: {{$perc_margin}},
                }, {
                    name: 'Costi',
                    y: {{$perc_expenses}}
                }, {
                    name: 'Iva',
                    y: {{$perc_taxes}}
                }, {
                    name: 'Commissioni',
                    y: {{$perc_commission}}
                }, ]
            }]
        });
    </script>

    <!--
                    name: 'Margine',
                    y: {{$perc_margin}},
                }, {
                    name: 'Costi',
                    y: {{$perc_expenses}}
                }, {
                    name: 'Iva',
                    y: {{$perc_taxes}}
                }, {
                    name: 'Commissioni',
                    y: {{$perc_commission}}
                }, ] */

            -->
    <!-- End custom js for this page -->
</body>

</html>