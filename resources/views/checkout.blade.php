<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <script src={{ asset('plugins/jquery/dist/jquery.min.js') }}></script>
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--back to top end-->
                <!--Message-->
                @if (Session::has('message'))
                    <div class="alert alert-info" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                @if (Session::has('errormessage'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('errormessage') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif
                <!--page start-->

                <div class="space-20"></div>
                <div class="container">
                    {!! Form::open(['method' => 'post', 'route' => 'forward.order']) !!}
                    <div class="row">
                        <div class="col-sm-12 col-lg-6 margin-b-10">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Indirizzo di spedizione</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6 col-sm-12">
                                        {!! Form::label('Nome', null, ['for' => 'delivery_name']) !!}
                                        {!! Form::text('delivery_name', Auth::user()->name, ['id' => 'delivery_name', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo nome', 'required' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 col-sm-12">
                                        {!! Form::label('Cognome', null, ['for' => 'delivery_surname']) !!}
                                        {!! Form::text('delivery_surname', Auth::user()->surname, ['id' => 'delivery_surname', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo nome', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        {!! Form::label('Indirizzo', null, ['for' => 'delivery_address_r1']) !!}
                                        {!! Form::text('delivery_address_r1', Auth::user()->address_r1, ['id' => 'delivery_address_r1', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo indirizzo', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        {!! Form::text('delivery_address_r2', Auth::user()->address_r2, ['id' => 'delivery_address_r2', 'class' => 'form-control', 'placeholder' => 'Scala, piano, interno, azienda (facoltativo)', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        {!! Form::label('CAP', null, ['for' => 'delivery_CAP']) !!}
                                        {!! Form::text('delivery_CAP', Auth::user()->CAP, ['id' => 'delivery_CAP', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo CAP', 'required' => 'true']) !!}
                                    </div>
                                    <div class="col-xs-6">
                                        {!! Form::label('Città', null, ['for' => 'delivery_city']) !!}
                                        {!! Form::text('delivery_city', Auth::user()->city, ['id' => 'delivery_city', 'class' => 'form-control', 'placeholder' => 'Inserisci la tua città', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        {!! Form::label('Provincia', null, ['for' => 'delivery_province']) !!}
                                        {!! Form::text('delivery_province', Auth::user()->province, ['id' => 'delivery_province', 'class' => 'form-control', 'placeholder' => 'Inserisci la tua provincia', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        {!! Form::label('Telefono', null, ['for' => 'delivery_phone']) !!}
                                        {!! Form::text('delivery_phone', Auth::user()->phone, ['id' => 'delivery_phone', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo numero di telefono', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button id="CopyAddressButton" type="button" class="btn btn-primary"
                                        id="copyDeliveryToBilling">Stesso
                                        indirizzo di
                                        fatturazione</button>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-lg-10"><label for="saveinfo">Salva le informazioni per il
                                            prossimo acquisto</label><br></div>
                                    <div class="col-lg-2"><input type="checkbox" id="saveinfo" name="saveinfo"
                                            value="1"></div>
                                </div>
                            </div>




                            <!--
                                <div class="row">
                                    <div class="col-sm-12">

                                    </div>
                                </div>
                            -->
                        </div>
                        <div class="col-sm-12 col-lg-6 margin-b-30">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Indirizzo di fatturazione</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6 col-sm-12">
                                        {!! Form::label('Nome', null, ['for' => 'billing_name']) !!}
                                        {!! Form::text('billing_name', Auth::user()->name, ['id' => 'billing_name', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo nome', 'required' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 col-sm-12">
                                        {!! Form::label('Cognome', null, ['for' => 'billing_surname']) !!}
                                        {!! Form::text('billing_surname', Auth::user()->surname, ['id' => 'billing_surname', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo nome', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        {!! Form::label('Indirizzo', null, ['for' => 'billing_address_r1']) !!}
                                        {!! Form::text('billing_address_r1', Auth::user()->address_r1, ['id' => 'billing_address_r1', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo indirizzo stradale', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        {!! Form::text('billing_address_r2', Auth::user()->address_r2, ['id' => 'billing_address_r2', 'class' => 'form-control', 'placeholder' => 'Scala, piano, interno, azienda (facoltativo)', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        {!! Form::label('CAP', null, ['for' => 'billing_CAP']) !!}
                                        {!! Form::text('billing_CAP', Auth::user()->CAP, ['id' => 'billing_CAP', 'class' => 'form-control', 'placeholder' => 'Inserisci il tuo CAP', 'required' => 'true']) !!}
                                    </div>
                                    <div class="col-xs-6">
                                        {!! Form::label('Città', null, ['for' => 'billing_city']) !!}
                                        {!! Form::text('billing_city', Auth::user()->city, ['id' => 'billing_city', 'class' => 'form-control', 'placeholder' => 'Inserisci la tua città', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        {!! Form::label('Provincia', null, ['for' => 'billing_province']) !!}
                                        {!! Form::text('billing_province', Auth::user()->province, ['id' => 'billing_province', 'class' => 'form-control', 'placeholder' => 'Inserisci la tua provincia', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="cart-totals">
                                <div class="cart-totals-title">
                                    <h3>Riepilogo</h3>
                                </div>
                                <div class="cart-totals-fields">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><strong>Nome</strong></td>
                                                <td><strong>Prezzo</strong></td>
                                                <td><strong>Quantità</strong></td>
                                            </tr>
                                            @foreach (Cart::content() as $item)
                                                <tr>
                                                    <td><span id="ItemName">{{ $item->name }}</span>
                                                    </td>
                                                    <td><span id="ItemPrice">{{ $item->price }} €</span>
                                                    </td>
                                                    <td><span id="ItemQuantity">{{ $item->qty }}</span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>Prezzo no IVA</td>
                                                <td><span id="PriceNoTax">{{ Cart::subtotal() }} €
                                                    </span></td>
                                            </tr>
                                            <tr>
                                                <td>Tasse</td>
                                                <td><span id="Tax">{{ Cart::tax() }} €</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-color"><strong>Totale</strong></td>
                                                <td class="text-color"><strong><span class="lead"
                                                            id="TotalPrice">{{ Cart::total() }}
                                                            €</span></strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="space-20"></div>
                        </div>
                    </div>
                    <div class="space-20"></div>
                    {!! Form::submit('Preventivo', ['class' => 'btn btn-primary', 'name' => 'order']) !!}
                    @if(count($any_installation_required_items) == 0)
                        {!! Form::submit('Acquista con spedizione', ['class' => 'btn btn-primary', 'name' => 'payment']) !!}
                    @endif
                    <div class="space-30"></div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
    <!--page end-->
    <!--footer-->
    @include('footer')
    <!--/footer-->
    <!--Common plugins-->
    @include('plugins')
    <script>
        $("#CopyAddressButton").on("click", function() {
            $.each(["address_r1", "address_r2", "CAP", "city", "province"], function(_, item) {
                $('#billing_' + item).val($('#delivery_' + item).val());
            });
        });
    </script>

</body>

</html>
