<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica prima pagina</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if ($show_categories)
                                <h3>Categorie in primo piano</h3>
                                <p>Seleziona un'altra categoria e premi conferma per sostituire quella categoria in
                                    primo
                                    piano nella homepage.</p>
                                <div class="row">
                                    @for ($i = 0; $i < 3; $i++)
                                        @include('first_page_category', [
                                            'category' => $first_page_categories[$i],
                                        ])
                                    @endfor
                                </div>
                                <div class="row">
                                    @for ($i = 3; $i < 6; $i++)
                                        @include('first_page_category', [
                                            'category' => $first_page_categories[$i],
                                        ])
                                    @endfor
                                </div>
                                <hr>
                            @else
                                <div class="alert alert-info" role="alert">
                                    Per impostare l'ordine delle categorie nella prima pagina è necessario inserirne
                                    almeno 6.
                                </div>
                            @endif
                            @if ($show_products)
                                <h3>Prodotti in primo piano</h3>
                                <p>Seleziona un'altro prodotto e premi conferma per sostituire quel prodotto in primo
                                    piano nella homepage.</p>
                                <div class="row">
                                    @for ($i = 0; $i < 4; $i++)
                                        @include('first_page_product', [
                                            'product' => $first_page_products[$i],
                                            'products' => $product_select,
                                        ])
                                    @endfor
                                </div>
                                <br>
                                <div class="row">
                                    @for ($i = 4; $i < 8; $i++)
                                        @include('first_page_product', [
                                            'product' => $first_page_products[$i],
                                            'products' => $product_select,
                                        ])
                                    @endfor
                                </div>
                            @else
                                <div class="alert alert-info" role="alert">
                                    Per impostare l'ordine dei prodotti nella prima pagina è necessario inserirne
                                    almeno 8.
                                </div>
                            @endif
                            <h3>Testi, informazioni e link</h3>
                            {!! Form::open(['method' => 'POST', 'route' => 'save-homepage-info']) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Titolo homepage pubblica
                                        {!! Form::label('Titolo homepage pubblica', null, ['class' => 'sr-only', 'for' => 'title_homepage']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('title_homepage', $homepage_info->title_homepage, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                        ]) !!} </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Sottotitolo homepage pubblica
                                        {!! Form::label('Sottotitolo homepage pubblica', null, ['class' => 'sr-only', 'for' => 'subtitle_homepage']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('subtitle_homepage', $homepage_info->subtitle_homepage, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                        ]) !!} </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Testo homepage pubblica
                                        {!! Form::label('Testo homepage pubblica', null, ['class' => 'sr-only', 'for' => 'homepage_text']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('homepage_text', $homepage_info->homepage_text, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                        ]) !!} </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        About footer pubblico
                                        {!! Form::label('Testo homepage pubblico', null, ['class' => 'sr-only', 'for' => 'footer_text']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('footer_text', $homepage_info->footer_text, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                        ]) !!} </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Mail supporto
                                        {!! Form::label('Mail supporto', null, ['class' => 'sr-only', 'for' => 'support_mail']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('support_mail', $homepage_info->support_mail, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Telefono supporto
                                        {!! Form::label('Telefono supporto', null, ['class' => 'sr-only', 'for' => 'support_phone']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('support_phone', $homepage_info->support_phone, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Link informativa privacy
                                        {!! Form::label('Link informativa privacy', null, ['class' => 'sr-only', 'for' => 'privacy_info_link']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('privacy_info_link', $homepage_info->privacy_info_link, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Link termini e condizioni
                                        {!! Form::label('Link termini e condizioni', null, ['class' => 'sr-only', 'for' => 't_t_link']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('t_t_link', $homepage_info->t_t_link, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Link cookie policy
                                        {!! Form::label('Link cookie policy', null, ['class' => 'sr-only', 'for' => 'cookie_policy_link']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('cookie_policy_link', $homepage_info->cookie_policy_link, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary">Aggiorna</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
