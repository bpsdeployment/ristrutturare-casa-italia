<div class="modal fade bd-example-modal-lg" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Aggiungi cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ml-3 mr-3">

                <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">

                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="name">Nome</label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="text" name="name" id="name">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="surname">Cognome</label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="text" name="surname" id="surname">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="mail">Email</label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="email" name="mail" id="mail">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="tel">Telefono</label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="text" name="tel" id="tel">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class="row">
                        <div class="col-2">
                            <label for="addr_r1">Indirizzo</label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="text" name="addr_r1" id="addr_r1">
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="row">
                        <div class="col-2">
                            <label for="addr_r2"></label>
                        </div>
                        <div class="col-10">
                            <input class="form-control input-lg" type="text" name="addr_r2" id="addr_r2">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="city">Citta'</label>
                        </div>
                        <div class="col-3">
                            <input class="form-control input-lg" type="text" name="city" id="city">
                        </div>

                        <div class="col-1">
                            <label for="prov">Prov.</label>
                        </div>
                        <div class="col-2">
                            <input class="form-control input-lg" type="text" name="prov" id="prov">
                        </div>

                        <div class="col-1">
                            <label for="cap">CAP</label>
                        </div>
                        <div class="col-3">
                            <input class="form-control input-lg" type="text" name="cap" id="cap">
                        </div>
                    </div>
                </div>

                @if(Session::get('is_site_master', 0))
                <div class="form-group ">
                    <div class="row">
                        <div class="col-2">
                            <label for="admin_ref">Assegna a</label>
                        </div>
                        <div class="col-10">
                            <select class="input-lg" type="text" name="admin_ref" id="admin_ref" style="float:left;">
                                <?php
                                if (isset($admin_list)) {

                                    foreach ($admin_list as $admin) {
                                        echo '<option>' .
                                            $admin .
                                            '</option>';
                                    };
                                } else {
                                    echo '<option>N/A</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                @endif

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="AddUserBtn" data-dismiss="modal">Inserisci</button>
            </div>

        </div>
    </div>
</div>