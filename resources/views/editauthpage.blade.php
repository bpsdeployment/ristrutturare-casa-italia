<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica autorizzazioni utente</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if (Session::has('error_message'))
                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('error_message') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'editUserAuth']) !!}
                            {{ Form::hidden('id', $id) }}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Prodotti
                                        <ul>
                                            <li> Visualizzare la lista prodotti</li>
                                            <li> Aggiungere nuovi prodotti</li>
                                            <li> Modificare i prodotti esistenti</li>
                                            <li> Cancellare i prodotti esistenti</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2">
                                        {!! Form::select('product_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $product_current_auth]) !!}
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Categorie
                                        <ul>
                                            <li> Visualizzare la lista categorie</li>
                                            <li> Aggiungere nuove categorie</li>
                                            <li> Modificare le categorie esistenti</li>
                                            <li> Cancellare le categorie esistenti</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        {!! Form::select('category_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $categories_current_auth]) !!}
                                    </div>
                                </div>
                            </div>

                            <!-- MS-BPS-08062022 -->
                            <div class="form-group" style="display:none;">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Richieste di contatto
                                        <ul>
                                            <li> Visualizzare la lista delle richieste di contatto</li>
                                            <li> Rispondere alle richieste di contatto</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        {!! Form::select('contact_request_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $contact_current_auth]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="display:none;">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Coupon
                                        <ul>
                                            <li> Visualizzare la lista dei coupon</li>
                                            <li> Creare nuovi coupon</li>
                                            <li> Cancellare coupon</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        {!! Form::select('edit_coupons_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $coupon_current_auth]) !!}
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Modifica prima pagina
                                        <ul>
                                            <li> Modificare i testi e i link della home page pubblica</li>
                                            <li> Modificare i prodotti in primo piano della home page privata</li>
                                            <li> Modificare le categorie in primo piano della home page privata</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        {!! Form::select('first_page_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $current_first_page]) !!}
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-8">
                                        Gestione ordini
                                        <ul>
                                            <li> Visualizzare la lista degli ordini</li>
                                            <li> Modificare lo stato degli ordini</li>
                                            <li> Contattare i clienti che hanno effettuato ordini</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        {!! Form::select('orders_auth', ['true' => 'Autorizzo', 'false' => 'Non autorizzo'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => $current_order_auth]) !!}
                                    </div>
                                </div>
                            </div>

                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary" onclick="return confirm('Sei sicuro di voler aggiornare le autorizzazioni?')" >Aggiorna</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
