<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia  - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Rispondi alla richiesta di contatto</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if (Session::has('error_message'))
                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('error_message') }}
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1"> <b>Nome</b>
                                    </div>
                                    <div class="col-lg-11">
                                        {{ $name }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1"> <b>Email</b>
                                    </div>
                                    <div class="col-lg-11">
                                        {{ $email }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1"> <b>Oggetto</b>
                                    </div>
                                    <div class="col-lg-11">
                                        {{ $subject }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">  <b>Testo</b>
                                    </div>
                                    <div class="col-lg-11">
                                        {{ $text }}
                                    </div>
                                </div>
                            </div>
                            @if ($last_reply != null)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-1"> <b>Ultima risposta</b>
                                        </div>
                                        <div class="col-lg-11">
                                            {{ $last_reply }}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'email_reply']) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1"> <b>Risposta</b>
                                        {!! Form::label('Risposta', null, ['class' => 'sr-only', 'for' => 'reply_text']) !!}
                                    </div>
                                    <div class="col-lg-11">
                                        {!! Form::textarea('reply_text', null, ['class' => 'form-control', 'rows' => 10, 'placeholder' => 'Testo della tua risposta']) !!}
                                    </div>
                                </div>
                            </div>
                            <!--//form-group-->
                            {{ Form::hidden('id', $id) }}
                            <button type="submit" class="btn btn-lg btn-primary">Invia</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                @include('adminfooter')
            </div>
            <!-- content-wrapper ends -->

            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
