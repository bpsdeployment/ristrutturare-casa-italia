@php
$name = $product->name;
$id = $product->id;
$folder_name = $product->photos_folder;
$thumbnail = 'App\Http\Controllers\ProductController'::GetThumbnailImage($folder_name);
@endphp

<div class="col-lg-3 col-sm-12">
    <h4 class="text-center">{{ $name }}</h4>
    <img src={{ asset('images/products_images/' . $folder_name . '/' . $thumbnail) }} alt="" class="img-fluid">
    {!! Form::open(['method' => 'POST', 'route' => 'showNewFirstPageProducts']) !!}
    {!! Form::select('new_product_id', $products, ['class' => 'form-control', 'placeholder' => $id]) !!}
    <input name="old_product_id" value="{{$id}}" hidden>
    <button type="submit">Conferma</button>
    {!! Form::close() !!}
</div>
