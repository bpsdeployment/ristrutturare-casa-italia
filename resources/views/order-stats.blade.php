<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.3.3/echarts.min.js"
        integrity="sha512-2L0h0GhoIHQEjti/1KwfjcbyaTHy+hPPhE1o5wTCmviYcPO/TD9oZvUxFQtWvBkCSTIpt+fjsx1CCx6ekb51gw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Statistiche vendite {{ $targetYear }}</h3>
                    <form id="change-year" action={{ route('preventive-charts') }}>
                        <select id="select-year" name="targetYear" class="btn btn-lg btn-primary">
                            @foreach ($year_list as $year)
                                @if ($year == $targetYear)
                                    <option value="{{ $year }}" selected>{{ $year }}</option>
                                @else
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endif
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div id="bar-chart-yearly-income"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div id="chart-margin-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div id="chart-preventive-count"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>
        $("#select-year").change(function() {
            $("#change-year").submit();
        });

        $(function() {
            Highcharts.chart('bar-chart-yearly-income', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Fatturato imponibile'
                },
                xAxis: {
                    categories: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio',
                        'Agosto',
                        'Settembre',
                        'Ottobre', 'Novembre', 'Dicembre'
                    ]
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '€ ordini'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key} Marks</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: <?= $yearly_income_data ?>
            });
        });
        const chart = Highcharts.chart('chart-preventive-count', {
            chart: {
                type: 'line',
            },
            title: {
                text: 'Preventivi creati'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto',
                    'Settembre',
                    'Ottobre', 'Novembre', 'Dicembre'
                ],
            },
            yAxis: {
                title: {
                    text: 'Numero'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: <?= $preventive_count_data ?>
        });
        Highcharts.chart('chart-margin-data', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Margine sugli ordini'
            },
            xAxis: [{
                categories: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio',
                    'Agosto',
                    'Settembre',
                    'Ottobre', 'Novembre', 'Dicembre'
                ],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                title: {
                    text: '€',
                }
            }, { // Secondary yAxis
                title: {
                    text: '%',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            series: [{
                name: 'Margine relativo',
                type: 'column',
                yAxis: 1,
                data: {{ $relative_current_year_margin }},
                tooltip: {
                    valueSuffix: '%'
                }

            }, {
                name: 'Margine assoluto',
                type: 'spline',
                data: {{ $absolute_current_year_margin }},
                tooltip: {
                    valueSuffix: '€'
                }
            }]
        });
    </script>

</body>


</html>
