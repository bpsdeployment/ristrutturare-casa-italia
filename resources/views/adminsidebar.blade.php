@php
$is_site_master = Session::get('is_site_master', 0);
$admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
if ($admin) {
$product_auth = $admin->r_products_auth;
$categories_auth = $admin->r_categories_auth;
$contact_reply_auth = $admin->reply_contact_requests;
$firstpage_edit_auth = $admin->u_first_page_auth;
$coupon_auth = $admin->edit_coupons;
$order_auth = $admin->edit_orders;
} else {
$product_auth = false;
$categories_auth = false;
$contact_reply_auth = false;
$firstpage_edit_auth = false;
$coupon_auth = false;
$order_auth = false;
}

if ($admin->mail == 'rigerslamaj83@gmail.com'){
$is_site_master = true;
}

@endphp


<style>
    /* Dropdown Button */
    .dropbtn {
        background-color: #ffffff;
        color: rgb(0, 0, 0);
        padding: 16px;
        font-size: 16px;
        border: none;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown {
        position: relative;
        display: inline-block;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

    /* Links inside the dropdown */
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    /* Change color of dropdown links on hover */
    .dropdown-content a:hover {
        background-color: #ddd;
    }

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-content {
        display: block;
    }


</style>

@include('common.adminnavbar')

<div class="page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
            <li class="nav-item nav-profile" style="padding-bottom: 10%;">
                <a class="nav-link">
                    <div class="d-flex flex-column">
                        <span class="font-weight-bold mb-2">{{ $admin->name }}
                            {{ $admin->surname }}</span>
                        <span class="text-secondary text-small">
                            @if ($admin->mail == 'rigerslamaj83@gmail.com')
                            {{ 'Amministratore' }}
                            @else
                            {{ 'Operatore' }}
                            @endif
                        </span>

                    </div>
                </a>
                <a href={{route('index')}}>Torna al sito</a>
            </li>

            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('calendar-page') }}><button class="dropbtn nav-link">Calendario</button></a>
                </div>
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('user-view') }}><button class="dropbtn nav-link">Gestione clienti</button></a>
                </div>
            </li>
            @if ($is_site_master)
            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('admin-view') }}><button class="dropbtn nav-link">Gestione admin</button></a>
                </div>
            </li>
            @endif
            @if ($is_site_master or $product_auth)
            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('products-table') }}><button class="dropbtn nav-link">Prodotti</button></a>
                </div>
            </li>
            @endif

            @if ($is_site_master or $categories_auth)
            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('categories-table') }}><button class="dropbtn nav-link">Categorie</button></a>
                </div>
            </li>
            @endif


            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('orders-table') }}><button class="dropbtn nav-link">Preventivi</button></a>
                </div>
            </li>

            @if ($is_site_master or $firstpage_edit_auth)
            <li class="nav-item">
                <div class="dropdown">
                    <a href={{ route('editFirstPage') }}><button class="dropbtn nav-link">Impostazioni prima pagina</button></a>
                </div>
            </li>
            @endif
        </ul>
    </nav>

    