<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="plugins/owl-carousel/assets/owl.theme.default.css" rel="stylesheet">

    <link href="plugins/icheck/skins/minimal/blue.css" rel="stylesheet">
    <!--master slider-->
    <link href="plugins/masterslider/style/masterslider.css" rel="stylesheet">
    <link href="plugins/masterslider/skins/default/style.css" rel='stylesheet'>
    <!--template css-->
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--back to top-->
    <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
    <!--back to top end-->
    <!--===========================start Header===========================-->
    <!--===========================start Header===========================-->
    <!--top bar-->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 hidden-xs">
                </div>
                <div class="col-sm-6">
                    <ul class="list-inline pull-right">
                        @if (Session::get('is_admin'))
                            <li><a href={{ route('admin.page') }}><i class="material-icons">account_box</i>
                                    {{ Session::get('admin_email') }}</a></li>
                            <li><a href={{ route('logout') }}><i class="material-icons">logout</i>Logout</a></li>
                        @else
                            <li><a href={{ route('login') }}><i class="material-icons">perm_identity</i>Login</a></li>
                        @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--end top bar-->
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top yamm sticky-header">
        <div class="container">
            <div class="pull-right">
                <ul class="right-icon-nav nav navbar-nav list-inline">
                </ul>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="" style="float: left;" href={{ route('index') }}><img
                        src={{ asset('images/logo_header.png') }} alt=""></a>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>


    <!--===========================End Header===========================-->


    <!--===========================End Header===========================-->
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="text-center">{{$frontpage_texts->title_homepage}}</h3>
                <h6 class="text-muted text-center margin-b-30">{{$frontpage_texts->subtitle_homepage}}</h6>
                <p>
                    {{$frontpage_texts->homepage_text}}
                </p>
            </div>
        </div>
    </div>
    <div class="space-20"></div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 margin-b-60">
            <div class="login-register-box">
                @if (Session::has('message'))
                    <div class="alert alert-info" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                {!! Form::open(['method' => 'POST', 'route' => 'store.customer']) !!}
                <div class="form-group">
                    <i class="material-icons icon">person</i>
                    {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'name']) !!}
                    {!! Form::text('name', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Nome',
                        'required' => 'true',
                        'autocomplete' => 'on',
                    ]) !!}
                </div>

                <div class="form-group">
                    <i class="material-icons icon">person</i>
                    {!! Form::label('Cognome', null, ['class' => 'sr-only', 'for' => 'surname']) !!}
                    {!! Form::text('surname', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Cognome',
                        'required' => 'true',
                        'autocomplete' => 'on',
                    ]) !!}
                </div>

                <div class="form-group">
                    <i class="material-icons icon">email</i>
                    {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'register-email']) !!}
                    {!! Form::email('mail', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Email',
                        'required' => 'true',
                        'autocomplete' => 'on',
                    ]) !!}
                </div>

                <div class="form-group">
                    <i class="material-icons icon">phone</i>
                    {!! Form::label('Telefono', null, ['class' => 'sr-only', 'for' => 'phone_number']) !!}
                    <input type="tel" name="phone_number" class="form-control" required autocomplete="on"
                        placeholder="Telefono">
                </div>

                <div class="form-group">
                    {!! Form::label('Provincia', null, ['class' => 'sr-only', 'for' => 'prov']) !!}
                    {!! Form::select(
                        'prov',
                        ['Savona' => 'Savona', 'Genova' => 'Genova', 'Imperia' => 'Imperia', 'La Spezia' => 'La Spezia'],
                        ['class' => 'form-control', 'required' => 'true'],
                    ) !!}

                </div>

                <div class="form-group">
                    <div class="clearfix">
                        <div class="checkbox remember pull-left">
                            <label>
                                {!! Form::checkbox('t&t-consent', null, false, ['class' => 'icheck', 'required' => 'true']) !!}
                                {!! Form::label(
                                    'Accetto termini e condizioni del servizio',
                                    null,
                                    ['class' => 'icheck', 'for' => 't&t-consent'],
                                    false,
                                ) !!} </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="clearfix">
                        <div class="checkbox remember pull-left">
                            <label>
                                {!! Form::checkbox('gdpr_conset', null, false, ['class' => 'icheck', 'required' => 'true']) !!}
                                {!! Form::label('Acconsento al trattamento dei dati personali', null, [
                                    'class' => 'icheck',
                                    'for' => 'gdpr-consent',
                                ]) !!}
                            </label>
                        </div>
                    </div>
                </div>
                {!! app('captcha')->display() !!}
            </div>
            <button type="submit" class="btn btn-lg btn-block btn-primary">Invia una richiesta di contatto</button>
            {!! Form::close() !!}
            @if (count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif


        </div>
    </div>
    <div class="space-50"></div>
    <!--footer-->
    <!--footer-->
    @include('public-footer')

    <!--/footer-->
    <!--Common plugins-->
    <script src="plugins/jquery/dist/jquery.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/pace/pace.min.js"></script>
    <script src="plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/sticky/jquery.sticky.js"></script>
    <script src="plugins/icheck/icheck.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/boland.custom.js"></script>
    <!--page template scripts-->
    <script src="plugins/masterslider/masterslider.min.js"></script>
    <script type="text/javascript">
        var slider = new MasterSlider();

        slider.control('arrows');
        slider.control('bullets', {
            autohide: false,
            align: 'bottom',
            margin: 10
        });

        slider.setup('masterslider', {
            width: 490,
            height: 440,
            layout: 'partialview',
            space: 5,
            view: 'basic',
            loop: true,
            filters: {
                grayscale: 1,
                contrast: 1.5
            }
        });
    </script>
</body>

</html>
