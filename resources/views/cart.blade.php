<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <style>
        /* ensures the increment/decrement arrows always display */
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            opacity: 1;
        }
    </style>
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header', ['NoShow' => true])

                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--Message-->
                @if (Session::has('message'))
                    <div class="alert alert-info" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                @if (Session::has('errormessage'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('errormessage') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                @endif

                <!--page start-->
                {!! Form::open(['method' => 'POST', 'route' => 'fill-preventive-rows']) !!}
                <div class="space-60"></div>
                <div class="container">
                    <div class="table-responsive">
                        <table class="table table-bordered cart-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nome Prodotto</th>
                                    <th>Prezzo</th>
                                    <th>Quantita</th>
                                    <th>Rimuovi</th>
                                </tr>
                            </thead>
                            <tbody id="Table-cart">
                                @php
                                    $counter = 0;
                                @endphp
                                @foreach (Cart::content() as $item)
                                    <tr>
                                        @include('cart-content', [
                                            'name' => $item->name,
                                            'price' => $item->price,
                                            'qty' => $item->qty,
                                            'rowId' => $item->rowId,
                                            'Id' => $item->id,
                                            'counter' => $counter,
                                        ])
                                        @php
                                            $counter += 1;
                                        @endphp
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end cart table-->
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="col-sm-6 margin-b-10 ">
                        </div>
                        <div class="col-sm-6 margin-b-10 text-right">
                            <form action="get" id="empty-cart">
                                <button class="btn btn-dark btn-lg" type='submit'>Svuota preventivo</button>
                            </form>
                        </div>
                    </div>
                    <!--<hr>
                    <div class="row">
                        <div class="col-sm-6 margin-b-10">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                    placeholder="Se hai un codice coupon inseriscilo qui"
                                    aria-describedby="basic-addon2">
                                <a href="#" class="btn input-group-addon" id="basic-addon2">Applica Coupon</a>
                            </div>
                        </div>
                    </div>-->
                    <hr>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary">Vai all' editor di preventivi</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
                <div class="space-30"></div>
                <link href={{ asset('css/toastr.css') }} rel="stylesheet">
                <script src={{ asset('js/toastr.js') }}></script>
                <!--page end-->
                <script type="text/javascript">
                    $('#empty-cart').on('submit', function(e) {
                        e.preventDefault();

                        $.ajax({
                            url: "{{ route('emptyCart') }}",
                            type: "GET",
                            success: function(response) {
                                $('#Table-cart').html("");
                                $('#TotalPrice').html('0.00 €');
                                $('#Tax').html('0.00 €');
                                $('#PriceNoTax').html('0.00 €');
                                $('#CartCounter').html(0);
                                //$('#SidebarCart').html("");
                                //$('#SidebarCounter').html(0);
                                toastr.success('Preventivo svuotato!');

                            },
                            error: function(response) {
                                $('#nameErrorMsg').text(response.responseJSON.errors.id);
                                $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                            },
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).on('click', '.deleteButton', function(e) {
                        e.preventDefault();
                        $.ajax({
                            url: "{{ route('deleteItem') }}",
                            type: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                id: e.target.id,
                            },
                            success: function(data) {
                                $('#cart-item_' + e.target.id).remove();
                                $('#TotalPrice').html(data.new_total + ' €');
                                $('#Tax').html(data.new_tax + ' €');
                                $('#PriceNoTax').html(data.new_subtotal + ' €');
                                toastr.success('Rimosso dal preventivo!');
                            },
                            error: function(response) {
                                $('#nameErrorMsg').text(response.responseJSON.errors.id);
                                $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                            },
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(".cart_update").change(function(e) {
                        e.preventDefault();

                        var ele = $(this);
                        var quantity = ele.parents("tr").find(".quantity").val();
                        if (quantity > 0) {
                            $.ajax({
                                url: "{{ route('UpdateQty') }}",
                                method: "POST",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    id: ele.parents("tr").attr("value"),
                                    quantity: quantity,
                                },
                                success: function(data) {
                                    $('#TotalPrice').html(data.new_total + ' €');
                                    $('#Tax').html(data.new_tax + ' €');
                                    $('#PriceNoTax').html(data.new_subtotal + ' €');
                                },
                                error: function(response) {
                                    $('#nameErrorMsg').text(response.responseJSON.errors.id);
                                    $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                                },
                            });
                        }
                    });
                </script>
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')

</body>

</html>
