<link href={{asset('plugins/bootstrap/css/bootstrap.min.css')}} rel="stylesheet">
<link href={{asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}} rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href={{asset('plugins/owl-carousel/assets/owl.carousel.css')}} rel="stylesheet">
<link href={{asset('plugins/owl-carousel/assets/owl.theme.default.css')}} rel="stylesheet">
<link href={{asset('plugins/icheck/skins/minimal/blue.css')}} rel="stylesheet">
<!--master slider-->
<link href={{asset('plugins/masterslider/style/masterslider.css')}} rel="stylesheet">
<link href={{asset('plugins/masterslider/skins/default/style.css')}} rel='stylesheet'>
<!--template css-->
<link href={{asset('css/style.css')}} rel="stylesheet">
<link href={{asset('plugins/font-awesome/css/font-awesome.min.css')}} rel="stylesheet">
<link rel="icon" href={{asset('images/Favicon_RCI.png')}} >
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
