<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="space-20"></div>
                <div class="container">
                    {!! Form::open(['method' => 'POST', 'route' => 'store.order']) !!}
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Informazioni aggiuntive</h2>
                            <p>Ti chiediamo di inserire alcune informazioni sulla tua abitazione e sui lavori di
                                installazione
                                necessari al fine di formulare un preventivo più preciso.</p>
                        </div>
                    </div>
                    <div class="space-20"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Metratura appartamento', null, ['for' => 'mq']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('mq', ['30 mq - 50 mq' => '30 mq - 50 mq', '50 mq - 70 mq' => '50 mq - 70 mq', '70 mq - 90 mq' => '70 mq - 90 mq', '90 mq - 110 mq' => '90 mq - 110 mq', '110 mq - 130 mq' => '110 mq - 130 mq', '130 mq - 150 mq' => '130 mq - 150 mq', 'oltre 160 mq' => 'oltre 160 mq'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => '30 mq - 50 mq']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Numero bagni', null, ['for' => 'n_bathroom']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('n_bathroom', ['1' => '1', '2' => '2', '3' => '3', '4+' => '4+'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => '1']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Impianti cucina', null, ['for' => 'kitchen']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('kitchen', ['Presenti' => 'Presenti', 'Non presenti' => 'Non presenti'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Presenti']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Impianti appartamento', null, ['for' => 'house_installations']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('house_installations', ['Non sono da fare' => 'Non sono da fare', 'Elettrico' => 'Elettrico', 'Idraulico' => 'Idraulico', 'Gas' => 'Gas', 'Elettrico-idraulico' => 'Elettrico-idraulico', 'Sono tutti da fare' => 'Sono tutti da fare'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => '1']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Installare condizionatore', null, ['for' => 'conditioner']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('conditioner', ['No' => 'No', 'Singolo' => 'Singolo', 'Dual split' => 'Dual split', 'Trial split' => 'Trial split', 'Oltre 3 split' => 'Oltre 3 split'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'No']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-10"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-6">
                                {!! Form::label('Realizzare controsoffitto', null, ['for' => 'countertop']) !!}
                            </div>
                            <div class="col-lg-10 col-sm-6">
                                {!! Form::select('countertop', ['Da fare' => 'Da fare', 'Non da fare' => 'Non da fare'], ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Da fare']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="space-20"></div>
                    {!! Form::submit('Richiedi preventivo', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    <div class="space-20"></div>
                </div>



                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
