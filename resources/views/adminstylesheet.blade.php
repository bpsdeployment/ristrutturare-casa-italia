  <!-- plugins:css -->
  <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/adminstyle.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="public/images/Favicon_RCI.png" />

