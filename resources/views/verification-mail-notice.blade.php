<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')

                <!--page start-->

                <div class="container">
                    <img src={{ asset('images/icons/outline_forward_to_inbox_black_48dp.png') }} alt="">
                    <div class="space-30"></div>
                    <h4 class="text-uppercase">Email di conferma inviata</h4>
                    <p>
                        Abbiamo inviato a <strong>{{ Session::get('email-sent') }}</strong> una mail di conferma,
                        accettala per
                        concludere la registrazione e poter accedere al sito.
                        <br>
                        Nel caso in cui la mail di verifica non sia arrivata o sia stata erroneamente cancellata clicca
                        il
                        bottone sottostante per riceverne una nuova.
                    </p>
                    <div class="space-20"></div>
                    <form method="POST" action={{ route('verification.resend') }}>
                        @csrf
                        <button class="btn btn-lg btn-primary" type="submit">Invia di nuovo</button>
                    </form>

                    @if (Session::has('message'))
                        <div class="space-10"></div>
                        <div class="row">
                            <div class="col-lg-3">
                                <p class="alert alert-info text-center">{{ Session::get('message') }}</p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="space-30"></div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
