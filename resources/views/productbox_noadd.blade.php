@php
$thumbnail = ('App\Http\Controllers\ProductController')::GetThumbnailImage($folder_name);
$measure_unit = ('App\Http\Controllers\ProductController')::GetMeasureUnitName($measure_unit_id);
@endphp
<style>
    .image {
        position: relative;
        overflow: hidden;
        padding-bottom: 100%;
    }

    .image img {
        position: absolute;
        max-width: 100%;
        max-height: 100%;
    }

</style>
<div class="product-box">
    <div class="product-thumb">
        @php
            $oneWeekAgo = ('Illuminate\Support\Carbon')::now()->subDays(7);
            $timestring = ('Illuminate\Support\Carbon')::parse(('App\Models\Product')::find($id)->created_at);
            if ($timestring->greaterThan($oneWeekAgo)) {
                $is_new = true;
            } else {
                $is_new = false;
            }
        @endphp
        @if ($is_new)
            <span style="font-size: x-large;" class="label label-success">Nuovo</span>
        @endif

        <div class="image">
            <img src={{ asset('images/products_images/' . $folder_name . '/' . $thumbnail) }} alt=""
                class="img img-responsive full-width" style="display: block">
        </div>
        <div class="product-overlay">
            <span>
                <a class="btn btn-default" href={{ route('showProduct', ['product_id' => $id]) }}>Dettagli</a>
            </span>
        </div>
    </div>
    <!--/product-thumb-->
    <div class="product-desc">
        <div class="col-sm-12">
            <div class="row">
                <span class="product-price pull-right">{{ number_format($price, 2) }} {{$measure_unit}}</span>
                <h5 class="product-name"><a href=>{{ $name }}</a></h5>
            </div>
        </div>

    </div>

    <!--/product-desc-->
</div>
