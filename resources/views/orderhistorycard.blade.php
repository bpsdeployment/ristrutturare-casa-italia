<div class="card">
    <div class="card-header" id="heading_orders">
        <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#order{{ $order_id }}"
                aria-expanded="true" aria-controls="order{{ $order_id }}">
                Ordine #{{ $order_id }} datato {{ date('d-m-Y H:m:s', strtotime($order_date)) }}
            </button>
        </h5>
    </div>

    <div id="order{{ $order_id }}" class="collapse" aria-labelledby="heading_orders">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>
                                Nome
                            </th>
                            <th>
                                Quantità
                            </th>
                            <th>
                                Prezzo unitario
                            </th>
                            <th>
                                Sconto
                            </th>
                            <th>
                                IVA
                            </th>
                            <th>
                                Subtotale
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            @include('adminordertablerow', [
                                'id' => $item->id,
                                'qty' => $item->qty,
                                'discount' => $item->discount,
                                'IVA' => $item->IVA,
                                'price' => $item->UM,
                            ])
                        @endforeach
                    </tbody>
                </table>
            </div>
            <b>Totale: {{ $order_total }} €</b> <br>
            <a href={{ route('manage.order.admin', $order_id) }}>Vai al preventivo</a>
        </div>
    </div>
</div>
