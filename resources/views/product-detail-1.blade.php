<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!--master slider-->
    <link href={{ asset('plugins/masterslider/style/masterslider.css') }} rel="stylesheet">
    <link href={{ asset('plugins/masterslider/skins/default/style.css') }} rel='stylesheet'>
    <link href={{ asset('css/ms-showcase2.css') }} rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src={{ asset('js/simpleCart.js') }}></script>
    <script type="text/javascript" src={{ asset('js/jquery.star.rating.js') }}></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body>
    @inject('product', 'App\Models\Product')
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--back to top end-->
                <div class="space-60"></div>
                <div class="container">
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="alert alert-info" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <div class="col-sm-7 margin-b-40">
                            <!-- master slider template -->
                            <div class="ms-showcase2-template ms-showcase2-vertical">
                                <!-- master slider -->
                                <div class="master-slider ms-skin-default" id="masterslider">
                                    @if (count($photo_folder) > 0)
                                        @foreach ($photo_folder as $file)
                                            @include('slider_component', [
                                                'src' => $file,
                                                'folder_name' => $folder_name,
                                            ])
                                        @endforeach
                                    @else
                                        @include('sliderphotonotfound')
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="item-description">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="item_name">{{ $product_name }}</h3>

                                    </div>
                                </div>
                                <span class="price item_price text-primary">{{ $product_price }}
                                    {{ $measure_unit }}</span>
                                @if ($material_type != 'N/A')
                                    <span class="price item_price text-secondary">Materiale: {{ $material_type }}</span>
                                @endif
                                <p style="overflow-wrap: break-word;">
                                    {{ $product_description }}
                                </p>
                                @if ($need_installation)
                                    <div class="alert alert-info" role="alert">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                Questo prodotto richiede anche l'installazione.
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                {!! Form::open(['method' => 'POST', 'id' => 'addToCartForm', 'name' => $product_id]) !!}
                                <div class="count-input">
                                    <a class="incr-btn" data-action="decrease" href="#">–</a>
                                    {!! Form::text('quantity', 1, ['class' => 'quantity', 'id' => 'quantity_field']) !!}
                                    <a class="incr-btn" data-action="increase" href="#">+</a>
                                </div>
                                <button class="btn btn-lg btn-block btn-primary item_add" type='submit'
                                    value="add to cart">Aggiungi
                                    al
                                    preventivo</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="space-20"></div>
                    @if ($material_type != 'N/A')
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="tabs-nav list-inline text-center" role="tablist">
                                        <li role="presentation" class="active"><a href="#info" aria-controls="info"
                                                role="tab" data-toggle="tab">Informazioni tecniche</a></li>
                                        <!--
                                    <li role="presentation"><a href="#review" aria-controls="review" role="tab"
                                            data-toggle="tab">Recensioni
                                        </a></li>
                                    -->
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="info">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td><strong>Peso:</strong></td>
                                                        <td>{{ $weight }} kg</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Dimensioni:</strong></td>
                                                        <td>{{ $dimensions_x }} x {{ $dimensions_y }} x
                                                            {{ $dimensions_z }} cm
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Altre info:</strong></td>
                                                        <td>{{ $additional_infos }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--end tabs-->
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="space-60"></div>
        <div class="container">
            <h4 class="margin-b-20">Articoli simili</h4>
            <div class="row">
                @if (count($related_products) == 0)
                    <p>Non ci sono prodotti simili a questo nel catalogo.</p>
                @endif
                @foreach ($related_products as $related_product)
                    <div class="col-sm-6 col-lg-3">
                        @include('productbox', [
                            'price' => $related_product->listing_price,
                            'name' => $related_product->name,
                            'id' => $related_product->id,
                            'folder_name' => $related_product->photos_folder,
                            'measure_unit_id' => $related_product->measure_unit_id,
                        ])
                    </div>
                @endforeach
            </div>
        </div>
        <div class="space-60"></div>
        @include('footer')

        <!--Common plugins-->
        @include('plugins')
        <!--page template scripts-->

        <script src={{ asset('plugins/masterslider/masterslider.min.js') }}></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var slider = new MasterSlider();
                slider.control('scrollbar', {
                    dir: 'h'
                });
                slider.control('thumblist', {
                    autohide: false,
                    dir: 'v',
                    arrows: false,
                    align: 'left',
                    width: 127,
                    height: 84,
                    margin: 5,
                    space: 5,
                    hideUnder: 300
                });
                slider.setup('masterslider', {
                    width: 540,
                    height: 586,
                    space: 5
                });
            });
        </script>


        <script type="text/javascript">
            $('#addToCartForm').on('submit', function(e) {
                e.preventDefault();

                let id = e.target.name;
                let qty = $('#quantity_field').val();
                $.ajax({
                    url: "{{ route('addToCart') }}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id: id,
                        qty: qty,
                    },
                    success: function(response) {
                        $('#successMsg').show();
                        if ($("#cart_title").text() == "Il tuo preventivo è vuoto") {
                            $("#cart_title").html(
                                "<h3 id= 'cart_title'>Il tuo preventivo (<span id=\"SidebarCounter\"></span>)</h3>"
                            )
                            $("#sidebar_button_cart").html(
                                "<a href={{ route('cart') }} class=\"btn btn-primary\">Riepilogo</a>"
                            )
                        }
                        if (response.new_item) {
                            $("#SidebarCart").append(response.html);
                        } else {
                            $("#qty_" + response.id).html(response.qty);
                        }
                        $("#SidebarCounter").html(response.cart_count);
                        $("#CartCounter").html(response.cart_count);
                        toastr.success('Aggiunto al preventivo!');
                    },
                    error: function(response) {
                        $('#nameErrorMsg').text(response.responseJSON.errors.id);
                        $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                        toastr.error('Prodotto non aggiunto al preventivo.');
                    },
                });
            });
        </script>
</body>

</html>
