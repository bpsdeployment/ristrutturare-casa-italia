<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header',['NoShow' => true])
                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--back to top end-->
                <!--page header-->
                <div class="margin-b-30">
                    <div class="container text-center">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="space-30"></div>
                                <h4>Crea il tuo account</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end page header-->

                <!--page start-->
                <div class="container">
                    <!--Message-->
                    @if (Session::has('message'))
                        <div class="alert alert-info" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 margin-b-60">
                            <div class="login-register-box">
                                <!--<form class="margin-b-30" method="POST" action="/users">-->
                                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\UserController@store']) !!}

                                <div class="form-group">
                                    <i class="material-icons icon">person</i>
                                    {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'name']) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'true', 'autocomplete' => "on"]) !!}
                                </div>
                                <!--//form-group-->

                                <div class="form-group">
                                    <i class="material-icons icon">person</i>
                                    {!! Form::label('Cognome', null, ['class' => 'sr-only', 'for' => 'surname']) !!}
                                    {!! Form::text('surname', null, ['class' => 'form-control', 'placeholder' => 'Cognome', 'required' => 'true', 'autocomplete' => "on"]) !!}
                                </div>
                                <!--//form-group-->

                                <div class="form-group">
                                    <i class="material-icons icon">email</i>
                                    {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'register-email']) !!}
                                    {!! Form::email('mail', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'true', 'autocomplete' => "on"]) !!}
                                </div>
                                <!--//form-group-->

                                <div class="form-group">
                                    <i class="material-icons icon">lock</i>
                                    {!! Form::label('Password', null, ['class' => 'sr-only', 'for' => 'password']) !!}
                                    {!! Form::password('register-password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => 'true', 'autocomplete' => "on"]) !!}
                                </div>
                                <!--//form-group-->

                                <div class="form-group">
                                    <i class="material-icons icon">lock</i>
                                    {!! Form::label('Conferma Password', null, ['class' => 'sr-only', 'for' => 'confirm-password']) !!}
                                    {!! Form::password('confirm-register-password', ['class' => 'form-control', 'placeholder' => 'Conferma password', 'required' => 'true', 'autocomplete' => "on" ]) !!}
                                </div>
                                <!--/form-group-->

                                <div class="form-group">
                                    <div class="clearfix">
                                        <div class="checkbox remember pull-left">
                                            <label>
                                                {!! Form::checkbox('t&t-consent', null, false, ['class' => 'icheck', 'required' => 'true']) !!}
                                                {!! Form::label('Accetto i <a>Termini e Condizioni</a> del servizio', null, ['class' => 'icheck', 'for' => 't&t-consent'], false) !!} </label>
                                        </div>
                                    </div>
                                    <!--/extra-->
                                </div>
                                <div class="form-group">

                                    <div class="clearfix">
                                        <div class="checkbox remember pull-left">
                                            <label>
                                                {!! Form::checkbox('gdpr_conset', null, false, ['class' => 'icheck', 'required' => 'true']) !!}
                                                {!! Form::label('Acconsento al trattamento dei dati personali', null, ['class' => 'icheck', 'for' => 'gdpr-consent']) !!}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {!! app('captcha')->display() !!}

                                <!--/extra-->
                            </div>
                            <button type="submit" class="btn btn-lg btn-block btn-primary">Registrati</button>
                            {!! Form::close() !!}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <div class="divider">
                            </div>
                            <!--
                            <ul class="social-buttons list-unstyled margin-b-20">
                                <li><a href= class="btn btn-social btn-google btn-block"><i
                                            class="fa fa-google" aria-hidden="true"></i><span
                                            class="btn-text">Registrati
                                            con Google</span></a></li>
                                <li><a href=
                                        class="btn btn-social btn-facebook btn-block"><i class="fa fa-facebook"
                                            aria-hidden="true"></i><span class="btn-text">Registrati
                                            con Facebook</span></a></li>
                            </ul>
                            -->
                            <div>Sei già registrato? <a href={{ route('login') }}>Accedi qui</a></div>

                        </div>
                    </div>
                </div>
                <!--page end-->

            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
