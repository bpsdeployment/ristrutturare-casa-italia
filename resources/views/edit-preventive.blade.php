@php
$rowCounter = 0;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <style>
        .img-wraps {
            position: relative;
            display: inline-block;

            font-size: 0;
        }

        .img-wraps .closes {
            position: absolute;
            top: 5px;
            right: 8px;
            z-index: 100;
            background-color: #FFF;
            padding: 4px 3px;

            color: #000;
            font-weight: bold;
            cursor: pointer;

            text-align: center;
            font-size: 22px;
            line-height: 10px;
            border-radius: 50%;
            border: 1px solid red;
        }

        .img-wraps:hover .closes {
            opacity: 1;
        }        

        .emptyRow {
            display:none;
        }
    </style>

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica preventivo</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open([
                                'method' => 'POST',
                                'route' => ['update-preventive', $id],
                                'onsubmit' => 'return validateForm()',
                            ]) !!}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="row form-group">
                                        <div class="col-lg-4">
                                            Nome cliente
                                            {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'customer_name']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            <select class="livesearch form-control p-3" id="customer_name"
                                                name="customer_id">
                                                <option value={{ $customer_id }}>{{ $customer_name }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-lg-4">
                                            Riferimento
                                            {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'reference_text']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('reference_text', $reference_text, [
                                                'class' => 'form-control',
                                                'required' => 'true',
                                                'placeholder' => 'Inserisci un nome per questo preventivo',
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-lg-4">
                                            Numero
                                            {!! Form::label('Numero', null, ['class' => 'sr-only', 'for' => 'preventive_number']) !!}
                                        </div>
                                        <div class="col-lg-3">
                                            {!! Form::text('preventive_number', $order_id, [
                                                'class' => 'form-control',
                                                'required' => 'true',
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-lg-4">
                                            Data
                                        </div>
                                        <div class="col-md-8">
                                            <input type="date" id="creation_date" name="creation_date" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-lg-4">
                                            Fine validità
                                        </div>
                                        <div class="col-md-8">
                                            <input type="date" id="end_date" name="end_date" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--  Bootstrap table-->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="width: 10%">Nome</th>
                                                    <th scope="col" style="width: 40%">Descrizione</th>
                                                    <th scope="col" style="width: 10%">Quantità</th>
                                                    <th scope="col" style="width: 12%">Prezzo unità</th>
                                                    <th scope="col" style="width: 8%">Sconto(%)</th>
                                                    <th scope="col" style="width: 10%">IVA(%)</th>
                                                    <th scope="col" style="width: 10%">Totale</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($items as $item)
                                                    @include('preventive-row', [
                                                        'item' => $item,
                                                        'counter' => $rowCounter,
                                                    ])
                                                    @php
                                                        $rowCounter += 1;
                                                    @endphp
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Add rows button-->
                                    <div class="row">
                                        <div class="col-lg-8 col">
                                            <a class="btn btn-primary btn-block" id="insertRow" href="#">Nuova riga</a>
                                        </div>
                                        <div class="col">
                                            <a class="btn btn-primary btn-block" id="insertEmptyRow" href="#">Riga vuota</a>
                                        </div>
                                        <div class="col">
                                            <a class="btn btn-primary btn-block" id="insertFreeRow" href="#">Riga libera</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:2%;">
                                <div class="col-lg-9"></div>
                                <div class="col-lg-1">
                                    <div class="row justify-content-end">Imponibile:</div>
                                    <div class="row justify-content-end">Imposta:</div>
                                    <div class="row justify-content-end">Totale:</div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="row justify-content-end" id="taxable_v">{{ $taxable }} €</div>
                                    <div class="row justify-content-end" id="taxes_v">{{ $taxes }} €</div>
                                    <div class="row justify-content-end" id="total_order_v">{{ $total }} €</div>
                                </div>
                            </div>
                            <!--//form-group-->
                            <button style="margin-top: 1%;" type="submit" class="btn btn-lg btn-primary">Salva</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->

    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        function validateForm() {

            var datetimeStart = $('#creation_date').val();
            var datetimeEnd = $('#end_time').val();
            if (Date.parse(datetimeStart) < Date.parse(datetimeEnd)) {
                alert("La data di scadenza del preventivo è antecedente alla data di creazione.");
                return false;
            }
            var numRows = $('.preventiveRow').length;
            if (numRows == 0) {
                alert("Questo preventivo non ha righe.");
                return false;
            }

        }

        function UpdateTotalPreventive() {
            var $rowTotals = $(".total_row");
            var $rowTaxes = $(".taxes_row");
            var totalTaxable = 0;
            var totalTaxes = 0;
            for (var i = 0; i < $rowTotals.length; i++) {
                var currentTaxable = Number($rowTotals[i].value);
                var currentTax = Number(($rowTaxes[i].value.length == 0) ? 0 : $rowTaxes[i].value);
                totalTaxable += currentTaxable;
                totalTaxes += Number((currentTaxable / 100 * currentTax));
            }
            $('#taxable_v').html(totalTaxable.toFixed(2) + ' €');
            $('#taxes_v').html(totalTaxes.toFixed(2) + ' €');
            $('#total_order_v').html((Number(totalTaxable) + Number(totalTaxes)).toFixed(2) + ' €');
        }

        function UpdateTotalRow(rowIndex) {
            if ($('#unitprice_' + rowIndex).val() != undefined && $('#qty_' + rowIndex).val() != undefined) {
                var price = $('#unitprice_' + rowIndex).val();
                var qty = $('#qty_' + rowIndex).val();
                var discount = ($('#discount_' + rowIndex).val() == undefined) ? 0 : $('#discount_' + rowIndex).val();
                $("#total_" + rowIndex).val((qty * price - qty * price / 100 * discount).toFixed(2));
                UpdateTotalPreventive();
            }
        }
        $(function() {

            var counter = {{ $rowCounter }};

                        
            $("#insertEmptyRow").on("click", function(event) {
                event.preventDefault();

                var newRow = $("<tr class=\"preventiveRow\" >");
                var cols = '';

                // Table columns

                cols += '<td><input type="text" class="form-control rounded-0 emptyRow" id="name_' + counter +
                    '" name="product[' + counter +'][id]" value="-2" required>';

                cols += '<textarea class="form-control rounded-0  emptyRow" id="descr_' + counter + '" name="product[' + counter +
                    '][description] required"></textarea>';

                cols += '<input class="form-control rounded-0 editable_values  emptyRow" id="qty_' + counter +
                    '" type="text" min="1" value="0" name="product[' + counter + '][qty]" required>';

                cols += '<input class="form-control rounded-0 editable_values  emptyRow" id="unitprice_' + counter +
                    '" type="text" value="0" name="product[' + counter + '][UM]" required>';

                cols += '<input class="form-control rounded-0 editable_values  emptyRow" id="discount_' + counter +
                    '" type="text" min="0" max="100" value="0" name="product[' + counter + '][discount]">';


                cols += '<select name="product[' + counter + '][IVA]" class="rounded-0 taxes_row editable_values emptyRow " id="IVA_' + counter +
                    '"<option value="0">0</option> <option value="4">4</option> <option value="5">5</option> <option value="10">10</option> <option value="20">20</option> <option value="21">21</option>  <option value="22">22</option></select>'
                /*
                cols +=
                    '<td><input class="form-control rounded-0" id="type_' + counter +
                    '" type="text" name="product[' + counter +
                    '][prod_type]"></td>';
                */
                cols += '<input class="form-control rounded-0 total_row  emptyRow" id="total_' + counter +
                    '" type="text" name="product[' + counter + '][total]" value="0">';

                cols += '<td colspan="6" style="text-align:right;font-style:italic;">Riga Vuota</td>';
                cols += '<td><button class="btn btn-danger rounded-0" type="button" id ="deleteRow">X</button></td>';

                // Insert the columns inside a row
                newRow.append(cols);

                // Insert the row inside a table
                $("table").append(newRow);

                // Increase counter after each row insertion
                counter++;
            });

            $("#insertFreeRow").on("click", function(event) {
                event.preventDefault();

                var newRow = $("<tr class=\"preventiveRow\" >");
                var cols = '';

                // Table columns
                cols += '<td><input type="hidden" id="name_' + counter + '" class="form-control rounded-0" name="product[' + counter +'][id]" value="-1" required>';                
                cols += '<input type="text" id="name_text' + counter + '" class="form-control rounded-0"  name="product[' + counter +'][name_text]"></td>';

                cols += '<td><textarea id="descr_' + counter + '" class="form-control rounded-0" name="product[' + counter + '][description] spellcheck="false""></textarea></td>';

                cols += '<td><input type="text" id="qty_' + counter + '" class="form-control rounded-0 editable_values" name="product[' + counter + '][qty]" ></td>';

                cols += '<td><input type="text" id="unitprice_' + counter + '" class="form-control rounded-0 editable_values" name="product[' + counter + '][UM]" ></td>';

                cols += '<td><input type="text" id="discount_' + counter + '" class="form-control rounded-0 editable_values" name="product[' + counter + '][discount]" ></td>';

                cols += '<td><input type="text" id="IVA_' + counter + '" class="form-control rounded-0 editable_values taxes_row" name="product[' + counter + '][IVA]" ></td>'

                cols += '<td><input type="text" id="total_' + counter + '" class="form-control rounded-0 total_row" name="product[' + counter + '][total]" readonly></td>';

                cols += '<td><button class="btn btn-danger rounded-0" type="button" id ="deleteRow">X</button</td>';

                // Insert the columns inside a row
                newRow.append(cols);

                // Insert the row inside a table
                $("table").append(newRow);

                // Increase counter after each row insertion
                counter++;
            });

            $("#insertRow").on("click", function(event) {
                event.preventDefault();

                var newRow = $("<tr class=\"preventiveRow\" >");
                var cols = '';

                // Table columns
                cols +=
                    '<td><select class="livesearch_products form-control rounded-0" id="name_' +
                    counter +
                    '" name="product[' + counter +
                    '][id]" required></td>';
                cols += '<td><textarea class="form-control rounded-0" id="descr_' + counter +
                    '" spellcheck="false" name="product[' + counter +
                    '][description] required"></textarea></td>';

                cols +=
                    '<td><input class="form-control rounded-0 editable_values" id="qty_' + counter +
                    '" type="number" min="0" name="product[' + counter +
                    '][qty]" step="any" required></td>';
                cols +=
                    '<td><input class="form-control rounded-0 editable_values" id="unitprice_' + counter +
                    '" type="text" name="product[' + counter +
                    '][UM]" required></td>';
                cols +=
                    '<td><input class="form-control rounded-0 editable_values" id="discount_' + counter +
                    '" type="number" min="0" max="100" value="0" name="product[' + counter +
                    '][discount]"></td>';


                cols += '<td><select name="product[' + counter +
                    '][IVA]" class="rounded-0 taxes_row editable_values" id="IVA_' + counter +
                    '"> <option value="0">0</option>  <option value="4">4</option> <option value="5">5</option> <option value="10">10</option> <option value="20">20</option> <option value="21">21</option>  <option value="22">22</option></select> </td>'
                /*
                cols +=
                    '<td><input class="form-control rounded-0" id="type_' + counter +
                    '" type="text" name="product[' + counter +
                    '][prod_type]"></td>';
                */
                cols +=
                    '<td><input class="form-control rounded-0 total_row" id="total_' + counter +
                    '" type="text" name="product[' + counter +
                    '][total]" disabled></td>';
                cols +=
                    '<td><button class="btn btn-danger rounded-0" type="button" id ="deleteRow">X</button</td>';

                // Insert the columns inside a row
                newRow.append(cols);

                // Insert the row inside a table
                $("table").append(newRow);
                $('.livesearch_products').select2({
                    ajax: {
                        url: '/admin-page/insert-order/productname-search',
                        dataType: 'json',
                        delay: 250,
                        processResults: function(data) {
                            var _rowIndex = jQuery(this)[0].container.id.split("_")[1];
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        text: item.name,
                                        description: item.description,
                                        id: item.id,
                                        rowIndex: _rowIndex,
                                        listing_price: item.listing_price,
                                        max_discount: 100//item.max_discount
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });
                $('.livesearch_products').on("select2:selecting", function(e) {
                    $('#descr_' + e.params.args.data.rowIndex).val(e.params.args.data
                        .description);
                    $('#unitprice_' + e.params.args.data.rowIndex).val(e.params.args.data
                        .listing_price);
                    $('#discount_' + e.params.args.data.rowIndex).attr('max', 100/*e.params.args.data
                        .max_discount*/);
                });

                $('.editable_values').change(function(event) {
                    var rowIndex = event.target.id.split("_")[1];
                    console.log(event.target.id);
                    UpdateTotalRow(rowIndex);
                });
                // Increase counter after each row insertion
                counter++;
            });

            // Remove row when delete btn is clicked
            $("table").on("click", "#deleteRow", function(event) {
                $(this).closest("tr").remove();
                UpdateTotalPreventive();
            });
        });

        $('.livesearch').select2({
            ajax: {
                url: '/admin-page/insert-order/autocomplete-search',
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name + " " + item.surname,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#creation_date').val("{{ $creation_date }}");
            $('#end_date').val("{{ $validity_end_date }}");
            $("#insertRow").click();
            $('table tr:last').remove();
        });
    </script>
</body>

</html>
