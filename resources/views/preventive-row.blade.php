<tr class="preventiveRow">
    @if ($item['id'] == -2)
    <td>
        <input type="text" class="form-control rounded-0 emptyRow" id="name_{{ $counter }}" name="product[{{ $counter }}][id]" value="-2" required>
        <textarea class="form-control rounded-0  emptyRow" id="descr_{{ $counter }}" name="product[{{ $counter }}][description]"></textarea>
        <input class="form-control rounded-0 editable_values  emptyRow" id="qty_{{ $counter }}" type="text" min="1" value="0" name="product[{{ $counter }}][qty]">
        <input class="form-control rounded-0 editable_values  emptyRow" id="unitprice_{{ $counter }}" type="text" value="0" name="product[{{ $counter }}][UM]">
        <input class="form-control rounded-0 editable_values  emptyRow" id="discount_{{ $counter }}" type="text" min="0" max="100" value="0" name="product[{{ $counter }}][discount]">
        <select name="product[{{ $counter }}][IVA]" class="rounded-0 taxes_row editable_values emptyRow " id="IVA_{{ $counter }}">
            <option value="0">0</option>
        </select>
        <input class="form-control rounded-0 total_row  emptyRow" id="total_{{ $counter }}" type="text" name="product[{{ $counter }}][total]" value="0">
    </td>
    <td colspan="6" style="text-align:right;font-style:italic;">Riga Vuota</td>
    <td><button class="btn btn-danger rounded-0" type="button" id="deleteRow">X</button></td>

    @else
    @if($item['id'] == -1)
    <!-- Free row -->
    <td>
        <input type="hidden" class="form-control rounded-0" id="name_{{ $counter }}" name="product[{{ $counter }}][id]" value="-1" required>
        <input type="text" class="form-control rounded-0" id="name_text{{ $counter }}" name="product[{{ $counter }}][name_text]" value="{{$item['name_text']}}">
    </td>
    @else
    <td>
        <select class="livesearch_products form-control rounded-0" id="name_{{ $counter }}" name="product[{{ $counter }}][id]">
            <option value={{ $item['id'] }}>{{ ('App\Models\Product')::find($item['id'])->name }}</option>
        </select>
    </td>
    @endif
    <td>

        <textarea class="form-control rounded-0" id="descr_{{ $counter }}" spellcheck="false" name="product[{{ $counter }}][description]">{{ $item['description'] }}</textarea>
    </td>
    <td><input class="form-control rounded-0 editable_values" id="qty_{{ $counter }}" type="text" min="1" name="product[{{ $counter }}][qty]" value={{ $item['qty'] }}></td>
    <td><input class="form-control rounded-0 editable_values" id="unitprice_{{ $counter }}" type="text" name="product[{{ $counter }}][UM]" value="{{ $item['UM'] }}"></td>
    <td><input class="form-control rounded-0 editable_values" id="discount_{{ $counter }}" type="text" min="0" max="100" name="product[{{ $counter }}][discount]" value={{ $item['discount'] }}>
    </td>
    @if ($item['id'] == -1)

    <td><input type="text" id="IVA_' + counter + '" class="form-control rounded-0 editable_values taxes_row" name="product[{{ $counter }}][IVA]" value="{{$item['IVA']}}"></td>

    @else

    <td><select name="product[{{ $counter }}][IVA]" class="rounded-0 taxes_row editable_values" id="IVA_{{ $counter }}">
            @if ($item['IVA'] == 0)
            <option value="0" selected>0</option>
            @else
            <option value="0">0</option>
            @endif
            @if ($item['IVA'] == 4)
            <option value="4" selected>4</option>
            @else
            <option value="4">4</option>
            @endif
            @if ($item['IVA'] == 5)
            <option value="5" selected>5</option>
            @else
            <option value="5">5</option>
            @endif

            @if ($item['IVA'] == 10)
            <option value="10" selected>10</option>
            @else
            <option value="10">10</option>
            @endif

            @if ($item['IVA'] == 20)
            <option value="20" selected>20</option>
            @else
            <option value="20">20</option>
            @endif

            @if ($item['IVA'] == 21)
            <option value="21" selected>21</option>
            @else
            <option value="21">21</option>
            @endif

            @if ($item['IVA'] == 22)
            <option value="22" selected>22</option>
            @else
            <option value="22">22</option>
            @endif

        </select> </td>
    @endif

    @if (is_numeric($item['UM']) && is_numeric($item['qty']) && is_numeric($item['discount']))
    <td><input class="form-control rounded-0 total_row" id="total_{{ $counter }}" type="text" name="product[{{ $counter }}][total]" disabled="" value="{{ number_format($item['UM'] * $item['qty'] - (($item['UM'] * $item['qty']) / 100) * $item['discount'], 2, '.', '') }}">
        @else
    <td><input class="form-control rounded-0 total_row" id="total_{{ $counter }}" type="text" name="product[{{ $counter }}][total]" disabled="" value="N/D">
        @endif
    </td>
    <td><button class="btn btn-danger rounded-0" type="button" id="deleteRow">X</button></td>
    @endif

</tr>