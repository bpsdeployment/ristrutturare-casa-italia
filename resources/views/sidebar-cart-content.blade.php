<li id="sidebar_item_{{$id}}" class="clearfix">
    <div class="cart-thumb">
        @php
            $product= ('App\Models\Product')::find($id);
            $thumbnail = 'App\Http\Controllers\ProductController'::GetThumbnailImage($product->photos_folder);
        @endphp
        <img src={{ asset('images/products_images/' . $product->photos_folder . '/' . $thumbnail) }} alt="" class="text-center" width="120">
    </div>
    <div class="cart-content">
        <span class="close"><i id="{{$id}}" class="fa fa-times"></i></span>
        <h5><a href={{ route('showProduct', ['product_id' => $id]) }}>{{$name}}</a></h5>
        <p><span class="price">{{$price}} €</span>x<span id="qty_{{$id}}">{{$qty}}</span></p>
    </div>
</li>
