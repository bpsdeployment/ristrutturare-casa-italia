<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <div class="container-scroller">
        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">{{ $admin->name . ' ' . $admin->surname }}</h3>
                </div>

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                            <div class="row">
                                <div class="alert alert-info" role="alert">
                                    <div class="col"> {{ Session::get('message') }} </div>

                                </div>
                            </div>
                            @endif

                            <div class="row mb-2">
                                <div class="col">
                                    <a class="btn btn-primary" href={{route('edit-admin-data', ['id' => $admin->id])}}>Modifica dettagli</a>
                                    <a class="btn btn-danger float-end" id="deleteAdminBtn" href={{route('delete-admin', ['id' => $admin->id])}}>Elimina admin</a>
                                </div>
                            </div>

                            {!! Form::open(['method' => 'POST', 'route' => ['update-sell-commission-perc', $admin->id], 'class' => 'd-inline']) !!}
                            <div class="row py-3">
                                <input type="hidden" value="{{ $admin->id }}" name="admin_id">
                                <div class="col-lg-3">
                                    Commissione sulle vendite
                                    {!! Form::label('selling_commission', null, ['class' => 'sr-only', 'for' => 'selling_commission']) !!}
                                </div>
                                <div class="col-lg-2 mb-1">
                                    <input type="number" min="0" max="100" name="selling_commission" class="form-control mb-1" value={{ $admin->selling_commission_perc }} required>
                                </div>
                                <div class="col-lg-3">
                                    <button class="btn btn-lg btn-primary  mb-1" type="submit">Salva</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4>Lista clienti</h3>
                                        <div class="table-responsive">
                                            <table id="datatable" class="table table-bordered yajra-datatable_customers" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Nome</th>
                                                        <th>Mail</th>
                                                        <th>Spesa totale</th>
                                                        <th>Azioni</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($customers as $customer)
                                                    <tr>
                                                        <td>{{ $customer->id }}</td>
                                                        <td>{{ $customer->name . ' ' . $customer->surname }}</td>
                                                        <td>{{ $customer->mail }}</td>
                                                        <td>{{ number_format($customer->total_spent,2,',','.').' €' }}</td>
                                                        <td><a href={{ route('show-user-details', $customer->id) }} class="edit btn btn-info btn-sm">Dettagli</a></td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                                <div class="col-lg-6">
                                    <h4>Lista preventivi</h4>

                                    <div class="table-responsive">
                                        <table id="datatable" class="table table-bordered yajra-datatable_preventives" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Cliente</th>
                                                    <th>Data</th>
                                                    <th>Totale prev. (€)</th>
                                                    <th>Stato prev.</th>
                                                    <th>Azioni</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($orders as $order)
                                                <tr>
                                                    <td>{{ $order->id }}</td>
                                                    <td>{{ ('App\Models\User')::find($order->customer_id)->name . ' ' . ('App\Models\User')::find($order->customer_id)->surname }}
                                                    </td>
                                                    <td>{{ $order->creation_date }}</td>
                                                    <td>{{ number_format($order->total,2,',','.').' €' }}</td>
                                                    <td>{{ $order->accepted_by_customer == 1 ? 'Accettato' : 'In attesa' }}
                                                    </td>
                                                    <td><a href={{ route('manage.order.admin', $order->id) }} class="edit btn btn-info btn-sm">Dettagli</a></td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <h4>Margini e sconti {{ $targetYear }} </h3>
                                            <div class="row justify-content-end" id="taxable_v">
                                                <form id="change-year" action={{ route('show-admin-details', $admin_id) }}>
                                                    <select id="select-year" name="targetYear" class="btn btn-lg btn-primary">
                                                        @foreach ($year_list as $year)
                                                        @if ($year == $targetYear)
                                                        <option value="{{ $year }}" selected>
                                                            {{ $year }}
                                                        </option>
                                                        @else
                                                        <option value="{{ $year }}">
                                                            {{ $year }}
                                                        </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </form>
                                            </div>


                                    </div>
                                    <div style="margin-top: 2%;" id="chart-preventive-count"></div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
    $(function() {
        var table = $('.yajra-datatable_customers').DataTable({
            processing: true,
            "pageLength": 10,
            order: [],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'complete_name',
                    name: 'complete_name'
                },
                {
                    data: 'mail',
                    name: 'mail'
                },
                {
                    data: 'total_spent',
                    name: 'total_spent'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Italian.json'
            }
        });

        var preventive_table = $('.yajra-datatable_preventives').DataTable({
            processing: true,
            "pageLength": 10,
            order: [],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'creation_date',
                    name: 'creation_date'
                },
                {
                    data: 'total',
                    name: 'total'
                },
                {
                    data: 'accepted_by_customer',
                    name: 'accepted_by_customer'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Italian.json'
            }
        });

    });

    const chart = Highcharts.chart('chart-preventive-count', {
        chart: {
            type: 'line',
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            title: {
                text: "Id preventivi"
            },

            categories: <?= $data_point_labels ?>,
        },
        yAxis: {
            title: {
                text: '%'
            },
            max: 100,
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= $margin_discount_data ?>
    });

    $("#select-year").change(function() {
        $("#change-year").submit();
    });

    $("#deleteAdminBtn").click(function() {
        alert('Sei sicuro?');
    });
</script>

</html>