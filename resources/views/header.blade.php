@php
    $rows = App\Http\Controllers\CategoryController::RetrieveCategoryMatrix();
@endphp
<!--===========================start Header===========================-->
<!--top bar-->
<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <!--
                    MS-BPS-07062022
                    <span>Placeholder text</span>
                -->
            </div>
            <div class="col-sm-6">
                <ul class="list-inline pull-right">
                    @if (Session::get('is_admin'))
                        <li><a href={{ route('admin.page') }}><i class="material-icons">account_box</i>
                                {{ Session::get('admin_email') }}</a></li>
                        <li><a href={{ route('logout') }}><i class="material-icons">logout</i>Logout</a></li>
                    @else
                        <li><a href={{ route('login') }}><i class="material-icons">perm_identity</i>Login</a></li>
                    @endif
                    <!--
                    MS-BPS-07062022
                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><img
                                src={{ asset('images/flags/italy.png') }} alt="">Italiano</a>
                    -->
                        <!--
                    <ul class="lang-dropdown dropdown-menu">
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/spanish.png" alt="Spanish">Spanish</a></li>
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/italy.png" alt="Italian">Italian</a></li>
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/german.png" alt="German">German</a></li>
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/fr.png" alt="French">French</a></li>
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/jp.png" alt="Japanise">Japanese</a></li>
                        <li><a href="javascript:void(0)"><img class="flag" src="images/flags/in.png" alt="Hindi">Hindi</a></li>
                    </ul>
                    -->
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--end top bar-->
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top yamm sticky-header">
    <div class="container">
        <div class="pull-right">
            <ul class="right-icon-nav nav navbar-nav list-inline">
                @unless(isset($NoShow))
                <li class="cart-nav"><a href="javascript:void(0)" data-toggle="offcanvas"
                        data-target="#cartNavmenu" data-canvas="body"><i class="material-icons">shopping_cart</i> <span
                            class="label label-primary" id="CartCounter">{{ Cart::count() }}</span></a></li>
                @endunless
            <!--
                <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle"
                        data-toggle="dropdown"><i class="material-icons">search</i></a>
                    <ul class="dropdown-menu search-dropdown">
                        <li>
                            <div class="search-form">
                                <form role="form">
                                    <input type="text" class="form-control" placeholder="Cerca un prodotto...">
                                    <button type="submit"><i class="material-icons">search</i></button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            -->
            </ul>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="" style="float: left;" href={{ route('index') }}><img src={{ asset('images/logo_header.png') }}
                    alt=""></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!--mega menu-->
                @unless(isset($NoShow))
                <li class="dropdown yamm-fw">
                    <a href={{route('products')}} class="dropdown-toggle" data-toggle="dropdown">Catologo <i
                            class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu" style="width: auto;">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-unstyled nav">
                                            <li><a href="{{route('products')}}">Vedi il catalogo completo</a></li>
                                        </ul>

                                    </div>
                                </div>
                                @foreach ($rows as $row)
                                    @include('catalog-row',["cat1" => $row[0],"cat2" => $row[1],"cat3" => $row[2],"cat4" => $row[3]])
                                @endforeach
                            </div>
                        </li>
                    </ul>
                </li>
                <!--menu Features li end here-->
                @endunless
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>
<!--cart menu side panel-->
<aside id="cartNavmenu" class="navmenu navmenu-default navmenu-fixed-right offcanvas">
    <div class="cart-inner">
        <ul class="list-unstyled cart-list margin-b-30" id="SidebarCart">
            @if (Cart::count() == 0)
                <div class="text-center">
                    <h3 id='cart_title'>Il tuo preventivo è vuoto</h3>
                    <hr>
                </div>
            @else
                <h3 id='cart_title'>Il tuo preventivo (<span id="SidebarCounter">{{ count(Cart::content()) }}</span>)
                </h3>
                <hr>
                @foreach (Cart::content() as $item)
                    @include('sidebar-cart-content', ['name'=>$item->name, 'price'=>$item->price,
                    'qty'=>$item->qty,
                    'rowId'=>$item->rowId, 'id'=>$item->id])
                @endforeach
            @endif
        </ul>
        <div class="text-center" id='sidebar_button_cart'>
            @if (Cart::count() > 0)
                <a href={{ route('cart') }} class="btn btn-primary">Riepilogo</a>
            @endif
        </div>
    </div>
</aside>
<link href={{asset('css/toastr.css')}} rel="stylesheet">
<script src={{asset('js/toastr.js')}}></script>
<script type="text/javascript">
    $(document).on('click', '.fa-times', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('deleteItem') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                id: e.target.id,
            },
            success: function(data) {
                if (data.cart_count == 0) {
                    $("#cart_title").html("<h3 id='cart_title'>Il tuo preventivo è vuoto</h3>");
                    $("#sidebar_button_cart").html("")
                }
                $('#sidebar_item_' + e.target.id).remove();
                $("#SidebarCounter").html(data.cart_count);
                $("#CartCounter").html(data.cart_count);
                console.log(data);
                toastr.success('Rimosso dal preventivo!');
            },
            error: function(response) {
                $('#nameErrorMsg').text(response.responseJSON.errors.id);
                $('#emailErrorMsg').text(response.responseJSON.errors.qty);
            },
        });
    });
</script>

<!--===========================End Header===========================-->
