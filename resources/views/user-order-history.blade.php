<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="container">
                    <div class="space-20"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="cart-totals">
                                <div class="cart-totals-title">
                                    <h3>Storico ordini</h3>
                                </div>
                                <div class="cart-totals-fields">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><strong>ID</strong></td>
                                                <!--
                                                MS-BPS-09062022
                                                <td><strong>Indirizzo spedizione</strong></td>
                                                <td><strong>Stato ordine</strong></td>
                                                -->
                                                <td><strong>Nome ordine</strong></td>
                                                <td><strong>Totale</strong></td>
                                                <td><strong>Data ordine</strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                            @foreach ($orderHistory as $order)
                                                <tr>
                                                    <td><span id="OrderId">{{ $order->id }}</span></td>
                                                    <td><span id="OrderMemo">{{ $order->order_memo }}</span></td>
                                                    <td><span id="OrderPriceTotal">{{ $order->total_price }} €</span></td>
                                                    <td><span id="OrderDate">{{ date('d-m-Y H:i', strtotime($order->created_at))}}</span></td>
                                                    <td><span id="OrderDetails"><button type="button"><a href={{route('checkOrderDetailUser', ['order_id' => $order->id])}}>Dettagli</a></button></span></td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{ $orderHistory->links() }}
                            <div class="space-30"></div>
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->

    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
