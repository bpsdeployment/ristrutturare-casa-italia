<div>
    {!! Form::open(['method' => 'POST', 'route' => 'addReview']) !!}
    <div class="row">
        <div class="col-sm-12">
            <h4 class="margin-b-30">Aggiungi una recensione</h4>
            {!! Form::hidden('product_id', $product_id) !!}
            <div class="form-group">
                <textarea name="review_text" class="form-control" rows="5" placeholder="Scrivi la tua recensione"
                    required></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-6 text-left">
            <div class="rating_field">
            </div>
            <script>
                $('.rating_field').addRating({
                    half: true,
                    max: 5
                });
            </script>
        </div>
        <div class="col-sm-12 col-lg-6 text-right">
            <input type="submit" class="btn btn-xl btn-dark" value="Invia">
        </div>
    </div>
    <div class="row">

    </div>

    {!! Form::close() !!}
    @if (count($errors) > 0)
        <div class="row">
            <br>
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>

    @endif
</div>
