<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')

                <!--page start-->
                <div class="container margin-b-10">
                    <div class="space-20"></div>
                    <div class="row margin-b-30">
                        <div class="row">
                            @if (Session::has('message'))
                                <br>
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            <div class="col-sm-5">
                                <h3>Informazioni su Ristrutturare Casa Italia</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod
                                    tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                </p>
                                <p>124, Munna Wali Dhani, Lalchandpura, Niwaru Road, Jhotwara, Jaipur, Rajsthan, 302012
                                </p>
                                <p>Telephone: 1800.123.4534<br>Fax: 1800.123.4534</p>
                            </div>
                        </div>

                    </div>
                    <div class="row margin-b-30">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Hai una domanda? Compila i campi seguenti per essere ricontattato al più presto</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                {!! Form::open(['method' => 'POST', 'route' => 'storeMessage', 'class' => 'boland-contact']) !!}
                                <div class="row">
                                    <div class="col-sm-12 col-lg-4 margin-b-20">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Il tuo nome', 'required' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-4 margin-b-20">
                                        {!! Form::email('mail', null, ['class' => 'form-control', 'placeholder' => 'La tua e-mail', 'required' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-4 margin-b-20">
                                        {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'L\'oggetto del messaggio', 'required' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 margin-b-20">
                                        {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 5, 'required' => 'true', 'placeholder' => 'Il tuo messaggio']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div class="data-status"></div> <!-- data submit status -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        {!! app('captcha')->display() !!}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12 text-left">
                                        <button type="submit" name="submit" class="btn btn-lg btn-dark">Invia
                                            messaggio</button>
                                    </div>
                                </div>
                                <div class="space-20"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger" role="alert">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>
</html>
