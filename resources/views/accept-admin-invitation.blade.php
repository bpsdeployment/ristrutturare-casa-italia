<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 hidden-xs">
                                <!--
                                MS-BPS-07062022
                                <span>Placeholder text</span>
                            -->
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-inline pull-right">
                                    @if (Auth::check())
                                    <li><a href={{ route('admin.page') }}><i class="material-icons">account_box</i>
                                            {{ Auth::user()->mail }}</a></li>
                                    <li><a href={{ route('logout') }}><i class="material-icons">logout</i>Logout</a></li>
                                    @else
                                    <li><a href={{ route('login') }}><i class="material-icons">perm_identity</i>Login</a></li>
                                    @endif
                                    <!--
                                MS-BPS-07062022
                                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><img
                                            src={{ asset('images/flags/italy.png') }} alt="">Italiano</a>
                                -->
                                    <!--
                                <ul class="lang-dropdown dropdown-menu">
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/spanish.png" alt="Spanish">Spanish</a></li>
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/italy.png" alt="Italian">Italian</a></li>
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/german.png" alt="German">German</a></li>
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/fr.png" alt="French">French</a></li>
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/jp.png" alt="Japanise">Japanese</a></li>
                                    <li><a href="javascript:void(0)"><img class="flag" src="images/flags/in.png" alt="Hindi">Hindi</a></li>
                                </ul>
                                -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end top bar-->
                <!-- Static navbar -->
                <nav class="navbar navbar-default navbar-static-top yamm sticky-header">
                    <div class="container">
                        <div class="pull-right">
                            <ul class="right-icon-nav nav navbar-nav list-inline">
                                <!--
                                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle"
                                            data-toggle="dropdown"><i class="material-icons">search</i></a>
                                        <ul class="dropdown-menu search-dropdown">
                                            <li>
                                                <div class="search-form">
                                                    <form role="form">
                                                        <input type="text" class="form-control" placeholder="Cerca un prodotto...">
                                                        <button type="submit"><i class="material-icons">search</i></button>
                                                    </form>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                -->
                            </ul>
                        </div>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="" style="float: left;" href={{ route('index') }}><img src={{ asset('images/logo_header.png') }} alt=""></a>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container-fluid -->
                </nav>
                <!--page start-->
                <div class="space-20"></div>
                <div class="container">
                    <div class="row">
                        <div class="login-register-box">
                            <form action={{route('complete-admin-registration')}} method="POST">
                                @csrf
                                <input type="hidden" name="token" value={{$admin->invitation_token }}>

                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="name">Nome</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('name', $admin->name, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Nome',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="surname">Cognome</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('surname', $admin->surname, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Cognome',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="mail">Email</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::email('mail', $admin->mail, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Email',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="tel">Telefono</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('tel', $admin->tel, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Telefono',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="codice_fiscale">Codice fiscale</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('codice_fiscale', $admin->codice_fiscale, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Codice fiscale',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="p_iva">Partita IVA</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('p_iva', $admin->p_iva, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Partita IVA',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="n_iscr_albo">N° iscrizione albo</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('n_iscr_albo', $admin->n_iscr_albo, [
                                        'class' => 'form-control',
                                        'placeholder' => 'N° iscrizione albo',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="selling_commission_perc">Commissione in %</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::text('selling_commission_perc', $admin->selling_commission_perc, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Commissione in %',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <!----------------------------------------------------------------------------------------------------------------------------------------------------------------------->
                                <div class="row mb-3 form-group">
                                    <div class="col-lg-2">
                                        <label for="passwordF">Password</label>
                                    </div>
                                    <div class="col-lg-9">
                                        {!! Form::password('password', [
                                        'class' => 'form-control',
                                        'placeholder' => 'Password',
                                        'required' => 'true',
                                        'autocomplete' => 'on',
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="form-group mb-3">
                                        <div class="col-lg-2">
                                            <label for="confirm_password">Conferma password</label>
                                        </div>
                                        <div class="col-lg-9">
                                            {!! Form::password('confirm_password', [
                                            'class' => 'form-control',
                                            'placeholder' => 'Conferma password',
                                            'required' => 'true',
                                            'autocomplete' => 'on',
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-5">
                                        <div class="clearfix">
                                            <div class="checkbox remember" style="float:right;">
                                                <label>
                                                    {!! Form::checkbox('gdpr_conset', null, false, ['class' => 'icheck', 'required' => 'true']) !!}
                                                    {!! Form::label('Acconsento al trattamento dei dati personali', null, [
                                                    'class' => 'icheck',
                                                    'for' => 'gdpr-consent',
                                                    ]) !!}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div id="captcha" style="float:right;">
                                            {!! app('captcha')->display() !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col" style="margin-top:20px;text-align: center">

                                        <button type="submit" class="btn btn-lg btn-block btn-primary ">Completa registrazione</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    @if (count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif

                </div>
            </div>
            <!--page end-->
        </div>
    </div>
    <!--footer-->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 margin-b-30">
                    <h4>Chi siamo</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                        tincidunt ut
                        laoreet dolore magna aliquam erat volutpat.
                    </p>
                    <!--
                <ul class="list-inline socials">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
            -->
                </div>
                <!--/col-->
                <div class="col-md-3 margin-b-30"></div>
                <!--
            <div class="col-md-3 margin-b-30">
                <h4>Ultime novità</h4>
                <ul class="list-unstyled latest-news">
                    <li class="clearfix">
                        <div class="pull-left">
                            <a href="#">
                                <img src="images/img-1.jpg" alt="" width="50" class="img-responsive">
                            </a>
                        </div>
                        <div class="content">
                            <h5><a href="#">Post Blog 1</a></h5>
                            <span><a href="#">5 Comments</a></span>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="pull-left">
                            <a href="#">
                                <img src="images/img-2.jpg" alt="" width="50" class="img-responsive">
                            </a>
                        </div>
                        <div class="content">
                            <h5><a href="#">this is a post title</a></h5>
                            <span><a href="#">5 Comments</a></span>
                        </div>
                    </li>
                    <li class="clearfix">
                        <div class="pull-left">
                            <a href="#">
                                <img src="images/img-3.jpg" alt="" width="50" class="img-responsive">
                            </a>
                        </div>
                        <div class="content">
                            <h5><a href="#">Post title goes here</a></h5>
                            <span><a href="#">5 Comments</a></span>
                        </div>
                    </li>
                </ul>
            </div>-->
                <div class="col-md-3 margin-b-30">
                    <ul class="list-unstyled link-list">
                        <!--
                    MS-BPS-08062022
                    <li><a href="#">Contatti</a></li>
                    -->
                        <!-- <li><a href="#">Spedizioni</a></li> -->
                        <li><a href="#">Termini & Condizioni</a></li>
                        <li><a href="https://www.iubenda.com/privacy-policy/54804280/cookie-policy">Cookie policy</a></li>
                        <li><a href="https://www.iubenda.com/privacy-policy/54804280">Informativa Privacy</a></li>
                    </ul>
                </div>
                <!--/col-->
                <div class="col-md-3 margin-b-30">
                    <!--
                <h4>Newsletter</h4>
                <div class="newsletter-form margin-b-20">
                    <form>
                        <input type="text" class="form-control margin-b-10" placeholder="Email ">
                        <button class="newsletter-button" type="button" class="btn btn-xl btn-dark"></button>
                    </form>
                </div>
                -->
                    <p class="lead"><small>Supporto: </small> <br>1234-567-89-01</p>
                    <p class="lead">mailsupporto@mail.com</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="container text-center">
            <span>&copy; Copyright <?php echo date('Y'); ?>. Tutti i diritti riservati.</span> <br>
            <!-- MS-BPS-07062022 Added following span -->
            <span>Powered by BPS Deployment srl</span>
            <!--
        <ul class="list-inline">
            <li><img src="images/flags/visa.png" alt=""></li>
            <li><img src="images/flags/paypal.png" alt=""></li>
            <li><img src="images/flags/mastercard.png" alt=""></li>
            <li><img src="images/flags/fedex.png" alt=""></li>
        </ul>
        -->
        </div>
    </footer>
    <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>