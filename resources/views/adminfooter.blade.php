        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span style="color: white;">Copyright © <?php echo date("Y"); ?></span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Powered by BPS Deployment srl<i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
