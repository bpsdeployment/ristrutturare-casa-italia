<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica dati cliente</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        @if (Session::has('error_message'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error_message') }}
                            </div>
                        @endif
                        @if (Session::has('message'))
                            <div class="alert alert-info" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <div class="card-body">
                            <form action={{ route('save-edited-customer-data', ['id' => $id]) }} method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Nome
                                            {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'name']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Cognome
                                            {!! Form::label('Cognome', null, ['class' => 'sr-only', 'for' => 'surname']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('surname', $user->surname, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Email
                                            {!! Form::label('Email', null, ['class' => 'sr-only', 'for' => 'email']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::email('email', $user->mail, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Telefono
                                            {!! Form::label('Telefono', null, ['class' => 'sr-only', 'for' => 'phone']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('phone', $user->phone, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Indirizzo riga 1
                                            {!! Form::label('Indirizzo r1', null, ['class' => 'sr-only', 'for' => 'addr_r1']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('addr_r1', $user->address_r1, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Indirizzo riga 2
                                            {!! Form::label('Indirizzo r2', null, ['class' => 'sr-only', 'for' => 'addr_r2']) !!}
                                        </div>
                                        <div class="col-lg-8">
                                            {!! Form::text('addr_r2', $user->address_r2, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Città
                                            {!! Form::label('Città', null, ['class' => 'sr-only', 'for' => 'city']) !!}
                                        </div>
                                        <div class="col-lg-2">
                                            {!! Form::text('city', $user->city, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-lg-1">
                                            Provincia
                                            {!! Form::label('Provincia', null, ['class' => 'sr-only', 'for' => 'prov']) !!}
                                        </div>
                                        <div class="col-lg-2">
                                            {!! Form::text('prov', $user->province, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-lg-1">
                                            CAP
                                            {!! Form::label('CAP', null, ['class' => 'sr-only', 'for' => 'CAP']) !!}
                                        </div>
                                        <div class="col-lg-2">
                                            {!! Form::text('CAP', $user->CAP, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                @if(Session::get('is_site_master', 0))
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            Assegna a
                                            {!! Form::label('Assegna a', null, ['class' => 'sr-only', 'for' => 'reference_admin_id']) !!}
                                        </div>
                                        <div class="col-lg-3">
                                            {!! Form::select('reference_admin_id', $admin_list, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <button type="submit" class="btn btn-lg btn-primary">Salva</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>


    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
