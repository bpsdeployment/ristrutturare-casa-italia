<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href={{asset('css/toastr.css')}} rel="stylesheet">
    <script src={{asset('js/toastr.js')}}></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')

</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')

                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--back to top end-->
                <div class="space-30"></div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 filter-row">
                            <div class="sidebar-widget margin-b-20">
                                <form type="POST" id="resetFilterForm">
                                    <button type="submit" class="btn btn-lg btn-block btn-black">Cancella
                                        filtri</button>
                                </form>
                            </div>
                            <div class="sidebar-widget margin-b-40">
                                <form class="input-group" action = "{{route('products')}}" method="GET">
                                    <input type="text" class="form-control" id="search" name="search"
                                        placeholder="Ricerca per nome" value="{{request()->query('search')}}">
                                </form>
                            </div>
                            <!--/sidebar widget-->
                            <div class="sidebar-widget margin-b-40">
                                <h4>Categorie</h4>
                                <ul class="list-unstyled">
                                    @foreach ($categories as $category)
                                        <li class="clearfix">
                                            <span
                                                class="pull-right">({{ count(('App\Models\Product')::where('type', $category->name)->get()) }})</span>
                                            <a href="{{ request()->fullUrlWithQuery(['category' => $category->name]) }}" class="category"
                                                id="{{ $category->name }}">{{ $category->name }}</a>
                                        </li>
                                    @endforeach
                            </div>
                            <div class="sidebar-widget margin-b-40">
                                <h4>Materiali</h4>
                                <ul class="list-unstyled">
                                    @for($index = 0; $index < count($materials_type); $index++)
                                        <li class="clearfix">
                                            <a href="{{ request()->fullUrlWithQuery(['material' => $materials_type[$index]->id]) }}" class="category"
                                                id="{{ $materials_type[$index]->name }}">{{ $materials_type[$index]->name }}</a>
                                        </li>
                                    @endfor
                            </div>
                            <!--/sidebar widget-->
                            <div class="sidebar-widget margin-b-40">
                                <h4>Ordina per</h4>
                                <ul class="list-unstyled">
                                    <li class="clearfix"><a href="{{ request()->fullUrlWithQuery(['sort' => 'popularity']) }}" class="sort"
                                            id="sort-by-popular">Popolari</a>
                                    </li>
                                    <li class="clearfix"><a class="sort" href="{{ request()->fullUrlWithQuery(['sort' => 'newest']) }}"
                                            id="sort-by-newest">Nuovo</a>
                                    </li>
                                    <li class="clearfix"><a class="sort" href="{{ request()->fullUrlWithQuery(['sort' => 'low_high']) }}"
                                            id="sort-by-priceLTH">Prezzo
                                            crescente</a></li>
                                    <li class="clearfix"><a class="sort" href="{{ request()->fullUrlWithQuery(['sort' => 'high_low']) }}"
                                            id="sort-by-priceHTL">Prezzo
                                            decrescente</a></li>
                                </ul>
                            </div>

                        </div>


                        <div class="col-sm-9" id="product_container">
                            <nav id="product_pagination" aria-label="Page navigation" class="text-right margin-b-30">
                                {!! $products->appends(request()->query())->links() !!}
                            </nav>
                            @php
                                $index = 0;
                            @endphp
                            @if(count($products)==0)
                                <p class="text-center">
                                    Nessun risultato.
                                </p>
                            @else
                                    @for ($row_counter = 0; $row_counter < $rows_number; $row_counter++)
                                        @php
                                            echo '<div class="row">';
                                        @endphp
                                        @for ($i = 0; $i < 3; $i++)
                                            <div class="col-sm-4">
                                                @include('productbox', ['price' => $products[$index]->listing_price,
                                                'name' => $products[$index]->name,
                                                'id' => $products[$index]->id,
                                                'folder_name' => $products[$index]->photos_folder,
                                                'measure_unit_id' => $products[$index]->measure_unit_id])
                                                @php
                                                    $index++;
                                                @endphp
                                            </div>
                                        @endfor
                                        @php
                                            echo '</div>';
                                            echo "<div class='space-50'></div>";
                                        @endphp
                                    @endfor
                                    @php
                                        echo '<div class="row">';
                                    @endphp
                                    @for ($leftover_counter = 0; $leftover_counter < $leftover_products; $leftover_counter++)
                                        <div class="col-sm-4">
                                            @include('productbox', ['price' => $products[$index]->listing_price,
                                            'name' => $products[$index]->name,
                                            'id' => $products[$index]->id,
                                            'folder_name' => $products[$index]->photos_folder,
                                            'measure_unit_id' => $products[$index]->measure_unit_id])
                                            @php
                                                $index++;
                                            @endphp
                                        </div>
                                    @endfor
                                    @php
                                        echo '</div>';
                                        echo "<div class='space-50'></div>";
                                    @endphp
                            @endif
                        </div>

                        <div class="space-30"></div>

                        <nav id="product_pagination" aria-label="Page navigation" class="text-right margin-b-30">
                            {!! $products->appends(request()->query())->links() !!}
                        </nav>

                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $('#addToCartForm').on('submit', function(e) {
                    e.preventDefault();
                    let id = e.target.name;
                    let qty = $('#quantity_field').val();
                    $.ajax({
                        url: "{{ route('addToCart') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            id: id,
                            qty: qty,
                        },
                        success: function(response) {
                            if ($("#cart_title").text() == "Il tuo preventivo è vuoto") {
                                $("#cart_title").html(
                                    "<h3 id= 'cart_title'>Il tuo preventivo (<span id=\"SidebarCounter\"></span>)</h3>"
                                    )
                                $("#sidebar_button_cart").html(
                                    "<a href={{ route('cart') }} class=\"btn btn-primary\">Riepilogo</a>"
                                    )
                            }
                            if (response.new_item) {
                                $("#SidebarCart").append(response.html);
                            } else {
                                $("#qty_" + response.id).html(response.qty);
                            }
                            $("#SidebarCounter").html(response.cart_count);
                            $("#CartCounter").html(response.cart_count);
                            toastr.success('Aggiunto al preventivo!');
                        },
                        error: function(response) {
                            $('#nameErrorMsg').text(response.responseJSON.errors.id);
                            $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                            toastr.error('Prodotto non aggiunto al preventivo.');
                        },
                    });
                });
            </script>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
