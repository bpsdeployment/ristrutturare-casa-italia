<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta name="_token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <script src={{ asset('plugins/jquery/dist/jquery.min.js') }}></script>
    <!-- Common plugins -->
    @include('stylesheet')
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--back to top-->
                <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
                <!--back to top end-->
                <!--start master slider-->
                <div class="clearfix"></div>
                <div class="container">
                    <h3 class="text-uppercase font-400 title-font text-center margin-b-30">Categorie</h3>
                    <div class="owl-theme">
                        <div class="row">
                            @if (count($categories) >= 3)
                                @for ($i = 0; $i < 3; $i++)
                                    @include('categoryshowcase', ['category' => $categories[$i]])
                                @endfor
                            @endif
                            <!--/item-->
                        </div>
                        <div class="space-20"></div>
                        <div class="row">
                            @if (count($categories) >= 6)
                                @for ($i = 3; $i < 6; $i++)
                                    @include('categoryshowcase', ['category' => $categories[$i]])
                                @endfor
                            @endif
                        </div>
                    </div>
                </div>
                <div class="space-40"></div>
                <div class="container">
                    <h3 class="text-uppercase font-400 title-font text-center margin-b-30">Prodotti in primo piano</h3>
                    <div class="row">
                        @if (count($products) >= 4)
                            @for ($i = 0; $i < 4; $i++)
                                @php
                                    echo '<div class="col-sm-6 col-md-3">';
                                @endphp
                                @include('productbox_noadd', [
                                    'id' => $products[$i]->id,
                                    'price' => $products[$i]->listing_price,
                                    'folder_name' => $products[$i]->photos_folder,
                                    'name' => $products[$i]->name,
                                    'measure_unit_id' => $products[$i]->measure_unit_id,
                                ])
                                @php
                                    echo '</div>';
                                @endphp
                            @endfor
                        @endif
                        <!--/col-->

                    </div>
                    <div class="space-20"></div>
                    <!--/row-->
                    <div class="row margin-b-20">
                        @if (count($products) >= 8)
                            @for ($i = 4; $i < 8; $i++)
                                @php
                                    echo '<div class="col-sm-6 col-md-3">';
                                @endphp
                                @include('productbox_noadd', [
                                    'id' => $products[$i]->id,
                                    'price' => $products[$i]->listing_price,
                                    'folder_name' => $products[$i]->photos_folder,
                                    'name' => $products[$i]->name,
                                    'measure_unit_id' => $products[$i]->measure_unit_id,
                                ])
                                @php
                                    echo '</div>';
                                @endphp
                            @endfor
                        @endif
                    </div>
                    <!--/row-->
                    <div class="space-30"></div>
                    <div class="text-center">
                        <a href={{ route('products') }} class="btn btn-link btn-lg">
                            <h3>Vedi il catalogo completo</h3>
                        </a>
                    </div>
                </div>
                <div class="space-30"></div>
            </div>
        </div>
        @include('footer')
        <!--Common plugins-->
        @include('plugins')
    </div>
    <script type="text/javascript">
        $('#addToCartForm').on('submit', function(e) {
            e.preventDefault();
            let id = e.target.name;
            let qty = $('#quantity_field').val();
            $.ajax({
                url: "{{ route('addToCart') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    qty: qty,
                },
                success: function(response) {
                    if ($("#cart_title").text() == "Il tuo preventivo è vuoto") {
                        $("#cart_title").html(
                            "<h3 id= 'cart_title'>Il tuo preventivo (<span id=\"SidebarCounter\"></span>)</h3>"
                        )
                        $("#sidebar_button_cart").html(
                            "<a href={{ route('cart') }} class=\"btn btn-primary\">Riepilogo</a>"
                        )
                    }
                    if (response.new_item) {
                        $("#SidebarCart").append(response.html);
                    } else {
                        $("#qty_" + response.id).html(response.qty);
                    }
                    $("#SidebarCounter").html(response.cart_count);
                    $("#CartCounter").html(response.cart_count);
                    toastr.success('Aggiunto al preventivo!');
                },
                error: function(response) {
                    $('#nameErrorMsg').text(response.responseJSON.errors.id);
                    $('#emailErrorMsg').text(response.responseJSON.errors.qty);
                    toastr.error('Prodotto non aggiunto al preventivo.');
                },
            });
        });
    </script>
    <!--page template scripts-->
    <script src="plugins/masterslider/masterslider.min.js"></script>
    <script>
        (function($) {
            "use strict";
            var slider = new MasterSlider();
            // adds Arrows navigation control to the slider.

            slider.control('timebar', {
                insertTo: '#masterslider'
            });
            slider.control('bullets');

            slider.setup('masterslider', {
                width: 1170, // slider standard width
                height: 510, // slider standard height
                space: 0,
                layout: 'fullwidth',
                loop: true,
                preload: 0,
                instantStartLayers: true,
                autoplay: true
            });
        })(jQuery);
    </script>
</body>

</html>
