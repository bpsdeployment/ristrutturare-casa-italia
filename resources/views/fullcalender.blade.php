<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <script src="https://use.fontawesome.com/fb7480bdbf.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        .fc-title {
            display: block;
            white-space: pre-line;
            /* for make fc-event-title-container expand */
        }
    </style>
</head>

<!-- container-scroller -->
<script src={{ asset('js/vendor.bundle.base.js') }}></script>

<!-- endinject -->
<!-- Plugin js for this page -->
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src={{ asset('js/off-canvas.js') }}></script>
<script src={{ asset('js/hoverable-collapse.js') }}></script>
<script src={{ asset('js/misc.js') }}></script>
<!-- endinject -->
<!-- Custom js for this page -->
<script src={{ asset('js/file-upload.js') }}></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"
    integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>


<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Calendario</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        @if (Session::has('message'))
                            <div class="alert alert-info" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="container">
                                <div class="response"></div>
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>




</body>
<script>
    $(document).ready(function() {

        var SITEURL = "{{ url('/') }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var calendar = $('#calendar').fullCalendar({
            buttonText: {
                today: 'Oggi',
                day: 'Giorno',
                week: 'Settimana',
                month: 'Mese'
            },
            monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio',
                'Agosto',
                'Settembre', 'Ottobre', 'Novembre', 'Dicembre'
            ],
            monthNamesShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott',
                'Nov', 'Dic'
            ],
            dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            firstDay: 1,
            businessHours: {
                daysOfWeek: [1, 2, 3, 4, 5],
                startTime: '8am',
                endTime: '6pm',
            },
            firstHour: '06:00',
            contentHeight: 'auto',
            timeFormat: 'HH:mm',
            displayEventEnd: true,
            defaultView: 'month',
            editable: true,
            events: SITEURL + "/admin-page/calendar",
            displayEventTime: true,
            editable: true,
            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
                let selector = element[0].querySelector('.fc-title');
                if (selector) {
                    selector.innerHTML = selector.innerHTML +
                        '<br><a href="#" style="float: right;"> <i title="' + selector.innerHTML +
                        '" start_time="' + event.start
                        ._i + '" end_time="' + event.end._i + '" class="fa fa-envelope"></i></a>';;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Titolo:');

                if (title) {
                    var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                    var colorR = Math.floor((Math.random() * 165) + 75).toString(16);
                    var colorG = Math.floor((Math.random() * 165) + 75).toString(16);
                    var colorB = Math.floor((Math.random() * 165) + 75).toString(16);
                    $.ajax({
                        url: SITEURL + "/fullcalendareventmaster/create",
                        data: 'title=' + title + '&start=' + start + '&end=' + end +
                            '&color=#' + colorR + colorG + colorB,
                        type: "POST",
                        success: function(data) {
                            calendar.fullCalendar('renderEvent', {
                                    title: title,
                                    start: start,
                                    end: end,
                                    color: "#" + colorR + colorG + colorB,
                                    allDay: allDay,
                                    id: data
                                },
                                true
                            );
                        }
                    });

                }
                calendar.fullCalendar('unselect');
            },

            eventDrop: function(event, delta) {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                $.ajax({
                    url: SITEURL + '/fullcalendareventmaster/update',
                    data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                        '&id=' + event.id,
                    type: "POST",
                    success: function(response) {}
                });
            },
            eventClick: function(event, jsEvent, view) {
                if ($(jsEvent.target).prop("tagName") == "I") {
                    jsEvent.preventDefault();
                    var mail_address = prompt(
                        'Inserisci l\'indirizzo email a cui mandare il promemoria:');

                    if (mail_address == null) {
                        return;
                    }
                    var data = {
                        email: mail_address,
                        message: $(jsEvent.target)[0].title,
                        start_time: $(jsEvent.target).attr('start_time'),
                        end_time: $(jsEvent.target).attr('end_time'),
                        _token: '{{ csrf_token() }}'
                    };
                    $.ajax({
                        type: "POST",
                        url: "{{ route('send-notification-calendar') }}",
                        data: data,
                        success: function() {
                            alert('Mail inviata!');
                        },
                        error: function() {
                            alert(
                                'Errore nell\' invio della mail, controlla l\'indirizzo inserito.'
                            );
                        }
                    });
                    return;
                }
                var deleteMsg = confirm("Vuoi cancellare questo evento?");
                if (deleteMsg) {
                    $.ajax({
                        type: "POST",
                        url: SITEURL + '/fullcalendareventmaster/delete',
                        data: "&id=" + event.id,
                        success: function(response) {
                            if (parseInt(response) > 0) {
                                $('#calendar').fullCalendar('removeEvents', event._id);
                            }
                        }
                    });
                }
            }
        });
    });

    function displayMessage(message) {
        $(".response").html("" + message + "");
        setInterval(function() {
            $(".success").fadeOut();
        }, 1000);
    }

    $(".fc-event-container").click(function() {
        alert("Handler for .click() called.");
    });
</script>

</html>
