<!DOCTYPE html>
<html lang="en" class="footer-sticky">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ristrutturare Casa Italia</title>
    <!-- Common plugins -->
    @include('stylesheet')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!--pre-loader-->
    <div id="preloader"></div>
    <!--pre-loader-->
    <div class="page-footer-sticky">
        <div class="footer-sticky-wrap">
            <div class="footer-sticky-inner">
                @include('header')
                <!--page start-->
                <div class="space-50"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 margin-b-60">

                            {!! Form::open(['method' => 'POST', 'route' => 'password.reset.action']) !!}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required
                                        autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Conferma
                                    password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password-confirm" class="form-control"
                                        name="password_confirmation" required autofocus>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary ">
                                    Reset Password
                                </button>
                            </div>
                            {!! form::close() !!}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!--page end-->
            </div>
        </div>
        <!--footer-->
        @include('footer')
        <!--/footer-->
    </div>
    <!--Common plugins-->
    @include('plugins')
</body>

</html>
