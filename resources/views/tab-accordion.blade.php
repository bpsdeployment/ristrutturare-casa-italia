<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Ristrutturare Casa Italia</title>
        <!-- Common plugins -->
        @include('stylesheet')
    </head>
    <body>
        <!--pre-loader-->
        <div id="preloader"></div>
        <!--pre-loader-->
        <!--back to top-->
        <a href="#" class="scrollToTop"><i class="material-icons 48dp">keyboard_arrow_up</i></a>
        <!--back to top end-->
        @include('header')

        <!--page header-->
        <div class="page-breadcrumb margin-b-60">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="margin-b-20">Tabs & Accordions</h4>
                    </div>
                </div>
            </div>
        </div>
        <!--end page header-->
        {{
            Auth::user()->mail;
        }}
        <div>
            <p>Admin dashboard</p>
            <a href={{route('admin.page')}}>Vai</a>
        </div>
        <!--page start-->
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3 class="text-center margin-b-40">Tabs</h3>
                    <div>

                        <!-- Nav tabs -->
                        <ul class="tabs-nav list-inline" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                            Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                            Dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">
                           Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--page end-->
        <div class="space-60"></div>
        @include('footer')
        <!--Common plugins-->
        @include('plugins')
    </body>
</html>
