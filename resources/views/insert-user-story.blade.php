<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Inserimento nuova nota utente</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'save-user-story']) !!}
                            <input type="hidden" name="id" value="{{ $id }}">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Tipo di contatto
                                        {!! Form::label('contact_category', null, ['class' => 'sr-only', 'for' => 'contact_category']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::select(
                                            'contact_category',
                                            [1 => 'Chiamata', 2 => 'Incontro diretto', 3 => 'Mail', 4 => 'Invio preventivo'],
                                            ['class' => 'form-control', 'required' => 'true'],
                                        ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Nota
                                        {!! Form::label('Nota', null, ['class' => 'sr-only', 'for' => 'contact_note']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('contact_note', null, [
                                            'class' => 'form-control',
                                            'rows' => 4,
                                            'maxlength' => 250,
                                            'placeholder' => 'Informazioni aggiuntive',
                                            'required' => 'true',
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-3">
                                    Data e ora di contatto
                                </div>
                                <div class="col-md-7">
                                    <input type="datetime-local" name="contact_time" required>
                                </div>
                            </div>
                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary">Inserisci</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>


    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
