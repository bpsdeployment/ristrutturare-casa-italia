@php
$is_site_master = Session::get('is_site_master', 0);
$admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
if ($admin) {
$product_auth = $admin->r_products_auth;
$categories_auth = $admin->r_categories_auth;
$contact_reply_auth = $admin->reply_contact_requests;
$firstpage_edit_auth = $admin->u_first_page_auth;
$coupon_auth = $admin->edit_coupons;
$order_auth = $admin->edit_orders;
} else {
$product_auth = false;
$categories_auth = false;
$contact_reply_auth = false;
$firstpage_edit_auth = false;
$coupon_auth = false;
$order_auth = false;
}

if ($admin->mail == 'rigerslamaj83@gmail.com'){
$is_site_master = true;
}

@endphp

<style>
    /* Small devices (tablets, 768px and up) */
    @media (min-width: 989px) {
        #admin-navbar {
            display: none;
        }
    }

    @media (max-width: 989px) {
        #admin-navbar {}

        .menu-item a {
            display: block;
            color: black;
            padding: 10px;
            align-content: center;
            text-align: center;
            font-weight: bold;
            position: relative;
            text-decoration: none;
        }

    }
</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light" id="admin-navbar">
    <a class="navbar-brand" href="#"><img src="{{asset('images/Favicon_RCI.png')}}" width="30" height="30" alt=""> Ristrutturare Casa Italia</a>
    <button class="navbar-toggler" id="btnToggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarContent">
        <div class="navbar-nav">
            <div class="row">
                <div class="col">
                    <a class="nav-item nav-link active" href="{{route('index')}}" style="text-decoration:underline;">Torna al sito</a>
                </div>
                <div class="col font-weight-bold pt-1 pr-1  text-right" id="usernameInfo">
                    {{ $admin->name }} {{ $admin->surname }}
                    <span class="text-secondary font-weight-normal text-small"><br>
                        @if ($admin->mail == 'rigerslamaj83@gmail.com')
                        {{ 'Amministratore' }}
                        @else
                        {{ 'Operatore' }}
                        @endif
                    </span>

                </div>
            </div>
            <a class="nav-item nav-link" href="{{ route('calendar-page') }}">Calendario</a>
            <a class="nav-item nav-link" href="{{ route('user-view') }}">Clienti</a>
            @if ($is_site_master)
            <a class="nav-item nav-link" href="{{ route('admin-view') }}">Admin</a>
            @endif
            @if ($is_site_master or $product_auth)
            <a class="nav-item nav-link" href="{{ route('products-table') }}">Prodotti</a>
            @endif
            @if ($is_site_master or $categories_auth)
            <a class="nav-item nav-link" href="{{ route('categories-table') }}">Categorie</a>
            @endif
            <a class="nav-item nav-link" href="{{ route('orders-table') }}">Preventivi</a>
            @if ($is_site_master or $firstpage_edit_auth)
            <a class="nav-item nav-link" href="{{ route('editFirstPage') }}">Prima pagina</a>
            @endif

        </div>
    </div>
    </div>
</nav>


<script>
    //Getting container element
    var adminNavbar = document.getElementById("admin-navbar");

    //Get all the buttons in the container
    var btns = adminNavbar.getElementsByClassName("nav-item");

    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace("active", "");
            this.className += " active";
        })
    }
</script>

<script>
    $("#btnToggler").on("click", function() {
        $($(this).attr('data-target')).slideToggle();
    });
</script>