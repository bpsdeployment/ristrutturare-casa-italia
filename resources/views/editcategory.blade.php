<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ristrutturare Casa Italia - Admin panel</title>
    <!-- plugins:css -->
    <link href={{ asset('plugins/font-awesome/css/font-awesome.min.css') }}rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('js/vendor.bundle.base.js') }}>
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href={{ asset('css/adminstyle.css') }}>
    <!-- endinject -->
    <link rel="shortcut icon" href={{ asset('images/Favicon_RCI.png') }} />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .img-wraps {
            position: relative;
            display: inline-block;

            font-size: 0;
        }

        .img-wraps .closes {
            position: absolute;
            top: 5px;
            right: 8px;
            z-index: 100;
            background-color: #FFF;
            padding: 4px 3px;

            color: #000;
            font-weight: bold;
            cursor: pointer;

            text-align: center;
            font-size: 22px;
            line-height: 10px;
            border-radius: 50%;
            border: 1px solid red;
        }

        .img-wraps:hover .closes {
            opacity: 1;
        }

    </style>

</head>

<body>
    <div class="container-scroller">

        @include('adminsidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Modifica prodotto/materiale</h3>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::open(['method' => 'POST', 'route' => 'editCategory', 'files' => 'true']) !!}
                            {{ Form::hidden('id', $id) }}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Nome
                                        {!! Form::label('Nome', null, ['class' => 'sr-only', 'for' => 'category_name']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::text('category_name', $category_name, ['class' => 'form-control', 'required' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        Descrizione
                                        {!! Form::label('Descrizione', null, ['class' => 'sr-only', 'for' => 'category_description']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        {!! Form::textarea('category_description', $category_description, ['class' => 'form-control', 'rows' => 4, 'required' => 'false']) !!}
                                    </div>
                                </div>
                            </div>
                            <!--
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3">Immagini
                                            {!! Form::label('Immagini', null, ['class' => 'sr-only', 'for' => 'category_thumbnail']) !!}
                                        </div>
                                        <div class="col-lg-7">
                                            <input required type="file" class="form-control-lg form-control" name="images[]" multiple>
                                        </div>
                                    </div>
                                </div>-->

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Copertina
                                    </div>
                                    <div class="col-lg-7">
                                        @php
                                            $images_found = file_exists(public_path() . $path);
                                        @endphp
                                        @if ($images_found)
                                            <div class="img-wraps">
                                                <img class="img-responsive"
                                                    src={{ Thumbnail::src(public_path() . $path)->smartcrop(100, 100)->save()->url() }} />
                                            </div>
                                        @else
                                            {{ 'Non sono presenti immagini per questo prodotto' }}
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">
                                        {!! Form::label('Immagini', null, ['class' => 'sr-only', 'for' => 'category_image']) !!}
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="file" class="" name="images[]">
                                    </div>
                                </div>
                            </div>
                            <!--//form-group-->
                            <button type="submit" class="btn btn-lg btn-primary">Aggiorna</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('adminfooter')
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>

    <!-- page-body-wrapper ends -->
    </div>

    <!-- container-scroller -->
    <script src={{ asset('js/vendor.bundle.base.js') }}></script>

    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src={{ asset('js/off-canvas.js') }}></script>
    <script src={{ asset('js/hoverable-collapse.js') }}></script>
    <script src={{ asset('js/misc.js') }}></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src={{ asset('js/file-upload.js') }}></script>

    <!-- End custom js for this page -->
</body>

</html>
