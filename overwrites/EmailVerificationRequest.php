<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use App\Models\VerificationMail;

class EmailVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Method changed to avoid needing user to be logged in as they verify mail
        /*
        if (! hash_equals((string) $this->route('id'),
                          (string) $this->user()->getKey())) {
            return false;
        }

        if (! hash_equals((string) $this->route('hash'),
                          sha1($this->user()->getEmailForVerification()))) {
            return false;
        }
        */
        $id = $this -> route('id');
        $new_user = User::findOrFail($id);
        $new_user_mail = $new_user->mail;
        $mail_waiting_for_verification = VerificationMail::where('mail', $new_user_mail)->
firstOrFail();
        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Fulfill the email verification request.
     *
     * @return void
     */
    public function fulfill($id)
    {
        $user = User::findOrFail($id);
        if (! $user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
            event(new Verified($user));
        }
        //After verification the pending mail VerificationMail table is deleted
        $verified_user = User::findOrFail($id);
        $verified_mail = $verified_user->mail;
        VerificationMail::where('mail', $verified_mail)->firstOrFail()->delete();
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        return $validator;
    }

}