<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('contactreply')
                    ->from('trsmailing.dev@gmail.com', 'TRS Support')
                    ->subject('Re: ' . $this->data['subject'])
                    ->with([ 'reply_text' => $this->data['message'], 'name' => $this->data['name']]);
    }
}
