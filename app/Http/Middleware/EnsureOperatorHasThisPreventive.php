<?php

namespace App\Http\Middleware;

use App\Models\Order;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EnsureOperatorHasThisPreventive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userId = $request->id;
        $targetPreventive = Order::find($userId);

        if ($targetPreventive) {
            $isSiteMaster = $request->session()->get('is_site_master', 0);
            if ($isSiteMaster) {
                return $next($request);
            }

            if (User::find($targetPreventive->customer_id)->reference_admin_id != Session::get('admin_id')) {
                return redirect()->route('orders-table');
            } else {
                return $next($request);
            }
        } else {
            return redirect()->route('orders-table');
        }
    }
}
