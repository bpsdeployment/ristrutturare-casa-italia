<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\Authorization;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class EnsureUserHasFirstPageRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->get('is_site_master', '0')) {
            return $next($request);
        }

        $admin_profile = Admin::find(Session::get('admin_id'));

        if (!$admin_profile->edit_first_page_products) {
            return redirect()->route('orders-table');
        } else {
            return $next($request);
        }
    }
}
