<?php
namespace App\Http\Middleware;

use App\Models\Authorization;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Facades\Session;

class EnsureUserHasOrderRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->get('is_site_master', '0')) {
            return $next($request);
        }
/*
        $admin_profile = Admin::find(Session::get('admin_id'));

        if ($admin_profile->r_categories_auth == 1) {
            return $next($request);
        } else {
            return redirect()->route('calendar-page')->with('message', 'Non sei autorizzato a vedere la lista delle categorie.');
        }
        */
        $user = Auth::user();
        $user_auth = Authorization::where('user_id', $user->id)->first();
        //If user doesn't exists in Auth table
        if (!$user_auth->edit_orders) {
            return redirect()->route('user.page');
        } else {
            return $next($request);
        }    
        
    }
}
