<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EnsureUserIsSiteMaster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $is_site_master = $request->session()->get('is_site_master', 0);
        $admin_mail = $request->session()->get('admin_email', '');
        
        if(!$is_site_master)// && $admin_mail != 'rigerslamaj83@gmail.com') 
        {            
            Log::debug('Attempting to access reserved pages. Redirect.');
            return redirect()->route('user-view')->with('message', 'You do not have permission to manage admins.');
        }
        else
        {
            return $next($request);
        }
    }
}
