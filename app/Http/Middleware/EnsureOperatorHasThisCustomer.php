<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EnsureOperatorHasThisCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userId = $request->id;
        $targetUser = User::find($userId);

        if($targetUser)
        {
            $isSiteMaster = $request->session()->get('is_site_master', 0);
            if($isSiteMaster)
            {
                return $next($request);
            }

            if($targetUser->reference_admin_id != Session::get('admin_id'))
            {
                return redirect()->route('user-view');
            }
            else
            {
                return $next($request);
            }
        }
        else
        {
            return redirect()->route('user-view');
        }

    }
}
