<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureUserEnteredAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //Check if user has completed registration in the last 2 hours (session-lifetime)
        if(!$request->session()->has('email-sent'))
        {
            return redirect()->route('index');
        }

        return $next($request);


    }
}
