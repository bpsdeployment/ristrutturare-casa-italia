<?php

namespace App\Http\Middleware;

use App\Models\Authorization;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureUserHasCouponRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->get('is_site_master', '0')) {
            return $next($request);
        }

        $user = Auth::user();
        $user_auth = Authorization::where('user_id', $user->id)->first();
        //If user doesn't exists in Auth table
        if (!$user_auth->edit_coupons) {
            return redirect()->route('user.page');
        } else {
            return $next($request);
        }
    }
}
