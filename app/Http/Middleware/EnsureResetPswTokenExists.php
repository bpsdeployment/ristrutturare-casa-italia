<?php

namespace App\Http\Middleware;

use App\Models\PasswordResetRequest;
use Closure;
use Illuminate\Http\Request;

class EnsureResetPswTokenExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->token;
        if(PasswordResetRequest::where('token', $token)->exists())
        {
            return $next($request);
        }
        else
        {
            return redirect()->route('index');
        }
    }
}
