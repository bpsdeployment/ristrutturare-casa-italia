<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'check.email.sent' => \App\Http\Middleware\EnsureUserEnteredAddress::class,
        'has.product.role' => \App\Http\Middleware\EnsureUserHasProductRole::class,
        'has.category.role' => \App\Http\Middleware\EnsureUserHasCategoryRole::class,
        'has.contact.role' => \App\Http\Middleware\EnsureUserHasContactRole::class,
        'has.coupon.role' => \App\Http\Middleware\EnsureUserHasCouponRole::class,
        'has.order.role' => \App\Http\Middleware\EnsureUserHasOrderRole::class,
        'sitemaster' => \App\Http\Middleware\EnsureUserIsSiteMaster::class,
        'token.exists' => \App\Http\Middleware\EnsureResetPswTokenExists::class,
        'has.cart' => \App\Http\Middleware\EnsureCartIsNotEmpty::class,
        'auth.admin' => \App\Http\Middleware\CheckAdminAuth::class,
        'user.customer' => \App\Http\Middleware\UserIsCustomer::class,
        'op.has.this.user' => \App\Http\Middleware\EnsureOperatorHasThisCustomer::class,
        'op.has.this.order' => \App\Http\Middleware\EnsureOperatorHasThisPreventive::class,
    ];
}
