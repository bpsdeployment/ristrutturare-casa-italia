<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Invite;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function ShowAdminViewPage()
    {
    }
    public function sendInvitation(Request $request)
    {
        $validated = $request->validate([
            'invite_email' => 'required|email|unique:admins,mail',
        ]);

        do {
            $token = Str::random(20);
        } while (Admin::where('invitation_token', $token)->first());

        $newAdmin = new Admin();
        $newAdmin->invitation_token = $token;
        $newAdmin->mail = $request->input('invite_email');
        $newAdmin->edit_products = ($request->product_auth == "true") ? 1 : 0;
        $newAdmin->edit_categories = ($request->category_auth == "true") ? 1 : 0;
        $newAdmin->edit_first_page_products = ($request->first_page_auth == "true") ? 1 : 0;
        $newAdmin->selling_commission_perc = $request->selling_commission;
        $newAdmin->save();

        $url = URL::temporarySignedRoute(
            'admin-invitation',
            now()->addMinutes(1440),
            ['token' => $token]
        );

        Notification::route('mail', $request->input('invite_email'))->notify(new Invite($url));
        return redirect()->route('invite-admin')->with('message', 'E\' stato mandato un invito all\'indirizzo email indicato.');
    }

    public function completeAdminRegistration(Request $request)
    {
        Log::Debug($request);
        $token = $request->session()->token();
        $token = csrf_token();
        echo $token;

        $validated = $request->validate([
            'g-recaptcha-response' => 'required|captcha',
            'password' => 'required|alpha_dash|min:8|same:confirm_password',
            'confirm_password' => 'required|alpha_dash|min:8'
        ]);

        $admin_token = $request->token;
        $admin = Admin::where('invitation_token', $admin_token)->first();
        if ($admin) {
            Log::debug('found amdin: ' . $admin);
            $admin->name = $request->name;
            $admin->surname = $request->surname;
            $admin->tel = $request->tel;
            $admin->codice_fiscale = $request->codice_fiscale;
            $admin->p_iva = $request->p_iva;
            $admin->n_iscr_albo = $request->n_iscr_albo;
            $admin->updated_at = date("Y-m-d H:i:s");

            $admin->password_hash = Hash::make($request->password);
            Log::debug('saving amdin: ' . $admin);
            $admin->save();
            Log::debug('Saved.');
            return redirect()->route('login')->with('message', 'Registrazione completata con successo.');
        } else {
            return redirect()->route('landing')->with('message', 'Non è stato possibile completare la registrazione. Conttatare l\'admin del sito');
            Log::debug('Admin not found.');
            //return redirect()->route('landing');
        }
    }

    /*    /*
        Log::Debug($request);
        
        $validated = $request->validate([
            'g-recaptcha-response' => 'required|captcha',
            'password' => 'required|alpha_dash|min:8|same:confirm_password',
            'confirm_password' => 'required|alpha_dash|min:8'
        ]);

        $token = $request->token;
        $admin = Admin::where('invitation_token', $token)->first();
        Log::debug('found amdin: ' . $admin);
        if ($admin) {
            $admin->name = $request->name;
            $admin->surname = $request->surname;
            $admin->tel = $request->tel;
            $admin->codice_fiscale = $request->codice_fiscale;
            $admin->p_iva = $request->p_iva;
            $admin->n_iscr_albo = $request->n_iscr_albo;
            
            $admin->password_hash = Hash::make($request->password);
            //$admin->save();
            return redirect()->route('login')->with('message', 'Registrazione completata con successo.');
        } else {
            return redirect()->route('landing')->with('message', 'Non è stato possibile completare la registrazione. Conttatare l\'admin del sito');
        }
    }
    */

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'mail' => 'required|exists:admins,mail',
            'password' => 'required|min:8',
            'g-recaptcha-response' => 'required|captcha',
        ]);


        $admin = Admin::where('mail', $request->mail)->first();
        if ($admin) {
            //Mail found
            if (Hash::check($request->password, $admin->password_hash)) {

                //Check if the user is the site master
                Session::put('is_admin', 1);
                Session::put('admin_id', $admin->id);
                Session::put('admin_email', $admin->mail);
                if ($request->mail == env('ADMIN_EMAIL') || $request->mail == 'rigerslamaj83@gmail.com' || $request->mail == 'manuelaciulla@gmail.com' || $request->mail == 'andrea.costa@bps.srl') {
                    Session::put('is_site_master', 1);
                }

                return redirect()->route('calendar-page');
            }
            //Mail not found
            else {
                return back()->withErrors([
                    'password' => 'La combinazione inserita non è corretta.',
                ]);
            }
        }
    }

    public function RenderAdminPage()
    {
        return redirect()->route('calendar-page');
        /*
        if (request()->session()->get('is_site_master')) {

            $new_users_chart_options = [
                'chart_title' => 'Nuovi utenti per settimana',
                'report_type' => 'group_by_date',
                'model' => 'App\Models\User',
                'group_by_field' => 'email_verified_at',
                'group_by_period' => 'week',
                'chart_type' => 'bar',
                'date_format' => 'w-F-Y'
            ];
            $order_chart_options = [
                'chart_title' => 'Vendite per giorno',
                'chart_type' => 'line',
                'report_type' => 'group_by_date',
                'model' => 'App\Models\Order',
                'group_by_field' => 'created_at',
                'group_by_period' => 'day',
                'aggregate_function' => 'sum',
                'aggregate_field' => 'total_price',
                'filter_field' => 'created_at',
                'filter_days' => 30,
                'date_format' => 'd m Y'
            ];
            $new_users_chart = new LaravelChart($new_users_chart_options);
            $order_chart = new LaravelChart($order_chart_options);
            return view('adminpage', ['chart1' => $new_users_chart, 'chart2' => $order_chart]);
        } else {
            $admin_profile = Admin::find(Session::get('admin_id'));
            if ($admin_profile->edit_products) {
                return redirect()->route('products-table');
            } else if ($admin_profile->edit_categories) {
                return redirect()->route('categories-table');
            } else if ($admin_profile->edit_first_page_products) {
                return redirect()->route('products-table');
            } else {
                return redirect()->route('orders-table');
            }
        }
        */
    }

    public function logout()
    {
        Session::forget('is_admin');
        Session::forget('admin_id');
        Session::forget('admin_email');
        Session::forget('is_site_master');

        return redirect()->route('login');
    }

    public function fillTable(Request $request)
    {
        if ($request->ajax()) {
            $data = Admin::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->rawColumns(['complete_name'])
                ->addColumn('complete_name', function ($row) {
                    return $row->name . " " . $row->surname;
                })
                ->escapeColumns('complete_name')
                ->rawColumns(['customer_count'])
                ->addColumn('customer_count', function ($row) {
                    return count(User::where('reference_admin_id', $row->id)->get());
                })
                ->escapeColumns('customer_count')
                ->rawColumns(['annual_income'])
                ->addColumn('annual_income', function ($row) {
                    $orders = Order::where('creator_id', $row->id)->where('accepted_by_customer', 1)->get();
                    $income_sum = 0;
                    foreach ($orders as $order) {
                        $income_sum += $order->total;
                    }
                    return number_format($income_sum, 2, ",", ".") . ' €';
                })
                ->escapeColumns('annual_income')
                ->addColumn('action', function ($row) {
                    $actionBtn = '<form action=' . route('show-admin-details', $row) . '>
                    <input type="hidden" name="targetYear" value=' . date('Y') . '>
                    <button class="edit btn btn-info btn-sm"   type="submit">Dettagli</button>
                    </form>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function StoreNewAdmin(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|unique:admins,mail|max:255',
        ]);

        $newAdmin = new Admin();
        $newAdmin->name = $request->name;
        $newAdmin->surname = $request->surname;
        $newAdmin->mail = $request->email;
        $newAdmin->tel = $request->phone;
        $newAdmin->address = $request->addr;
        $newAdmin->codice_fiscale = $request->codice_fiscale;
        $newAdmin->p_iva = $request->p_iva;
        $newAdmin->n_iscr_albo = $request->n_iscr_albo;

        $newAdmin->r_products_auth = ($request->r_products_auth == 'on') ? 1 : 0;
        $newAdmin->u_products_auth = ($request->u_products_auth == 'on') ? 1 : 0;
        $newAdmin->d_products_auth = ($request->d_products_auth == 'on') ? 1 : 0;
        $newAdmin->c_products_auth = ($request->c_products_auth == 'on') ? 1 : 0;
        $newAdmin->r_categories_auth = ($request->r_categories_auth == 'on') ? 1 : 0;
        $newAdmin->u_categories_auth = ($request->u_categories_auth == 'on') ? 1 : 0;
        $newAdmin->d_categories_auth = ($request->d_categories_auth == 'on') ? 1 : 0;
        $newAdmin->c_categories_auth = ($request->c_categories_auth == 'on') ? 1 : 0;
        $newAdmin->u_first_page_auth = ($request->u_first_page_auth == 'on') ? 1 : 0;

        do {
            $token = Str::random(20);
        } while (Admin::where('invitation_token', $token)->first());
        $newAdmin->invitation_token = $token;

        $newAdmin->save();

        $url = URL::temporarySignedRoute(
            'admin-invitation',
            now()->addMinutes(1440),
            ['token' => $token]
        );

        Notification::route('mail', $request->input('email'))->notify(new Invite($url));
        return redirect()->route('admin-view')->with('message', 'E\' stato mandato un invito all\'indirizzo email indicato.');

        echo $newAdmin;
    }


    public function addAdmin()
    {
        return view('add-admin');
    }

    public function inviteAdmin()
    {
        return view('invite-admin');
    }

    public function ShowAdminDetails(Request $request)
    {
        $targetAdmin = Admin::find($request->id);
        $customers = User::where('reference_admin_id', $request->id)->get();
        $orders = Order::where('creator_id', $request->id)->get();

        $currentYearPercMarginCollection = array();
        $currentYearPercDiscountCollection = array();
        $currentYearPercCommissionCollection = array();
        $orderIDCollection  = array();
        $currentYearOrders = Order::where('accepted_by_customer', 1)->where('creator_id', $request->id)->whereYear('creation_date', '=', $request->targetYear)->get();
        foreach ($currentYearOrders as $order) {
            $orderIDCollection[] = "#" . $order->id;
            $orderInfo = OrderController::GetOrderInfo($order, $targetAdmin->selling_commission_perc);
            $currentYearPercMarginCollection[] = round($orderInfo['perc_margin'], 2);
            $currentYearPercDiscountCollection[] = round($orderInfo['perc_discount'], 2);
            $currentYearPercCommissionCollection[] = round($orderInfo['perc_commission'], 2);
        }

        //Preventive count
        $yearlyPreventiveCountDataPoints[] = array(
            "name" => "Margine %",
            "data" => $currentYearPercMarginCollection,
        );

        $yearlyPreventiveCountDataPoints[] = array(
            "name" => "Sconti %",
            "data" => $currentYearPercDiscountCollection,
        );

        $yearlyPreventiveCountDataPoints[] = array(
            "name" => "Commissione %",
            "data" => $currentYearPercCommissionCollection,
        );

        $year_list = array();
        $orders = Order::where('creator_id', $request->id)->get();
        foreach ($orders as $order) {
            $year = date('Y', strtotime($order->creation_date));
            if (!in_array($year, $year_list)) {
                array_push($year_list, $year);
            }
        }

        return view('view-admin-details', [
            'admin' => $targetAdmin, 'customers' => $customers, 'orders' => $orders,
            'margin_discount_data' => json_encode($yearlyPreventiveCountDataPoints),
            'data_point_labels' => json_encode($orderIDCollection),
            'admin_id' => $request->id,
            'year_list' => $year_list,
            'targetYear' => $request->targetYear
        ]);
    }

    public function EditAdminData(Request $request)
    {

        $existingMail = Admin::where('mail', $request->email)->first();
        if ($existingMail && $existingMail->id != $request->id) {
            return back()->with('error_message', 'Mail esistente.');
        }

        /* Storing new Admin details */
        $targetAdmin = Admin::find($request->id);

        $targetAdmin->name = $request->name;
        $targetAdmin->surname = $request->surname;
        $targetAdmin->mail = $request->email;
        $targetAdmin->tel = $request->tel;
        $targetAdmin->address = $request->address;
        $targetAdmin->codice_fiscale = $request->codice_fiscale;
        $targetAdmin->p_iva = $request->p_iva;
        $targetAdmin->n_iscr_albo = $request->n_iscr_albo;

        $targetAdmin->r_products_auth = (isset($request->r_products_auth) ? 1 : 0);
        $targetAdmin->u_products_auth = (isset($request->u_products_auth) ? 1 : 0);
        $targetAdmin->d_products_auth = (isset($request->d_products_auth) ? 1 : 0);
        $targetAdmin->c_products_auth = (isset($request->c_products_auth) ? 1 : 0);
        $targetAdmin->r_categories_auth = (isset($request->r_categories_auth) ? 1 : 0);
        $targetAdmin->u_categories_auth = (isset($request->u_categories_auth) ? 1 : 0);
        $targetAdmin->d_categories_auth = (isset($request->d_categories_auth) ? 1 : 0);
        $targetAdmin->c_categories_auth = (isset($request->c_categories_auth) ? 1 : 0);
        $targetAdmin->u_first_page_auth = (isset($request->u_first_page_auth) ? 1 : 0);
        Log::debug('Saving ' . $targetAdmin);
        $targetAdmin->save();
        Log::debug('Saved.');

        return redirect()->route('show-admin-details', ['id' => $request->id])->with('message', 'Dettagli modificati.');
    }

    public function ShowEditAdminDataPage(Request $request)
    {
        $targetAdmin = Admin::find($request->id);
        Log::debug('going to edit admin page');


        return view('edit-admin')->with('admin', $targetAdmin);
    }

    public function manageAutorization(Request $request)
    {
        $product_out = null;
        $categories_out = null;
        $first_page_out = null;

        $admin = Admin::find($request->admin_id);
        if ($admin->edit_products == 1)
            $product_out = 'true';
        else
            $product_out = 'false';
        if ($admin->edit_categories == 1)
            $categories_out = 'true';
        else
            $categories_out = 'false';
        if ($admin->edit_first_page_products == 1)
            $first_page_out = 'true';
        else
            $first_page_out = 'false';

        return view('manage-autorization')->with('product_out', $product_out)->with('categories_out', $categories_out)
            ->with('first_page_out', $first_page_out)->with('id', $request->admin_id);
    }

    public function updateAdminOut(Request $request)
    {
        $admin = Admin::find($request->admin_id);

        if ($request->product_auth == 'true')
            $admin->edit_products = 1;
        else
            $admin->edit_products = 0;
        if ($request->category_auth == 'true')
            $admin->edit_categories = 1;
        else
            $admin->edit_categories = 0;
        if ($request->first_page_auth == 'true')
            $admin->edit_first_page_products = 1;
        else
            $admin->edit_first_page_products = 0;

        $admin->save();

        return back()->with('message', 'Autorizzazioni aggiornate');
    }

    //Mostra la pagina che viene mostrata al NUOVO admin quando clicca nel link ricevuto per mail
    //passa un token generato casualmente 
    public function showAdminRegistrationPage(Request $request)
    {
        $targetAdmin = AdminController::RetrieveAdminByToken($request->token);
        if ($targetAdmin != null) {
            //echo 'found new admin';
        } else {
            //echo 'new admin not found';
        }
        //echo ($targetAdmin);
        return view('accept-admin-invitation', ['admin' => $targetAdmin]);
        //devo trovcare il modo di trovare l'admin a partire dal token
        //aprire la view accept-admin-invitation popolando i campi che ho nel database PER CONFERMA da parte del nuovo admin
        //il form nella view sopracitata avrà una route che va a salvare i dati nel db una volta submittati
        //
    }


    public function SetSellingCommissionPercentage(Request $request)
    {
        $targetAdmin = Admin::find($request->admin_id);
        $targetAdmin->selling_commission_perc = $request->selling_commission;
        $targetAdmin->save();
        return back()->with('message', 'Commissione aggiornata');
    }

    public function RetrieveAdminByToken($token)
    {

        $admins = Admin::All();

        foreach ($admins as $admin) {
            if (isset($admin->invitation_token) && $admin->invitation_token == $token) {
                return $admin;
            }
        }
        return null;
    }

    public static function RetrieveAdminList()
    {
        $admins = Admin::all();
        $admin_list = array();
        foreach ($admins as $admin) {
            $admin_list[$admin->id] = $admin->name . " " . $admin->surname;
        }
        return $admin_list;
    }

    public function deleteAdmin(Request $request)
    {
        //An admin cannot delete theirself
        if (Session::has('admin_id')) {
            if ($request->id == Session::get('admin_id')) {
                return redirect()->route('admin-view')->with('message', 'Non è possibile eliminare l\'account quando sei loggato.');
            }
        }
        //Find admin
        $targetAdmin = Admin::find($request->id);
        if (!$targetAdmin) {
            return redirect()->route('admin-view')->with('message', 'Non è stato possibile trovare l\'admin cercato (id ' . $request->id . ').');
        }

        //Find all clients that have this admin as admin_reference and set it to sitemaster
        if (DB::table('users')->where('reference_admin_id', $targetAdmin->id)->exists()) {
            $targetUsers = DB::table('users')->where('reference_admin_id', $targetAdmin->id)->get();
            foreach ($targetUsers as $user) {
                $targetUser = User::find($user->id);
                Log::debug('The user (ID: ' . $targetUser->id . ') was created by this admin. Changing admin_reference_id to sitemaster\'s: ');
                if (Session::has('admin_id')) {
                    Log::debug(Session::get('admin_id'));
                    $targetUser->reference_admin_id = Session::get('admin_id');
                } else {
                    //TODO Find admin with mail 'rigerslamaj83@gmail.com, get the id and set the user's ref id to that value
                    Log::debug('Could not find admin id, setting user\' admin ref id to 3');
                    $targetUser->reference_admin_id = 3;
                }

                if ($targetUser->save()) {
                    Log::debug('Cliente modificato: ' . $targetUser);
                } else {
                    Log::debug('Could not edit user.' . $targetUser);
                }
            }
        }

        //Find all the orders that were created by this admin and change the admin reference to the sitemaster
        if (DB::table('orders')->where('creator_id', $targetAdmin->id)->exists()) {
            $targetOrders = DB::table('orders')->where('creator_id', $targetAdmin->id)->get();
            foreach ($targetOrders as $order) {
                $targetOrder = Order::find($order->id);
                Log::debug('The order (ID: ' . $targetOrder->id . ') was created by this admin (id: ' . $targetOrder->creator_id . '). Changing creator_id to sitemaster\'s: ');

                if (Session::has('admin_id')) {
                    Log::debug(Session::get('admin_id'));
                    $targetOrder->creator_id = Session::get('admin_id');
                } else {
                    //TODO Find admin with mail 'rigerslamaj83@gmail.com, get the id and set the order's creator_id to that value
                    Log::debug('Could not find admin id, setting orders creator_id to 3');
                    $targetOrder->creator_id = 3;
                }

                if ($targetOrder->save()) {
                    Log::debug('Preventivo modificato: ' . $targetOrder);
                } else {
                    Log::debug('Could not edit order.' . $targetOrder);
                }
            }
        }
        
        if ($targetAdmin->delete()) {
            return redirect()->route('admin-view')->with('message', 'Admin eliminato con successo');
        } else {
            return redirect()->route('admin-view')->with('message', 'Non è stato possibile elminare l\'admin');
        }
    }
}
