<?php

namespace App\Http\Controllers;

use App\Models\Checkout;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\PayPal as PayPalClient;


class PayPalController extends Controller
{

    public function successTransaction(Request $request)
    {
        $data = $request->data;
        //Get checkout row for this transaction
        $checkout = Checkout::find($request->checkout_id);
        $checkout->order_completed = true;
        $checkout->save();
        //Create an Order object and fill it with form info
        $order = new Order;
        $order->user_id = $checkout->user_id;
        $order->item_list = $checkout->item_list;
        $order->order_state = 0;
        $order->total_price = $checkout->total_price;
        $order->delivery_address = $checkout->delivery_address;
        $order->billing_address = $checkout->billing_address;
        $order->phone_number =  $checkout->phone_number;
        $order->payment_method = "Paypal";
        $order->payment_id = $data['id'];
        $order->payer_id = $data['payer']['payer_id'];
        $order->save();

        //Update user information
        $user = User::find($checkout->user_id);
        if ($request->save_user_info == "1") {
            //Update user profile with form info for the next order
            $fields = explode(",", $checkout->delivery_address);
            $user->address_r1 = $fields[2];
            $user->address_r2 = $fields[3];
            $user->city = $fields[4];
            $user->province = $fields[5];
            $user->CAP = $fields[6];
            $user->phone = $checkout->phone_number;
        }
        $user->total_spent += $order->total_price;
        $user->save();

        //Update item sold counter for each product in the cart
        foreach(json_decode($checkout->item_list, true) as $item)
        {
            $target_item = Product::find($item['id']);
            $target_item->units_sold++;
            $target_item->save();
        }
        Cart::destroy();
        return response()->json(['status' => 'done']);
    }
}
