<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->addColumn('action', function ($row) {
                    //Getting the admin for authorization purpose (CRUD)
                    $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
                    $actionBtn = '<a href="' . route("showCategoryEditPage", $row) . '" class="edit btn btn-success btn-sm"';
                    //if the admin cannot modify the category, button will not be shown
                    if ($admin->u_categories_auth == 0) {
                        $actionBtn .= ' style="background-color:#6AB190; pointer-events: none;"';
                    }
                    $actionBtn .= '>Modifica</a> <a href="' . route("deleteCategory", $row) . '" class="delete btn btn-danger btn-sm"';
                    //if the admin cannot delet the category, button will not be shown
                    if ($admin->d_categories_auth == 0) {
                        $actionBtn .= ' style="background-color:#E87C87; pointer-events: none;"';
                    }
                    $actionBtn .= '>Cancella</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'regex:/^[a-zA-Z0-9 ]*$/|unique:categories,name',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $category = new Category();
        $category->name = $request->category_name;
        $category->description = $request->category_description;


        //Directory creation
        //The product name, trimmed of every space, is used as folder name
        $path = '/images/categories_thumbnail/';

        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $img = Image::make($image->path());
                $filename = str_replace(' ', '', $image->getClientOriginalName());
                // Image resize to given aspect dimensions
                // Save this thumbnail image to /public/thumbnails folder
                $img->resize(400, 400)->save(public_path() . $path . $filename);
            }
        }

        //Check if there are less than 6 front-page categories
        $first_page_items = Category::where('is_first_page', true)->get();
        if (count($first_page_items) < 6) {
            $category->is_first_page = true;
        }

        $category->thumbnail_path = $path . $filename;
        $category->save();
        return back()->with('message', 'Categoria inserita correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
        if ($admin and ($admin->u_categories_auth == 0)) {
            return redirect()->route('categories-table')->with('message', 'Non sei autorizzato a modificare le categorie.');
        }

        $validation = $this->validate($request, [
            'category_name' => 'regex:/^[a-zA-Z0-9 ]*$/|unique:categories,name,' . $request->id,
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $category = Category::findOrFail($request->id);

        //Update category name on every product using the updated category
        $products = Product::where('type', $category->name)->get();
        foreach ($products as $product) {
            $product->type = $request->category_name;
            $product->update();
        }
        //Update category
        $category->name = $request->category_name;
        $category->description = $request->category_description;
        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $filename = $image->getClientOriginalName();
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(400, 400)->save(public_path() . "/images/categories_thumbnail/" . $filename);
            }
            unlink(public_path() . $category->thumbnail_path);
            $category->thumbnail_path = "/images/categories_thumbnail/" . $filename;
        }
        $category->update();


        return redirect()->route('categories-table')->with('message', 'Categoria aggiornata correttamente.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        //It's not possible to delete a category which collects products

        $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
        if ($admin and ($admin->d_categories_auth == 0)) {
            return redirect()->route('categories-table')->with('message', 'Non sei autorizzato a cancellare le categorie.');
        }
        
        if ($category->product_count != 0) {
            return back()->with('message', 'Non è possibile cancellare una categoria che contiene ancora dei prodotti.');
        } else {
            //Remove thumbnail image
            $thumb_path = $category->thumbnail_path;
            if (file_exists(public_path() . $thumb_path)) {
                unlink(public_path() . $thumb_path);
            }
            //Delete db entry
            $category->delete();
            return back()->with('message', 'Categoria cancellata correttamente.');
        }
    }

    public function showCategoryEditPage($id)
    {
        $category = Category::findOrFail($id);
        $name = $category->name;
        $description = $category->description;
        $path = $category->thumbnail_path;

        return view('editcategory')
            ->with('id', $id)
            ->with('category_name', $name)
            ->with('category_description', $description)
            ->with('path', $path);
    }

    public static function RetrieveCategoryMatrix()
    {
        $categories = Category::all();
        $items_per_row = 4;
        $row = array();
        $category_count = count($categories);
        foreach ($categories as $category) {
            array_push($row, $category->name);
        }
        for ($leftover = 0; $leftover < $items_per_row - ($category_count % $items_per_row); $leftover++) {
            array_push($row, "none");
        }
        return array_chunk($row, $items_per_row, false);
    }
}
