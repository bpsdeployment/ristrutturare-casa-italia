<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Category;
use App\Models\Order;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserStory;
use App\Models\VerificationMail;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laravel\Ui\Presets\React;
use Yajra\DataTables\Facades\DataTables;
use willvincent\Rateable\Rating;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //User input validation
        $validated = $request->validate([
            'mail' => 'required|unique:users|max:255',
            'phone_number' => 'required|max:15',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $new_user = new User;
        $new_user->name = $request['name'];
        $new_user->surname = $request['surname'];
        $new_user->mail = $request['mail'];
        $new_user->phone = $request['phone_number'];
        $new_user->province = $request['prov'];
        $new_user->save();
        return redirect()->route('landing')->with('message', 'Abbiamo ricevuto la tua richiesta, verrai ricontattato da uno dei nostri consulenti.');
    }


    public function storeNewUserFromPreventiveModal(Request $request)
    {

        //Log::debug($request);
        $new_user = new User;
        $new_user->name = $request->name;
        $new_user->surname = $request->surname;
        $new_user->mail = $request->mail;
        $new_user->phone = $request->tel;
        $new_user->address_r1 = $request->addr_r1;
        $new_user->address_r2 = $request->addr_r2;
        $new_user->city = $request->city;
        $new_user->province = $request->prov;
        $new_user->cap = $request->cap;

        //Find reference admin id
        if ($request->reference_admin_id != '') {
            $admin_name = explode(" ", $request->reference_admin_id);
            $admin_id = DB::table('admins')->where('name', $admin_name[0])->where('surname', $admin_name[1])->value('id');
            $new_user->reference_admin_id = $admin_id;
        } else {
            $new_user->reference_admin_id = 1;
        }
        $new_user->save();

        Log::debug('Aggiunto cliente ' . $new_user);
    }

    public function fillTable(Request $request)
    {
        if ($request->ajax()) {
            $data = User::where('reference_admin_id', Session::get('admin_id'))->orderBy('updated_at', 'DESC')->get();
            //Site owner can see every customer
            if (Session::get('is_site_master')) {
                $data = User::latest()->get();
            }
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->rawColumns(['complete_name'])
                ->addColumn('complete_name', function ($row) {
                    return $row->name . " " . $row->surname;
                })
                ->escapeColumns('complete_name')
                ->rawColumns(['reference_admin'])
                ->addColumn('reference_admin', function ($row) {
                    $refAdminId = $row->reference_admin_id;
                    if ($refAdminId) {
                        $admin = Admin::find($refAdminId);
                        if ($admin) {
                            return $admin->name . ' ' . $admin->surname;
                        } else {
                            return 'ID admin non valido (' . $refAdminId . ').';
                        }
                    } else {
                        return "Non assegnato";
                    }
                })
                ->escapeColumns('reference_admin')
                ->editColumn('total_spent', function($row){
                    return number_format($row->total_spent,2,',','.').' €';
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("show-user-details", $row) . '" class="edit btn btn-info btn-sm">Dettagli</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public static function showUserDetails($id)
    {
        $user = User::find($id);
        $last_order = Order::where('customer_id', $user->id)->latest()->first();
        $referent_admin = Admin::find($user->reference_admin_id);
        //Reverse the newest is the first of the list
        $orders = Order::where('customer_id', $user->id)->get()->reverse();
        if (count($orders) == 0) {
            $last_order_date = "Mai ordinato";
        } else {
            $last_order_date = $orders[0]->created_at;
        }
        $notes = UserStory::where('user_id', $user->id)->get()->reverse();
        foreach ($notes as $note) {
            switch ($note->type) {
                case 1:
                    $note->type = "Chiamata";
                    break;
                case 2:
                    $note->type = "Incontro diretto";
                    break;
                case 3:
                    $note->type = "Mail";
                    break;
                case 4:
                    $note->type = "Invio preventivo";
                    break;
                default:
                    $note->type = "N/A";
                    break;
            }
        }

        $metratura_casa = "N/A";
        $nr_bagni = "N/A";
        $stato_cucina = "N/A";
        $stato_bagno = "N/A";
        $stato_riscaldamento = "N/A";
        $stato_condizionatore = "N/A";
        $stato_controsoffitto = "N/A";
        $stato_pavimento = "N/A";
        $budget = "N/A";
        $from_siteground = true;

        if (!$user->external_submission_id) {
            $from_siteground = false;
        } else {
            $additionalInfoCollection = json_decode($user->additional_info, true);

            foreach ($additionalInfoCollection as $additionalInfo) {
                //Metratura casa
                if (array_key_exists('aub891u', $additionalInfo)) {
                    $metratura_casa = $additionalInfo['aub891u'];
                } else if (array_key_exists('name', $additionalInfo)) {
                    $metratura_casa = $additionalInfo['name'];
                }

                //Numero di bagni
                if (array_key_exists('igy8cm9', $additionalInfo)) {
                    $nr_bagni = $additionalInfo['igy8cm9'];
                } else if (array_key_exists('field_8cd735e', $additionalInfo)) {
                    $nr_bagni = $additionalInfo['field_8cd735e'];
                }

                //Stato cucina
                if (array_key_exists('field_ce8e6ea', $additionalInfo)) {
                    $stato_cucina = $additionalInfo['field_ce8e6ea'];
                } else if (array_key_exists('0lgkigg', $additionalInfo)) {
                    $stato_cucina = $additionalInfo['0lgkigg'];
                }

                //Stato bagno
                if (array_key_exists('field_9cec832', $additionalInfo)) {
                    $stato_bagno = $additionalInfo['field_9cec832'];
                } else if (array_key_exists('field_8b939aa', $additionalInfo)) {
                    $stato_bagno = $additionalInfo['field_8b939aa'];
                }

                //Stato riscaldamento
                if (array_key_exists('field_daa892e', $additionalInfo)) {
                    $stato_riscaldamento = $additionalInfo['field_daa892e'];
                } else if (array_key_exists('field_8b939aa', $additionalInfo)) {
                    $stato_riscaldamento = $additionalInfo['field_8b939aa'];
                }

                //Stato condizionatore
                if (array_key_exists('field_f00a332', $additionalInfo)) {
                    $stato_condizionatore = $additionalInfo['field_f00a332'];
                } else if (array_key_exists('field_f8543fb', $additionalInfo)) {
                    $stato_condizionatore = $additionalInfo['field_f8543fb'];
                }

                //Stato controsoffitto
                if (array_key_exists('field_1ca1551', $additionalInfo)) {
                    $stato_controsoffitto = $additionalInfo['field_1ca1551'];
                } else if (array_key_exists('field_4b9c2af', $additionalInfo)) {
                    $stato_controsoffitto = $additionalInfo['field_4b9c2af'];
                }

                //Stato pavimento
                if (array_key_exists('field_ffab9d9', $additionalInfo)) {
                    $stato_pavimento = $additionalInfo['field_ffab9d9'];
                }

                //Budget
                if (array_key_exists('field_9ebf005', $additionalInfo)) {
                    $budget = $additionalInfo['field_9ebf005'];
                }
            }
        }

        return view(
            'userdetailsadmin',
            [
                'id' => $id,
                'name' => $user->name,
                'mail' => $user->mail,
                'phone_number' => $user->phone,
                'surname' => $user->surname,
                'addr_1' => $user->address_r1,
                'addr_2' => $user->address_r2,
                'city' => $user->city,
                'prov' => $user->province,
                'cap' => $user->cap,
                'total_spent' => $user->total_spent,
                'registration_date' => $user->created_at,
                'last_order_date' => $last_order_date,
                'orders' => $orders,
                'notes' => $notes,
                'referent_admin' => $referent_admin,
                'from_siteground' => $from_siteground,
                'mq' => $metratura_casa,
                'nr_bagni' => $nr_bagni,
                'stato_cucina' => $stato_cucina,
                'stato_bagno' => $stato_bagno,
                'stato_riscaldamento' => $stato_riscaldamento,
                'stato_condizionatore' => $stato_condizionatore,
                'stato_controsoffitto' => $stato_controsoffitto,
                'stato_pavimento' => $stato_pavimento,
                'budget' => $budget

            ]
        );
    }

    public function addUserStory(Request $request)
    {
        return view('insert-user-story', ['id' => $request->id]);
    }

    public function ShowEditUserDataPage(Request $request)
    {
        $targetUser = User::find($request->id);
        return view('edit-user')->with('user', $targetUser)->with('id', $request->id)->with('admin_list', AdminController::RetrieveAdminList());
    }

    public function EditCustomerData(Request $request)
    {
        $existingMail = User::where('mail', $request->email)->first();
        if ($request->email != '' && $existingMail && $existingMail->id != $request->id) {
            return back()->with('error_message', 'Mail esistente.');
        }
        $targetUser = User::find($request->id);
        $targetUser->name = $request->name;
        $targetUser->surname = $request->surname;
        $targetUser->mail = $request->email;
        $targetUser->phone = $request->phone;
        $targetUser->address_r1 = $request->addr_r1;
        $targetUser->address_r2 = $request->addr_r2;
        $targetUser->city = $request->city;
        $targetUser->CAP = $request->CAP;
        $targetUser->reference_admin_id = Session::get('admin_id');
        if (Session::get('is_site_master', 0)) {
            $targetUser->reference_admin_id = $request->reference_admin_id;
        }
        $targetUser->save();
        return redirect()->route('show-user-details', ['id' => $request->id])->with('message', 'Dettagli modificati.');
    }


    public function RenderAddUserPage()
    {
        return view('add-user')->with('admin_list', AdminController::RetrieveAdminList());
    }

    public function StoreNewCustomer(Request $request)
    {
        $validated = $request->validate([
            'email' => 'max:255',
            //'email' => 'unique:users,mail|max:255',
        ]);
        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->surname = $request->surname;
        $newUser->mail = $request->email;
        $newUser->phone = $request->phone;
        $newUser->address_r1 = $request->addr_r1;
        $newUser->address_r2 = $request->addr_r2;
        $newUser->city = $request->city;
        $newUser->province = $request->prov;
        $newUser->CAP = $request->CAP;
        $newUser->reference_admin_id = Session::get('admin_id');
        if (Session::get('is_site_master', 0)) {
            $newUser->reference_admin_id = $request->reference_admin_id;
        }
        $newUser->save();
        return redirect()->route('add-customer')->with('message', 'Hai aggiunto un nuovo cliente.');
    }

    public static function GetNewUsersFromSiteGround()
    {

        $externalSubmissionsCount = DB::connection('mysql_sg')->table('qev_e_submissions_values')->max('submission_id');
        $registeredExternalSubmissionCount = DB::connection('mysql')->table('users')->max('external_submission_id');
        if ($registeredExternalSubmissionCount == NULL) {
            $registeredExternalSubmissionCount = 0;
        }
        //Sync with external DB
        $newUserCount = 0;
        $updatedUserCount = 0;
        if ($registeredExternalSubmissionCount < $externalSubmissionsCount) {
            for ($submissionId = $registeredExternalSubmissionCount; $submissionId <= $externalSubmissionsCount; $submissionId++) {
                $submissionData = DB::connection('mysql_sg')->table('qev_e_submissions_values')->where('submission_id', $submissionId)->get();
                if (count($submissionData) != 0) {
                    $newExternalUser = new User();
                    $additionalInfoArray = array();
                    foreach ($submissionData as $submissionField) {
                        $newExternalUser->external_submission_id = $submissionId;
                        //return $submissionField->value;
                        //Discard empty fields
                        if (strlen($submissionField->value) != 0) {
                            //Name
                            if (($submissionField->key == 'field_65f22f3' || $submissionData == 'name' || $submissionField->key == 'field_0b30d1f' || $submissionData == 'message') && !str_contains($submissionField->value, 'mq')) {
                                $newExternalUser->name = $submissionField->value;
                            }
                            //Surname
                            else if (($submissionField->key == 'field_c806299' || $submissionField->key == 'field_fa77854' || $submissionField->key == 'field_0a2a88d') && !str_contains($submissionField->value, 'mq')) {
                                $newExternalUser->surname = $submissionField->value;
                            }
                            //Email
                            else if ($submissionField->key == 'field_bcb6e59' || $submissionField->key == 'field_88238c8' || $submissionField->key == 'field_c806299' || $submissionField->key == 'field_5fbe38a' || $submissionField->key == 'email' || $submissionField->key == 'message' && strlen($submissionField->value) < 30) {
                                $newExternalUser->mail = $submissionField->value;
                            }
                            //Phone
                            else if ($submissionField->key == 'field_44c26d8' || $submissionField->key == 'field_e1ddcc0' || $submissionField->key == 'field_aec6242') {
                                $newExternalUser->phone = $submissionField->value;
                            } else if ($submissionField->key == 'field_88f25e6' || $submissionField->key == 'field_df1fbd2') {
                                $newExternalUser->province = $submissionField->value;
                            } else {
                                $additionalInfoArray[] = [$submissionField->key => $submissionField->value];
                            }
                        }
                    }
                    $newExternalUser->additional_info = json_encode($additionalInfoArray);
                    $user = User::where('mail', $newExternalUser->mail)->first();
                    $submissionTime = DB::connection('mysql_sg')->table('qev_e_submissions_actions_log')->where('submission_id', $submissionId)->get();
                    if ($user == null) {
                        $newUserCount++;
                        $newExternalUser->save();
                        $newExternalUser->created_at = $submissionTime[0]->created_at_gmt;
                        $newExternalUser->updated_at = $submissionTime[0]->updated_at_gmt;
                        $newExternalUser->save();
                    }
                    //2022-08-09T07:47:47.000000Z
                    //Aggiorna utente pre esistente
                    else if ($user->updated_at != $submissionTime[0]->updated_at_gmt) {
                        $updatedUserCount++;
                        $user->name = (strlen($newExternalUser->name) > 0) ? $newExternalUser->name : "";
                        $user->surname = (strlen($newExternalUser->surname) > 0) ? $newExternalUser->surname : "";
                        $user->phone = $newExternalUser->phone;
                        $user->province = $newExternalUser->province;
                        $user->save();
                    }
                }
                //Avoid 60 seconds php timeout
                set_time_limit(0);
            }
        }

        if ($newUserCount == 0 && $updatedUserCount == 0) {
            return back()->with('message', 'Nessun nuovo dato sul database di SiteGround.');
        } else if ($newUserCount != 0 && $updatedUserCount == 0) {
            return back()->with('message', 'Aggiunti ' . $newUserCount . ' nuovi utenti.');
        } else if ($newUserCount == 0 && $updatedUserCount != 0) {
            return back()->with('message', 'Aggiornati ' . $updatedUserCount . ' utenti.');
        } else {
            return back()->with('message', 'Aggiunti ' . $newUserCount . ' nuovi utenti. Aggiornati '  . $updatedUserCount . ' utenti.');
        }
    }

    public function deleteUser(Request $request)
    {
        //Find the user
        $targetUser = User::find($request->id);

        if(!$targetUser){
            return redirect()->route('user-view')->with('message', 'Cliente non trovato. Contattare admin.');
        }          

        //Find all the orders that are associated with the user
        if ($userOrders = DB::table('orders')->where('customer_id', $targetUser->id)->exists()) {
            $userOrders = DB::table('orders')->where('customer_id', $targetUser->id)->get();
            foreach ($userOrders as $order) {
                $targetOrder = Order::find($order->id);
                Log::debug('Changing customer reference to order ' . ($targetOrder));

                if (Session::has('admin_id')) {
                    $targetOrder->customer_id = Session::get('admin_id');
                } else {
                    Log::debug('Could not find admin id in Session, setting order\'s customer id to 3');
                    //TODO If admin id is not found, it will be set to admin's id rigers lamaj
                    $targetOrder->customer_id = 3;
                }
                $targetOrder->save();
            }
        }

        if ($targetUser->delete()) {
            Log::debug('Cliente eliminato: ' . $targetUser);
            return redirect()->route('user-view')->with('message', 'Cliente eliminato con successo.');
        } else {
            Log::debug('Could not delete client.');
            return redirect()->route('landing')->with('message', 'Non è stato possibile elminare il cliente.');
        }
    }
}
