<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\PasswordResetRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Admin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:admins,mail',
        ]);

        $token = Str::random(64);

        $reset_request = new PasswordResetRequest();
        $reset_request->email = $request->email;
        $reset_request->token = $token;
        $reset_request->save();

        //forgetPassword is the blade view being sent by mail
        Mail::send('forgetPassword', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });

        return back()->with('message', 'Hai ricevuto una mail con un link per resettare la password');
    }

    public function passwordReset(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);

        $updateRequest = PasswordResetRequest::where('token', $request->token)->first();

        if(!$updateRequest){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $admin = Admin::where('mail', $updateRequest->email)
                    ->update(['password_hash' => Hash::make($request->password)]);


        PasswordResetRequest::where('token',$request->token)->delete();
        return redirect()->route('login')->with('message', 'La tua password è stata modificata con successo');
    }
}
