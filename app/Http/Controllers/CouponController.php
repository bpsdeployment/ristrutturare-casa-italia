<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Coupon::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("deleteCoupon", $row) . '" class="delete btn btn-danger btn-sm">Cancella</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'coupon_name' => 'regex:/^[a-zA-Z0-9 ]*$/|unique:coupons,name|required',
            'due_date' => 'date_format:"m-d-Y"|required',
            'discount' => 'numeric|min:0|max:100|required',
            'free_shipping' => 'numeric|min:0|max:1|required',
            'coupon_code' => 'alpha_num|min:8|max:8|unique:coupons,code'
        ]);

        $converted_date = date('Y-m-d', strtotime($request->due_date)) . " 00:00:00";
        $coupon = new Coupon();
        $coupon->name = $request->coupon_name;
        $coupon->due_date = $converted_date;
        $coupon->discount_value = $request->discount;
        $coupon->free_shipping = $request->free_shipping;
        $coupon->n_uses = 0;
        $coupon->code = $request->coupon_code;
        $coupon->save();

        return redirect()->back()->with('message', 'Coupon creato con successo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        return back()->with('message', 'Coupon cancellata correttamente.');
    }
}
