<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Support\Facades\Redirect;


class AuthTPController extends Controller
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            //create a user using socialite driver google
            $user = Socialite::driver('google')->user();
            // if the user exits, use that user and login
            $finduser = User::where('google_id', $user->id)->first();
            if($finduser){
                //if the user exists, login and show dashboard
                Auth::login($finduser);
                return redirect()->route('user.page');
            }else{
                //user is not yet created, so create first
                $complete_name = $user->name;
                $name = explode(" ", $complete_name)[0];
                $surname = explode(" ", $complete_name)[1];
                $newUser = User::create([
                    'name' => $name,
                    'surname' => $surname,
                    'mail' => $user->email,
                    'google_id'=> $user->id,
                    'password_hash' => 'null',
                ]);
                $newUser->save();
                $newUser->markEmailAsVerified();
                event(new Verified($user));
                //login as the new user
                Auth::login($newUser);
                // go to the dashboard
                return redirect()->route('user.page');
            }
            //catch exceptions
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        try {

            $user = Socialite::driver('facebook')->user();
            $facebookId = User::where('facebook_id', $user->id)->first();

            if($facebookId){
                Auth::login($facebookId);
                return redirect()->route('user.page');
            }else{
                $complete_name = $user->name;
                $name = explode(" ", $complete_name)[0];
                $surname = explode(" ", $complete_name)[1];
                $newUser = User::create([
                    'name' => $name,
                    'surname' => $surname,
                    'mail' => $user->email,
                    'facebook_id'=> $user->id,
                    'password_hash' =>'null',
                ]);
                $newUser->save();
                $newUser->markEmailAsVerified();
                event(new Verified($user));
                //login as the new user
                Auth::login($newUser);
                // go to the dashboard
                return redirect()->route('user.page');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
