<?php

namespace App\Http\Controllers;

use App\Models\Authorization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class AuthorizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user_auth = Authorization::where('user_id', $id)->first();

        if (!$user_auth) {
            $new_user_auth = new Authorization();
            $new_user_auth->user_id = $id;
            $new_user_auth->save();
            $user_auth = Authorization::where('user_id', $id)->first();
        }

        if ($user_auth->edit_products) {
            $product_current_auth = 'true';
        } else {
            $product_current_auth = 'false';
        }

        if ($user_auth->edit_categories) {
            $categories_current_auth = 'true';
        } else {
            $categories_current_auth = 'false';
        }

        if ($user_auth->reply_contact_request) {
            $contact_current_auth = 'true';
        } else {
            $contact_current_auth = 'false';
        }

        if ($user_auth->edit_coupons) {
            $coupon_current_auth = 'true';
        } else {
            $coupon_current_auth = 'false';
        }

        if ($user_auth->edit_first_page_products) {
            $current_first_page = 'true';
        } else {
            $current_first_page = 'false';
        }

        if ($user_auth->edit_orders) {
            $current_order_auth = 'true';
        } else {
            $current_order_auth = 'false';
        }


        return view('editauthpage')
            ->with('id', $user_auth->id)
            ->with('product_current_auth', $product_current_auth)
            ->with('categories_current_auth', $categories_current_auth)
            ->with('contact_current_auth', $contact_current_auth)
            ->with('coupon_current_auth', $coupon_current_auth)
            ->with('current_first_page', $current_first_page)
            ->with('current_order_auth', $current_order_auth);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Authorization  $authorization
     * @return \Illuminate\Http\Response
     */
    public function show(Authorization $authorization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Authorization  $authorization
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request, [
            'product_auth' => 'in:true,false|required',
            'category_auth' => 'in:true,false|required',
            'contact_request_auth' => 'in:true,false|required',
            'edit_coupons_auth' => 'in:true,false|required',
            'first_page_auth' => 'in:true,false|required',
            'orders_auth' => 'in:true,false|required',
        ]);


        $auth_user = Authorization::findOrFail($request->id);
        if ($auth_user) {
            if ($request->product_auth == 'true') {
                $auth_user->edit_products = true;
            } else {
                $auth_user->edit_products = false;
            }

            if ($request->category_auth == 'true') {
                $auth_user->edit_categories = true;
            } else {
                $auth_user->edit_categories = false;
            }

            if ($request->contact_request_auth == 'true') {
                $auth_user->reply_contact_requests = true;
            } else {
                $auth_user->reply_contact_requests = false;
            }

            if ($request->edit_coupons_auth == 'true') {
                $auth_user->edit_coupons = true;
            } else {
                $auth_user->edit_coupons = false;
            }

            if ($request->first_page_auth == 'true') {
                $auth_user->edit_first_page_products = true;
            } else {
                $auth_user->edit_first_page_products = false;
            }

            if ($request->orders_auth == 'true') {
                $auth_user->edit_orders = true;
            } else {
                $auth_user->edit_orders = false;
            }


            $auth_user->update();
            return back()->with('message', 'Autorizzazioni aggiornate.');
        } else {
            return back()->with('errormessage', 'Impossibile aggiornare le autorizzazioni di questo utente.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Authorization  $authorization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Authorization $authorization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Authorization  $authorization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authorization $authorization)
    {
        //
    }

    public static function HasAnyAuth($user_id)
    {
        $user_auth = Authorization::where('user_id', $user_id)->first();
        //Check if there is an entry for this user in the auth db
        if (!$user_auth) {
            return false;
        }
        //If there is check if any auth is set to true
        else {
            return ($user_auth->edit_products || $user_auth->edit_categories || $user_auth->edit_coupons ||
                $user_auth->edit_orders || $user_auth->edit_first_page_products || $user_auth->reply_contact_requests);
        }
    }


}
