<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Carbon;
use Illuminate\Contracts\Support\Jsonable as JSON;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $products=CatalogController::Sorting($request);
            return CatalogController::ProductImpagination($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function Sorting(Request $request)
    {
        if ($request->has('category')) {
            $products= Product::where('type',$request->category);

        }else{
            $products = Product::where('id','!=', 0);
        }

        if($request->has('material'))
        {
            $products= Product::where('material_type_id',$request->material);
        }


        if($request->has('sort')){
            switch($request->sort){
                case $request->sort == 'popularity':
                    $products = $products->orderByDesc('units_sold');
                    break;
                case $request->sort == 'newest':
                    $products = $products->orderByDesc('created_at');
                    break;
                case $request->sort == 'discount':
                    $products = $products->orderByDesc('max_discount');
                    break;
                case $request->sort == 'low_high':
                    $products = $products->orderBy(DB::raw('listing_price'),'ASC' );
                    break;
                case $request->sort == 'high_low':
                    $products = $products->orderBy(DB::raw('listing_price'),'DESC' );
                    break;
            }
        }
        $search= request()->query('search');
        if($search){
            $products = $products->where('name','LIKE',"%{$search}%");

        }
        return $products->paginate(30);
    }
    public static function ProductImpagination($products)
    {
        $number_of_items_per_row = 3;
        $rows_number = intdiv(count($products), $number_of_items_per_row);
        $leftover_products = count($products) % $number_of_items_per_row;
        $categories = Category::get();
        $materials_type = DB::table('material_types')->get();

        return view('catologpage')
            ->with('products',$products)
            ->with('rows_number', $rows_number)
            ->with('leftover_products', $leftover_products)
            ->with('categories', $categories)
            ->with('materials_type', $materials_type);
    }
}
