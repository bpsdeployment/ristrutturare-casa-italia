<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])
                ->editColumn('buy_price', function($row){
                    return number_format($row->buy_price,2,",",".") . ' €';
                })
                ->editColumn('listing_price', function($row){
                    return number_format($row->listing_price,2,",",".") . ' €';
                })
                ->addColumn('action', function ($row) {
                    //Getting the admin for authorization purpose (CRUD)
                    $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();

                    $actionBtn = '<a href="' . route("showProductEditPage", $row) . '" class="edit btn btn-success btn-sm"';
                    //if the admin cannot modify the product, button will not be shown
                    if ($admin->u_products_auth == 0) {
                        $actionBtn .= ' style="background-color:#6AB190; pointer-events: none;"';
                    }
                    $actionBtn .= '>Modifica</a> <a href="' . route("deleteProduct", $row) . '" class="delete btn btn-danger btn-sm"';
                    //if the admin cannot delet the product, button will not be shown
                    if ($admin->d_products_auth == 0) {
                        $actionBtn .= ' style="background-color:#E87C87; pointer-events: none;"';
                    }
                    $actionBtn .= '>Cancella</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    public function selectProductPage()
    {
        $products = Product::all();
        return view('select-product')->with('products', $products);
    }

    public function showPriceChangeMultiplePage(){
        $products = Product::all();
        return view('change-price-multiple-products')->with('products', $products);
    }
    


    public function DisplayInsertProductPage()
    {
        ProductController::deleteTrashedProducts();
        $categories_list = Category::all();
        $measure_units = DB::table('measure_units')->get();
        $material_types = DB::table('material_types')->get();
        //It's not possible to add products if there aren't categories
        if (count($categories_list) == 0) {
            return redirect()->route('insert-category')->with('message', 'Prima di aggiungere un prodotto devi creare almeno una categoria.');
        }
        //Non è possibile aggiungere prodotti se non ci sono unità di misura
        if (count($measure_units) == 0) {
            return redirect()->route('measure-unit-view')->with('message', 'Prima di aggiungere un prodotto devi creare almeno un\' unità di misura.');
        }

        //Non è possibile aggiungere prodotti se non ci sono materiali
        if (count($material_types) == 0) {
            return redirect()->route('material-types-view')->with('message', 'Prima di aggiungere un prodotto devi creare almeno un tipo di materiale.');
        }

        $selectedCategories = array();
        foreach ($categories_list as $category) {
            $selectedCategories[$category->name] = $category->name;
        }

        $measure_unit_list = array();
        foreach ($measure_units as $measure_unit) {
            $measure_unit_list[$measure_unit->id] = $measure_unit->name;
        }

        $material_types_list = array();
        foreach ($material_types as $material_type) {
            $material_types_list[$material_type->id] = $material_type->name;
        }

        return view('insertproduct')->with('categories_list', $selectedCategories)->with('measure_unit_list', $measure_unit_list)->with('material_types_list', $material_types_list);
    }

    public function StoreService(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required|string|regex:/^[a-zA-Z0-9 ]*$/|unique:products,name',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $product = new Product();
        $product->name = $request->product_name;
        $product->type = $request->product_type;
        $product->description = $request->product_description;
        $product->dimensions_x = null;
        $product->dimensions_y = null;
        $product->dimensions_z = null;
        $product->weight = null;
        $product->need_installation = false;
        $product->buy_price = $request->product_buy_price;
        $product->listing_price = $request->product_listing_price;
        $product->max_discount = $request->max_discount;
        $product->measure_unit_id = null;
        $product->material_type_id = null;
        $product->additional_infos = null;

        //Directory creation
        //The product name, trimmed of every space, is used as folder name
        $folder_name = str_replace(' ', '', $request->product_name);
        $path = public_path() . '/images/products_images/' . $folder_name;
        File::makeDirectory($path, $mode = 0777, true, true);

        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $filename = str_replace(' ', '', $image->getClientOriginalName());
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(400, 400)->save($path . "/" . $filename);
            }
        }

        $product->photos_folder = $folder_name;
        $product->save();

        $category = Category::where('name', $request->product_type)->first();
        $category->product_count = $category->product_count + 1;
        $category->update();
        return redirect()->route('insert-service')->with('message', 'Servizio inserito correttamente.');
    }

    public function ShowMeasureUnitsPage()
    {
        return view('measure-unit');
    }

    public function FillMeasureUnitTable(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('measure_units')->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->rawColumns(['name'])
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                ->escapeColumns('name')
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("delete-measure-unit", $row->id) . '" class="delete btn btn-danger btn-sm">Elimina</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function StoreMeasureUnit(Request $request)
    {
        DB::table('measure_units')->insert(
            ['name' => $request->measure_unit_name, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );
        return back()->with('message', 'Unità di misura aggiunta.');
    }

    public function DeleteMeasureUnit(Request $request)
    {
        //Check if the measure unit is used
        $products = Product::where('measure_unit_id', $request->id)->get();
        if (count($products) != 0) {
            return back()->with('error_message', 'L\' unità di misura è usata in ' . count($products) . ' prodotto/i, prima di cancellarla è necessario modificarli.');
        } else {
            DB::table('measure_units')->where('id', $request->id)->delete();
            return back()->with('message', 'Unità di misura eliminata.');
        }
    }

    public function ShowMaterialTypesPage(Request $request)
    {
        return view('material-types-view');
    }

    public function FillMaterialTypesTable(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('material_types')->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->rawColumns(['name'])
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                ->escapeColumns('name')
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("delete-material-type", $row->id) . '" class="delete btn btn-danger btn-sm">Elimina</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    public function StoreMaterialTypes(Request $request)
    {
        DB::table('material_types')->insert(
            ['name' => $request->material_type_name, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );
        return back()->with('message', 'Tipo di materiale aggiunto.');
    }

    public function DeleteMaterialType(Request $request)
    {
        //Check if the material is used
        $products = Product::where('material_type_id', $request->id)->get();
        if (count($products) != 0) {
            return back()->with('error_message', 'Il materiale è usato in ' . count($products) . ' prodotto/i, prima di cancellarlo è necessario modificarli.');
        } else {
            DB::table('material_types')->where('id', $request->id)->delete();
            return back()->with('message', 'Materiale eliminato.');
        }
    }

    public function DisplayInsertServicePage()
    {
        ProductController::deleteTrashedProducts();
        $categories_list = Category::all();
        //It's not possible to add products if there aren't categories
        if (count($categories_list) == 0) {
            return redirect()->route('insert-category')->with('message', 'Prima di aggiungere un prodotto devi creare almeno una categoria.');
        }

        $selectedCategories = array();
        foreach ($categories_list as $category) {
            $selectedCategories[$category->name] = $category->name;
        }

        return view('insert-service')->with('categories_list', $selectedCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'product_name' => 'required|string|regex:/^[a-zA-Z0-9 ]*$/|unique:products,name',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $product = new Product();
        $product->name = $request->product_name;
        $product->type = $request->product_type;
        $product->description = $request->product_description;
        $product->dimensions_x = $request->product_dimensions_x;
        $product->dimensions_y = $request->product_dimensions_y;
        $product->dimensions_z = $request->product_dimensions_z;
        $product->weight = $request->product_weight;
        $product->need_installation = $request->need_installation;
        $product->buy_price = $request->product_buy_price;
        $product->listing_price = $request->product_listing_price;
        $product->max_discount = $request->max_discount;;
        $product->measure_unit_id = $request->measure_unit_id;
        $product->material_type_id = $request->material_type_id;
        $product->additional_infos = $request->product_additional_info;

        //Directory creation
        //The product name, trimmed of every space, is used as folder name
        $folder_name = str_replace(' ', '', $request->product_name);
        $path = public_path() . '/images/products_images/' . $folder_name;
        File::makeDirectory($path, $mode = 0777, true, true);

        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $filename = str_replace(' ', '', $image->getClientOriginalName());
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(400, 400)->save($path . "/" . $filename);
            }
        }

        //Check if there are less than 8 front-page items
        $first_page_items = Product::where('is_first_page', true)->get();
        if (count($first_page_items) < 8) {
            $product->is_first_page = true;
        }

        $product->photos_folder = $folder_name;
        $product->save();

        $category = Category::where('name', $request->product_type)->first();
        $category->product_count = $category->product_count + 1;
        $category->update();
        return redirect()->route('insert-product')->with('message', 'Prodotto inserito correttamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        $product = Product::findOrFail($product_id);
        if (is_dir("./images/products_images/" . $product->photos_folder)) {
            $files = array_diff(scandir("./images/products_images/" . $product->photos_folder), array('.', '..'));
        } else {
            $files = [];
        }

        //Get related products
        $related_products = ProductController::GetRelatedProducts($product);
        $measure_unit = ProductController::GetMeasureUnitName($product->measure_unit_id);
        $material_type_query = DB::table('material_types')->where('id', '=', $product->material_type_id)->first();
        $material_type = ($material_type_query) ? $material_type_query->name : "N/A";
        return view('product-detail-1')
            ->with('product_name', $product->name)
            ->with('product_description', $product->description)
            ->with('product_type', $product->type)
            ->with('photo_folder', $files)
            ->with('product_price', $product->listing_price)
            ->with('product_discount', $product->discount)
            ->with('folder_name', $product->photos_folder)
            ->with('need_installation', $product->need_installation)
            ->with('weight', $product->weight)
            ->with('dimensions_x', $product->dimensions_x)
            ->with('dimensions_y', $product->dimensions_y)
            ->with('dimensions_z', $product->dimensions_z)
            ->with('additional_infos', $product->additional_infos)
            ->with('product_id', $product_id)
            ->with('related_products', $related_products)
            ->with('measure_unit', $measure_unit)
            ->with('material_type', $material_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
        if ($admin and ($admin->u_products_auth == 0)) {
            return redirect()->route('products-table')->with('message', 'Non sei autorizzato a modificare i prodotti.');
        }

        $this->validate($request, [
            'product_name' => 'regex:/^[a-zA-Z0-9 ]*$/|unique:products,name,' . $request->id,
        ]);

        $product = Product::find($request->id);

        //Remove spaces from the product name to get the folder name
        $folder_name = $product->photos_folder;
        $new_folder_name = str_replace(' ', '', $request->product_name);

        //Change folder name if the product name has changed
        if ($folder_name != $new_folder_name) {
            $current_path = public_path() . '\images\products_images\\' . $folder_name;
            $target_path = public_path() . '\images\products_images\\' . $new_folder_name;
            rename($current_path, $target_path);
        }

        //Save new photos if any
        $path = public_path() . '/images/products_images/' . $new_folder_name;
        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $filename = $image->getClientOriginalName();
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(400, 400)->save($path . "/" . $filename);
            }
        }

        //Update product count in categories
        $new_category = Category::where('name', $request->product_type)->first();
        $new_category->product_count = $new_category->product_count + 1;
        $new_category->update();
        $old_category = Category::where('name', $product->type)->first();
        $old_category->product_count = $old_category->product_count - 1;
        $old_category->update();

        $product->update(
            [
                'name' => $request->product_name,
                'type' => $request->product_type,
                'description' => $request->product_description,
                'photos_folder' => $new_folder_name,
                'need_installation' => $request->need_installation,
                'dimensions_x' => $request->product_dimensions_x,
                'dimensions_y' => $request->product_dimensions_y,
                'dimensions_z' => $request->product_dimensions_z,
                'weight' => $request->product_weight,
                'buy_price' => $request->product_buy_price,
                'listing_price' => $request->product_listing_price,
                'max_discount' => $request->max_discount,
                'additional_infos' => $request->product_additional_info,
                'measure_unit_id' => $request->measure_unit,
                'material_type_id' => $request->material_type
            ]
        );
        return redirect()->route('products-table')->with('message', 'Prodotto aggiornato correttamente.');
    }

    public function bulkProductListingPriceEdit(Request $request)
    {
        $targetProducts = Product::find($request->products_id);
        foreach($targetProducts as $product)
        {
            $listing_price = $product->listing_price;
            $newListingPrice = $listing_price + (($listing_price / 100) * $request->multiplier);
            $product->listing_price = $newListingPrice;
            $product->save();
        }
        //return back()->with('message','Operation Successful !');
        //return redirect()->route('products-table')->with('message', 'Prodotto aggiornato correttamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = ('App\Models\Admin')::where('id', Session::get('admin_id'))->first();
        if ($admin and ($admin->d_products_auth == 0)) {
            return redirect()->route('products-table')->with('message', 'Non sei autorizzato a cancellare i prodotti.');
        }
        //Update product count
        $product_category = Product::find($id)->type;
        $category = Category::where('name', $product_category)->first();
        $category->product_count = $category->product_count - 1;
        $category->update();

        //Delete db entry
        Product::find($id)->delete();
        return redirect()->route('products-table')->with('message', 'Prodotto eliminato con successo.')->with('id', $id);
    }

    public function fillTable(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("showProductEditPage", $row) . '" class="edit btn btn-success btn-sm">Modifica</a> <a href="' . route("deleteProduct", $row) . '" class="delete btn btn-danger btn-sm">Elimina</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function showEditProductPage($id)
    {
        ProductController::deleteTrashedProducts();
        $product = Product::findOrFail($id);
        $files = [];
        if (is_dir("./images/products_images/" . $product->photos_folder)) {
            $files = array_diff(scandir("./images/products_images/" . $product->photos_folder), array('.', '..'));
        }

        $categories_list = Category::all();
        $selectedCategories = array();
        foreach ($categories_list as $category) {
            $selectedCategories[$category->name] = $category->name;
        }

        if ($product->weight != NULL) {
            if (!$product->measure_unit_id) {
                $product->measure_unit_id = DB::table('measure_units')->first()->id;
                $product->save();
            }

            if (!$product->material_type_id) {
                $product->material_type_id = DB::table('material_types')->first()->id;
                $product->save();
            }

            $measure_units = DB::table('measure_units')->get();
            $measure_unit_list = array();
            foreach ($measure_units as $measure_unit) {
                $measure_unit_list[$measure_unit->id] = $measure_unit->name;
            }
            $measure_unit =  DB::table('measure_units')->where('id', '=', $product->measure_unit_id)->get();

            $material_types = DB::table('material_types')->get();
            $material_types_list = array();
            foreach ($material_types as $material_type) {
                $material_types_list[$material_type->id] = $material_type->name;
            }
            $material_type =  DB::table('material_types')->where('id', '=', $product->material_type_id)->get();

            return view('editproduct')
                ->with('id', $id)
                ->with('product_name', $product->name)
                ->with('product_description', $product->description)
                ->with('product_type', $product->type)
                ->with('categories_list', $selectedCategories)
                ->with('need_installation', $product->need_installation)
                ->with('dimensions_x',  $product->dimensions_x)
                ->with('dimensions_y', $product->dimensions_y)
                ->with('dimensions_z', $product->dimensions_z)
                ->with('weight', $product->weight)
                ->with('buy_price', $product->buy_price)
                ->with('listing_price', $product->listing_price)
                ->with('discount', $product->max_discount)
                ->with('additional_infos', $product->additional_infos)
                ->with('images', $files)
                ->with('folder_name', $product->photos_folder)
                ->with('measure_unit_list', $measure_unit_list)
                ->with('measure_unit', $measure_unit[0]->id)
                ->with('material_types_list', $material_types_list)
                ->with('material_type', $material_type[0]->id);
        } else {
            return view('edit-service')
                ->with('id', $id)
                ->with('product_name', $product->name)
                ->with('product_description', $product->description)
                ->with('product_type', $product->type)
                ->with('categories_list', $selectedCategories)
                ->with('buy_price', $product->buy_price)
                ->with('listing_price', $product->listing_price)
                ->with('discount', $product->max_discount)
                ->with('additional_infos', $product->additional_infos)
                ->with('images', $files)
                ->with('folder_name', $product->photos_folder);
        }
    }

    public function EditService(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'regex:/^[a-zA-Z0-9 ]*$/|unique:products,name,' . $request->id,
        ]);

        $product = Product::find($request->id);

        //Remove spaces from the product name to get the folder name
        $folder_name = $product->photos_folder;
        $new_folder_name = str_replace(' ', '', $request->product_name);

        //Change folder name if the product name has changed
        if ($folder_name != $new_folder_name) {
            $current_path = public_path() . '\images\products_images\\' . $folder_name;
            $target_path = public_path() . '\images\products_images\\' . $new_folder_name;
            rename($current_path, $target_path);
        }

        //Save new photos if any
        $path = public_path() . '/images/products_images/' . $new_folder_name;
        if ($request->hasfile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $filename = $image->getClientOriginalName();
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(400, 400)->save($path . "/" . $filename);
            }
        }

        //Update product count in categories
        $new_category = Category::where('name', $request->product_type)->first();
        $new_category->product_count = $new_category->product_count + 1;
        $new_category->update();
        $old_category = Category::where('name', $product->type)->first();
        $old_category->product_count = $old_category->product_count - 1;
        $old_category->update();

        $product->update(
            [
                'name' => $request->product_name,
                'type' => $request->product_type,
                'description' => $request->product_description,
                'photos_folder' => $new_folder_name,
                'buy_price' => $request->product_buy_price,
                'listing_price' => $request->product_listing_price,
                'max_discount' => $request->max_discount,
                'additional_infos' => $request->product_additional_info,
            ]
        );
        return redirect()->route('products-table')->with('message', 'Prodotto aggiornato correttamente.');
    }

    public function deleteProductImage($folder_name, $filename)
    {
        $file_path = public_path() . '/images/products_images/' . $folder_name . '/' . $filename;
        unlink($file_path);
        return back();
    }

    public static function deleteTrashedProducts()
    {
        $products =  Product::onlyTrashed()->get();
        if (count($products) > 0) {
            foreach ($products as $product) {
                //Delete entry from DB
                $product->forceDelete();
                //Delete folder
                $file_path = public_path() . '\images\products_images\\' . $product->photos_folder;
                ProductController::deleteDir($file_path);
            }
        }
    }

    public function undoProductDelete($id)
    {
        Product::withTrashed()->find($id)->restore();
        return back()->with('message', 'Il prodotto è stato ripristinato.');
    }

    public static function deleteDir($dirPath)
    {

        if (is_dir($dirPath)) {
            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }
            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::deleteDir($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($dirPath);
        }
    }

    public static function GetRelatedProducts(Product $product)
    {
        $category = $product->type;
        $all_other_products = Product::where('type', $category)->get()->pluck('name')->toArray();
        $userInput = $product->name;
        usort($all_other_products, function ($a, $b) use ($userInput) {
            $levA = levenshtein($userInput, $a);
            $levB = levenshtein($userInput, $b);
            return $levA === $levB ? 0 : ($levA > $levB ? 1 : -1);
        });

        $related_products = array();
        $number_of_related_products = 4;
        $related_products_count = count($all_other_products);
        if ($related_products_count < 5) {
            $number_of_related_products = $related_products_count - 1;
        }

        //Skip the first result because is the starting product
        for ($i = 1; $i < $number_of_related_products + 1; $i++) {
            array_push($related_products, Product::where('name', $all_other_products[$i])->first());
        }
        return $related_products;
    }

    //Return the first image of $folder_name folder inside products_images if any,
    //otherwise return image-not-found.png
    public static function GetThumbnailImage($folder_name)
    {
        $images = array_values(array_diff(scandir('./images/products_images/' . $folder_name), ['.', '..']));
        //Folder empty
        if (empty($images)) {
            $thumbnail = '../../image-not-found.jpg';
        }
        //If folder is not empty pick the first image as thumbnail
        else {
            $thumbnail = $images[0];
        }
        return $thumbnail;
    }

    public static function GetMeasureUnitName($measure_unit_id)
    {
        $measure_unit = DB::table('measure_units')->where('id', '=', $measure_unit_id)->get();
        if (count($measure_unit) > 0) {
            return $measure_unit[0]->name;
        } else {
            return "€";
        }
    }
}
