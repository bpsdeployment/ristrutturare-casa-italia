<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\PseudoTypes\True_;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::find($request->id);
        $product_price = $product->listing_price;
        $cart_item = Cart::content()->where('id', $product->id);

        if (count($cart_item) > 0) {
            $cart_item = Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->qty, 'price' => $product_price])->associate('Product');
            return response()->json(
                [
                    'id' => $cart_item->id,
                    'qty' => $cart_item->qty,
                    'new_item'=> false,
                    'cart_count' => Cart::count()
                ]
            );
        } else {
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->qty, 'price' => $product_price])->associate('Product');
            return response()->json(
                [
                    'html' => view('sidebar-cart-content', ['id' => $product->id, 'qty' => $request->qty, 'name' => $product->name, 'price' => $product_price])->render(),
                    'new_item'=> true,
                    'cart_count' => Cart::count()
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            Cart::destroy();
            return response()->json([
                'success' => 'Ok',
            ], 200);
        }
    }

    public function deleteItem(Request $request)
    {
        if ($request->ajax()) {
            $rows  = Cart::content();
            $rowId = $rows->where('id', $request->id)->first()->rowId;
            Cart::remove($rowId);
            return response()->json([
                'success' => 'Ok',
                'cart_count' => Cart::count(),
                'id' => $request->id,
                'new_total' => Cart::total(),
                'new_tax' => Cart::tax(),
                'new_subtotal' => Cart::subtotal()
            ], 200);
        }
    }
    public function UpdateQuantity(Request $request)
    {
        if ($request->ajax()){

            $rows  = Cart::content();
            $rowId =  $rows->where('id', $request->id)->first()->rowId;
            $cart_item = Cart::get($rowId);
            $update_qty = $request->quantity;
            Cart::update($rowId, $update_qty);
            return response()->json([
                'success' => 'Ok',
                'new_total' => Cart::total(),
                'new_tax' => Cart::tax(),
                'new_subtotal' => Cart::subtotal()
            ], 200);
        }

    }


}
