<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FirstPageController extends Controller
{


    public function ShowHomepage()
    {
        $frontpage_texts = DB::table('frontpage_infos')->where('id', 1)->first();
        return view('index-landing')->with('frontpage_texts', $frontpage_texts);
    }

    public function ShowLogin()
    {
        $frontpage_texts = DB::table('frontpage_infos')->where('id', 1)->first();
        return view('login')->with('frontpage_texts', $frontpage_texts);
    }

    public function index()
    {
        $show_categories = true;
        $show_products = true;
        $categories_list = Category::all();
        if (count($categories_list) < 6) {
            $show_categories = false;
        }
        $products_list = Product::all();
        if (count($products_list) < 8) {
            $show_products = false;
        }

        $first_page_categories = Category::where('is_first_page', true)->get();

        $selectedCategories = array();
        foreach ($categories_list as $category) {
            if (!$category->is_first_page) {
                $selectedCategories[$category->id] = $category->name;
            }
        }
        $first_page_products = Product::where('is_first_page', true)->get();
        $product_select = array();
        foreach ($products_list as $product) {
            if (!$product->is_first_page) {
                $product_select[$product->id] = $product->name;
            }
        }
        $homepage_info = DB::table('frontpage_infos')->where('id', 1)->first();

        return view(
            'editfirstpage',
            [
                'first_page_categories' => $first_page_categories,
                'categories' => $selectedCategories,
                'first_page_products' => $first_page_products,
                'product_select' => $product_select,
                'show_categories' => $show_categories,
                'show_products' => $show_products,
                'homepage_info' => $homepage_info
            ]
        );
    }

    public function swapCategories(Request $request)
    {
        $old_cat_id = $request->old_cat_id;
        $new_cat_id = $request->new_cat_id;
        $old_cat = Category::find($old_cat_id);
        $new_cat = Category::find($new_cat_id);
        $old_cat->is_first_page = false;
        $old_cat->update();
        $new_cat->is_first_page = true;
        $new_cat->update();
        return back()->with('message', 'Categoria in primo piano aggiornata correttamente.');
    }

    public function swapProducts(Request $request)
    {
        $old_product_id = $request->old_product_id;
        $new_product_id = $request->new_product_id;
        $old_product = Product::find($old_product_id);
        $new_product = Product::find($new_product_id);
        $old_product->is_first_page = false;
        $old_product->update();
        $new_product->is_first_page = true;
        $new_product->update();
        return back()->with('message', 'Prodotto in primo piano aggiornato correttamente.');
    }

    public function UpdateFirstPageText(Request $request)
    {
        DB::table('frontpage_infos')->where('id', 1)->update(array(
            'title_homepage'  => $request->title_homepage,
            'subtitle_homepage' => $request->subtitle_homepage,
            'homepage_text' => $request->homepage_text,
            'footer_text' => $request->footer_text,
            'support_mail' => $request->support_mail,
            'support_phone' => $request->support_phone,
            'privacy_info_link' => $request->privacy_info_link,
            't_t_link' => $request->t_t_link,
            'cookie_policy_link' => $request->cookie_policy_link
        ));

        return back()->with('message', 'Testi homepage aggiornati.');
    }
}
