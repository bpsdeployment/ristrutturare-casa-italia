<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Message::latest()->get();
            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])->addColumn('message_state', function ($row) {
                    switch ($row->message_state) {
                        case 2:
                            return '<label class="badge badge-gradient-success">RISPOSTO</label>';
                        case 1:
                            return '<label class="badge badge-gradient-warning">VISUALIZZATO</label>';
                        case 0:
                            return '<label class="badge badge-gradient-info">NUOVO</label>';
                    }
                })
                ->escapeColumns('message_state')
                ->rawColumns(['message_state'])
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("showMessagePage", $row) . '" class="edit btn btn-success btn-sm">Dettagli</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|regex:/^[a-zA-Z0-9 ]*$/',
            'mail' => 'required|string|email:rfc,dns',
            'message' => 'required|string',
            'subject' => 'required|string',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        $message = new Message();
        $message->name = $request->name;
        $message->email = $request->mail;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->last_reply = null;

        $message->save();

        return back()->with('message', 'Il messaggio è stato inviato. Verrai ricontattato a breve dal nostro supporto.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

    public function showMessagePage($id)
    {
        $message = Message::findOrFail($id);
        $name = $message->name;
        $email = $message->email;
        $subject = $message->subject;
        $text = $message->message;
        $last_reply = $message->last_reply;
        //If the message is "new" it becomes "displayed"
        if ($message->message_state == 0) {
            $message->message_state = 1;
            $message->update();
        }

        return view('replymessage', ['text' => $text, 'subject' => $subject, 'name' => $name, 'email' => $email, 'id' => $id, 'last_reply' => $last_reply]);
    }

    public function replyToMessage(Request $request)
    {
        $id = $request->id;
        $message = Message::findOrFail($id);
        $reply = $request->reply_text;
        if ($message) {
            $data = ['message' => $reply, 'subject' => $message->subject, 'name' => $message->name];
            Mail::to($message->email)->send(new ContactEmail($data));
            $message->message_state = 2;
            $message->last_reply = $reply;
            $message->update();
            return redirect()->route('messages-table')->with('message', 'La mail di risposta è stata inviata');
        } else {
            return back()->with('error_message', 'Impossibile inviare la mail di risposta, utente non trovato.');
        }
    }
}
