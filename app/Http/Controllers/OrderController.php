<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $affiliateCustomersId = User::select('id')->where('reference_admin_id', Session::get('admin_id'))->get()->toArray();
            $data = Order::latest()->whereIn('customer_id', $affiliateCustomersId)->get();
            if (Session::get('is_site_master')) {
                $data = Order::latest()->get();
            }

            return DataTables::of($data)
                ->addColumn('DT_RowIndex', function ($row) {
                    return $row->id;
                })
                ->rawColumns(['DT_RowIndex'])
                ->addColumn('full_name', function ($row) {
                    $user_id = Order::where('id', $row->id)->first()->customer_id;
                    $user = User::find($user_id);
                    return $user->name . " " . $user->surname;
                })
                ->rawColumns(['full_name'])
                ->addColumn('admin_name', function ($row) {
                    $admin_id = Order::where('id', $row->id)->first()->creator_id;
                    $admin = Admin::find($admin_id);
                    return $admin->name . " " . $admin->surname;
                })
                ->rawColumns(['admin_name'])
                ->addColumn('date', function ($row) {
                    $date = Order::where('id', $row->id)->first()->created_at;
                    return date('d-m-Y H:i', strtotime($date));
                })
                ->editColumn('total', function ($row) {
                    return number_format($row->total, 2, ",", ".") . ' €';
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="' . route("manage.order.admin", $row) . '" class="edit btn btn-info btn-sm">Dettagli</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function showDetailPageAdmin($id)
    {
        Log::debug('Showing details page admin');

        $order = Order::find($id);
        $admin_commission_perc = Admin::find($order->creator_id)->selling_commission_perc;
        $user = User::find($order->customer_id);
        $items = json_decode($order->item_list, true);
        $acc_date = date_create($order->acceptance_date);


        $orderInfo = OrderController::GetOrderInfo($order, $admin_commission_perc);
        $perc_margin = $orderInfo['perc_margin'];
        $perc_expenses = $orderInfo['perc_expenses'];
        $perc_discount = $orderInfo['perc_discount'];
        $perc_taxes = $orderInfo['perc_taxes'];
        $perc_commission = $orderInfo['perc_commission'];

        return view(
            'manage-order',
            [
                'id' => $id,
                'user_id' => $order->customer_id,
                'order_memo' => $order->reference_text,
                'order_date' => $order->created_at,
                'taxable' => $order->taxable,
                'taxes' => $order->taxes,
                'total' => $order->total,
                'discount' => $order->discount,
                'name' => $user->name,
                'mail' => $user->mail,
                'surname' => $user->surname,
                'items' => $items,
                'accepted_by_customer' => $order->accepted_by_customer,
                'acceptance_date' => date_format($acc_date, "d/m/Y H:i:s"),
                'perc_margin' => number_format($perc_margin, 2, ".", ""),
                'perc_expenses' => number_format($perc_expenses, 2, ".", ""),
                'perc_discount' => number_format($perc_discount, 2, ".", ""),
                'perc_taxes' => number_format($perc_taxes, 2, '.', ''),
                'abs_commission' => round($orderInfo['abs_commission'], 2),
                'perc_commission' => number_format($perc_commission, 2, '.', '')
            ]
        );
    }

    public function SetOrderAccepted(Request $request)
    {
        $order = Order::find($request->id);
        $order->accepted_by_customer = 1;
        $order->acceptance_date = date('Y-m-d H:i:s');
        $order->save();
        //Update item sell count
        $item_list = json_decode($order->item_list, true);
        foreach ($item_list as $item) {
            if ($item['id'] == -1 || $item['id'] == -2) continue;
            $sold_product = Product::find($item['id']);
            $sold_product->units_sold += $item['qty'];
            $sold_product->save();
        }
        //Update user total money spent
        $customer = User::find($order->customer_id);
        $customer->total_spent += $order->total;
        $customer->save();
        return back()->with('message', 'Il preventivo è stato contrassegnato come accettato dal cliente.');
    }

    public function DownloadPreventive(Request $request)
    {
        $preventive = Order::find($request->id);
        $pdf = Pdf::loadView('preventive', array('preventive' => $preventive));
        return $pdf->download($preventive->reference_text . '.pdf');
    }

    public function PrintPreventive(Request $request)
    {
        $preventive = Order::find($request->id);
        $pdf = Pdf::loadView('preventive', array('preventive' => $preventive));

        $output = $pdf->output();
        return new Response($output, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="preventivo.pdf"',
        ]);
    }

    public function ShowInsertPreventivePage()
    {
        $admin_list = [];
        foreach (Admin::all() as $admin)
            $admin_list[$admin->id] = $admin->name . ' ' . $admin->surname;
        Log::debug($admin_list);

        return view('create-preventive', ['nextOrderId' => OrderController::RetrieveNextOrderId(), 'items' => []])->with('admin_list', $admin_list);
    }

    public function ShowEditPreventivePage(Request $request)
    {
        $preventive = Order::find($request->id);
        Log::debug('Editing order: ' . $preventive);
        return view('edit-preventive')
            ->with('id', $request->id)
            ->with('reference_text', $preventive->reference_text)
            ->with('order_id', $preventive->preventive_number)
            ->with('customer_id', $preventive->customer_id)
            ->with('customer_name', User::find($preventive->customer_id)->name . " " . User::find($preventive->customer_id)->surname)
            ->with('items', json_decode($preventive->item_list, true))
            ->with('taxable', $preventive->taxable)
            ->with('taxes', $preventive->taxes)
            ->with('total', $preventive->total)
            ->with('creation_date', $preventive->creation_date)
            ->with('validity_end_date', $preventive->validity_end_date);
    }

    public function UpdatePreventive(Request $request)
    {
        $preventive = Order::find($request->id);
        Log::debug('Updating order #' . $request->id);
        foreach ($request->product as $product) {
            foreach ($product as $key => $item) {

                Log::debug($key . ' = ' . $product[$key]);
                //UM
                $item = $product[$key];
                if (!is_numeric($item)) {
                    //Comma to dot?
                    if (preg_match("/^[a-zA-Z]+$/", $item)) {
                        Log::debug('item contains letters: (' . $item . ')');
                        continue;
                    }
                    if (strpos($item, ',') != false) {
                        $item = str_replace(',', '.', $item);
                        Log::debug('Changed number from ' . $product[$key] . 'to ' . $item . ')');
                        $product[$key] = $item;
                    }
                }
            }
        }
        OrderController::UpdatePreventiveFields($preventive, $request);
        return redirect()->route('manage.order.admin', $request->id)->with('message', 'Il preventivo è stato modificato.');
    }

    public function SelectLiveSearch(Request $request)
    {
        $users = [];

        if ($request->has('q')) {
            $search = $request->q;
            if (Session::get('is_site_master')) {
                $users = DB::table('users')->select("id", "name", "surname")
                    ->where(DB::raw('CONCAT_WS(" ", name, surname)'), 'LIKE', '%' . $search . '%')->get();
            } else {
                $users = DB::table('users')->select("id", "name", "surname")
                    ->where(DB::raw('CONCAT_WS(" ", name, surname)'), 'LIKE', '%' . $search . '%')
                    ->where('reference_admin_id', Session::get('admin_id'))->get();
            }
        }
        return response()->json($users);
    }

    public function SelectProductNameLiveSearch(Request $request)
    {
        $products = [];

        if ($request->has('q')) {
            $search = $request->q;
            Log::debug('Searching product with description: ' . $search);
            $products = DB::table('products')->select("id", "name", "description", "listing_price", "measure_unit_id", "max_discount")->where('description', 'LIKE', '%' . $search . '%')->orWhere('name', 'LIKE', '%' . $search . '%')->get();
            //$products = DB::table('products')->select("id", "name", "description", "listing_price", "measure_unit_id", "max_discount")->where('name', 'LIKE', '%' . $search . '%')->get();
        }
        return response()->json($products);
    }

    public function StorePreventive(Request $request)
    {
        Log::debug('Storing preventive.');
        $newOrder = new Order();
        OrderController::UpdatePreventiveFields($newOrder, $request);
        return redirect()->route('manage.order.admin', ['id' => $newOrder->id]);
    }

    public static function UpdatePreventiveFields(Order $order, Request $request)
    {
        $order->creator_id = Session::get('admin_id');
        $order->customer_id = $request->customer_id;
        $order->reference_text = $request->reference_text;
        $order->preventive_number = $request->preventive_number;
        $order->creation_date = $request->creation_date;
        $order->validity_end_date = $request->end_date;
        $order->item_list = json_encode($request->product);

        $full_taxable = 0;
        $taxes = 0;
        $discount = 0;
        $total = 0;

        Log::debug('Order #: ' . $order->preventive_number);
        foreach ($request->product as $item) {

            $current_taxable = 0;
            $current_discount = 0;
            $current_taxes = 0;

            if ($item['id'] == -2) {
                Log::debug('Empty row - skipping.');
                continue;
            }

            Log::debug('Calculating taxes on product ' . $item['id']);

            //Check if UM, qty, discount and IVA are numeric fields or not

            //UM
            $itemUM = $item['UM'];
            if (!is_numeric($itemUM)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemUM)) {
                    Log::debug('UM contains letters: skipping row. (' . $itemUM . ')');
                    continue;
                }
                if (strpos($itemUM, ',') != false) {
                    $itemUM = str_replace(',', '.', $itemUM);
                    Log::debug('Changed number from ' . $item['UM'] . 'to ' . $itemUM . ')');
                }
            }


            //qty
            $itemQty = $item['qty'];
            if (!is_numeric($itemQty)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemQty)) {
                    Log::debug('Qty contains letters: skipping row. (' . $itemQty . ')');
                    continue;
                }
                if (strpos($itemQty, ',') != false) {
                    $itemQty = str_replace(',', '.', $itemQty);
                    Log::debug('Changed number from ' . $item['qty'] . 'to ' . $itemQty . ')');
                }
            }


            //discount
            $itemDiscount = $item['discount'];
            if (!is_numeric($itemDiscount)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemDiscount)) {
                    Log::debug('Discount contains letters: skipping row. (' . $itemDiscount . ')');
                    continue;
                }
                if (strpos($itemDiscount, ',') != false) {
                    $itemDiscount = str_replace(',', '.', $itemDiscount);
                    Log::debug('Changed number from ' . $item['discount'] . 'to ' . $itemDiscount . ')');
                }
            }


            //IVA
            $itemIVA = $item['IVA'];
            if (!is_numeric($itemIVA)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemIVA)) {
                    Log::debug('IVA contains letters: skipping row. (' . $itemIVA . ')');
                    continue;
                }
                if (strpos($itemIVA, ',') != false) {
                    $itemIVA = str_replace(',', '.', $itemIVA);
                    Log::debug('Changed number from ' . $item['IVA'] . 'to ' . $itemIVA . ')');
                }
            }

            $current_taxable = $itemUM * $itemQty;
            $full_taxable += $current_taxable;
            $current_discount = ($current_taxable / 100) * $itemDiscount;
            $discount += $current_discount;
            $current_taxes = ($current_taxable - $current_discount) / 100 * $itemIVA;
            $taxes += $current_taxes;
            $total += $current_taxable - $current_discount + $current_taxes;
            Log::debug('Current taxable: ' . $current_taxable);
            Log::debug('Current discount: ' . $current_discount);
            Log::debug('Current taxes: ' . $current_taxes);
        }

        $order->taxable = $full_taxable - $discount;
        $order->taxes = $taxes;
        $order->discount = $discount;
        $order->total = $total;
        Log::debug('Total: ' . $total);
        Log::debug('Discount: ' . $discount);
        Log::debug('Taxes: ' . $taxes);

        $order->save();
    }

    public function CommasToDotNumber($number)
    {
        if (!preg_match("/[a-z]/i", $number)) {
            return (float)strtr($number, ',', '.');
        } else {
            return $number;
        }
    }

    public function FillPreventiveFromCart(Request $request)
    {
        $preventive_items = array();
        $counter = 0;
        foreach ($request->items as $item) {
            $product = Product::find($item['id']);
            $preventive_items[$counter]['id'] = $item['id'];
            $preventive_items[$counter]['name'] = $item['name'];
            $preventive_items[$counter]['qty'] = $item['qty'];
            $preventive_items[$counter]['description'] = $product->description;
            $preventive_items[$counter]['UM'] = $product->listing_price;
            $preventive_items[$counter]['discount'] = 0;
            $preventive_items[$counter]['IVA'] = 0;
            $counter++;
        }


        return view('create-preventive')
            ->with('items', $preventive_items)
            ->with('nextOrderId', OrderController::RetrieveNextOrderId());
    }

    public function RenderPreventiveStatisticsPage(Request $request)
    {

        $currentYear = $request->targetYear;
        //Imponibile
        $currentYearTotal = OrderController::GetMonthlyIncomeByYear($currentYear);
        $lastYearTotal = OrderController::GetMonthlyIncomeByYear($currentYear - 1);
        $yearlyIncomeDataPoints = [];
        $yearlyIncomeDataPoints[] = array(
            "name" => "Imponibile " . ($currentYear - 1),
            "data" => [
                $lastYearTotal[1],
                $lastYearTotal[2],
                $lastYearTotal[3],
                $lastYearTotal[4],
                $lastYearTotal[5],
                $lastYearTotal[6],
                $lastYearTotal[7],
                $lastYearTotal[8],
                $lastYearTotal[9],
                $lastYearTotal[10],
                $lastYearTotal[11],
                $lastYearTotal[12],
            ],
        );
        $yearlyIncomeDataPoints[] = array(
            "name" => "Imponibile " . $currentYear,
            "data" => [
                $currentYearTotal[1],
                $currentYearTotal[2],
                $currentYearTotal[3],
                $currentYearTotal[4],
                $currentYearTotal[5],
                $currentYearTotal[6],
                $currentYearTotal[7],
                $currentYearTotal[8],
                $currentYearTotal[9],
                $currentYearTotal[10],
                $currentYearTotal[11],
                $currentYearTotal[12],
            ],
        );

        $currentYearAcceptedPreventiveCount[] = array();
        $currentYearTotalPreventiveCount[] = array();
        for ($month = 1; $month <= 12; $month++) {
            $currentYearAcceptedPreventiveCount[$month] = count(Order::where('accepted_by_customer', 1)->whereYear('acceptance_date', '=', $currentYear)
                ->whereMonth('creation_date', '=', $month)->get());
            $currentYearTotalPreventiveCount[$month] = count(Order::whereYear('creation_date', '=', $currentYear)
                ->whereMonth('creation_date', '=', $month)->get());
        }

        //Preventive count
        $yearlyPreventiveCountDataPoints[] = array(
            "name" => "Preventivi creati",
            "data" => [
                $currentYearTotalPreventiveCount[1],
                $currentYearTotalPreventiveCount[2],
                $currentYearTotalPreventiveCount[3],
                $currentYearTotalPreventiveCount[4],
                $currentYearTotalPreventiveCount[5],
                $currentYearTotalPreventiveCount[6],
                $currentYearTotalPreventiveCount[7],
                $currentYearTotalPreventiveCount[8],
                $currentYearTotalPreventiveCount[9],
                $currentYearTotalPreventiveCount[10],
                $currentYearTotalPreventiveCount[11],
                $currentYearTotalPreventiveCount[12],
            ],
        );

        $yearlyPreventiveCountDataPoints[] = array(
            "name" => "Preventivi accettati",
            "data" => [
                $currentYearAcceptedPreventiveCount[1],
                $currentYearAcceptedPreventiveCount[2],
                $currentYearAcceptedPreventiveCount[3],
                $currentYearAcceptedPreventiveCount[4],
                $currentYearAcceptedPreventiveCount[5],
                $currentYearAcceptedPreventiveCount[6],
                $currentYearAcceptedPreventiveCount[7],
                $currentYearAcceptedPreventiveCount[8],
                $currentYearAcceptedPreventiveCount[9],
                $currentYearAcceptedPreventiveCount[10],
                $currentYearAcceptedPreventiveCount[11],
                $currentYearAcceptedPreventiveCount[12],
            ],
        );
        //Margine
        $currentYearPercentageMargin[] = array();
        $currentYearAbsoluteMargin[] = array();
        for ($month = 1; $month <= 12; $month++) {
            $currentYearAbsoluteMargin[$month] = 0;
            $currentYearPercentageMargin[$month] = 0;
            $monthOrders = Order::where('accepted_by_customer', 1)->whereYear('acceptance_date', '=', $currentYear)
                ->whereMonth('acceptance_date', '=', $month)->get();
            if (count($monthOrders) == 0) {
                $currentYearAbsoluteMargin[$month - 1] = 0;
                $currentYearPercentageMargin[$month - 1] = 0;
            } else {
                $totalMonthMargin = 0;
                $totalMonthTaxable = 0;
                foreach ($monthOrders as $currentOrder) {
                    $admin_commission_perc = Admin::find($currentOrder->creator_id)->selling_commission_perc;
                    $totalMonthTaxable += $currentOrder->total;
                    $orderInfo = OrderController::GetOrderInfo($currentOrder, $admin_commission_perc);
                    $totalMonthMargin += $orderInfo['abs_margin'];
                }
                $currentYearAbsoluteMargin[$month - 1] = round($totalMonthMargin, 2);
                $currentYearPercentageMargin[$month - 1] = round(($totalMonthMargin / $totalMonthTaxable) * 100, 2);
            }
        }


        $year_list = array();
        $orders = Order::all();
        foreach ($orders as $order) {
            $year = date('Y', strtotime($order->creation_date));
            if (!in_array($year, $year_list)) {
                array_push($year_list, $year);
            }
        }

        return view('order-stats', [
            "yearly_income_data" => json_encode($yearlyIncomeDataPoints),
            "preventive_count_data" => json_encode($yearlyPreventiveCountDataPoints),
            "absolute_current_year_margin" => json_encode($currentYearAbsoluteMargin),
            "relative_current_year_margin" => json_encode($currentYearPercentageMargin),
            'targetYear' => $currentYear,
            'year_list' => $year_list
        ]);
    }

    public static function RetrieveNextOrderId()
    {
        $statement = DB::select("SHOW TABLE STATUS LIKE 'orders'");
        return $statement[0]->Auto_increment . "/A";
    }

    public static function GetMonthlyIncomeByYear($year)
    {
        $currentYearTotal[] = array();
        for ($month = 1; $month <= 12; $month++) {
            $orders = Order::where('accepted_by_customer', 1)->whereYear('acceptance_date', '=', $year)
                ->whereMonth('acceptance_date', '=', $month)->get();
            $monthTotal = 0;
            foreach ($orders as $order) {
                $monthTotal += $order->total;
            }
            $currentYearTotal[$month] = $monthTotal;
        }
        return $currentYearTotal;
    }

    public static function GetOrderInfo($order, $admin_commission_perc)
    {
        Log::debug('Getting info on order: ' . $order->preventive_number);
        $abs_margin = -$order->discount;
        $abs_expenses = 0;
        $abs_commission = 0;
        $total = $order->total;
        $items = json_decode($order->item_list, true);
        foreach ($items as $item) {

            if ($item['id'] == -2) {
                Log::debug('Empty row - skipping.');
                continue;
            }

            Log::debug('Calculating margins on item: ' . $item['id']);

            //Check if UM, qty, discount and IVA are numeric fields or not

            //UM
            $itemUM = $item['UM'];
            if (!is_numeric($itemUM)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemUM)) {
                    Log::debug('UM contains letters: skipping row. (' . $itemUM . ')');
                    continue;
                }
                if (strpos($itemUM, ',') != false) {
                    $itemUM = str_replace(',', '.', $itemUM);
                    Log::debug('Changed number from ' . $item['UM'] . 'to ' . $itemUM . ')');
                }
            }
            //qty
            $itemQty = $item['qty'];
            if (!is_numeric($itemQty)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemQty)) {
                    Log::debug('Qty contains letters: skipping row. (' . $itemQty . ')');
                    continue;
                }
                if (strpos($itemQty, ',') != false) {
                    $itemQty = str_replace(',', '.', $itemQty);
                    Log::debug('Changed number from ' . $item['qty'] . 'to ' . $itemQty . ')');
                }
            }
            //discount
            $itemDiscount = $item['discount'];
            if (!is_numeric($itemDiscount)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemDiscount)) {
                    Log::debug('Discount contains letters: skipping row. (' . $itemDiscount . ')');
                    continue;
                }
                if (strpos($itemDiscount, ',') != false) {
                    $itemDiscount = str_replace(',', '.', $itemDiscount);
                    Log::debug('Changed number from ' . $item['discount'] . 'to ' . $itemDiscount . ')');
                }
            }
            //IVA
            $itemIVA = $item['IVA'];
            if (!is_numeric($itemIVA)) {
                //Comma to dot?
                if (preg_match("/^[a-zA-Z]+$/", $itemIVA)) {
                    Log::debug('IVA contains letters: skipping row. (' . $itemIVA . ')');
                    continue;
                }
                if (strpos($itemIVA, ',') != false) {
                    $itemIVA = str_replace(',', '.', $itemIVA);
                    Log::debug('Changed number from ' . $item['IVA'] . 'to ' . $itemIVA . ')');
                }
            }

            $current_expense = 0.0;
            $current_commission = 0.0;
            $current_gain = 0.0;

            $current_gain = $itemUM * $itemQty;
            $current_commission = (($current_gain - ($current_gain / 100 * $itemDiscount)) / 100) * ($admin_commission_perc + ((100 - $itemDiscount) / 2));
            $abs_commission += $current_commission;
            $abs_expenses += $current_expense;
            $abs_margin += $current_gain - $current_expense - $current_commission;
            Log::debug('Current commission: ' . $abs_commission);
            Log::debug('Current expenses: ' . $abs_expenses);
            Log::debug('Current margin: ' . $abs_margin);
        }

        $orderInfo = array();
        $orderInfo['abs_margin'] = $abs_margin;
        $orderInfo['abs_expenses'] = $abs_expenses;
        $orderInfo['abs_commission'] = $abs_commission;

        Log::debug('Final commission: ' . $abs_commission);
        Log::debug('Final expenses: ' . $abs_expenses);
        Log::debug('Final margin: ' . $abs_margin);

        if ($order->total > 0) {
            $orderInfo['perc_margin'] = ($abs_margin / ($order->total)) * 100;
            $orderInfo['perc_expenses'] = ($abs_expenses / ($order->total)) * 100;
            $orderInfo['perc_taxes'] = ($order->taxes / ($order->total)) * 100;
            $orderInfo['perc_commission'] = ($abs_commission / ($order->total) * 100);
        } else {
            $orderInfo['perc_margin'] = 0;
            $orderInfo['perc_expenses'] = 0;
            $orderInfo['perc_taxes'] = 0;
            $orderInfo['perc_commission'] = 0;
        }
        if (($order->taxable + $order->discount) > 0) {
            $orderInfo['perc_discount'] = ($order->discount / ($order->taxable + $order->discount)) * 100;
        } else {
            $orderInfo['perc_discount'] = 0;
        }

        return $orderInfo;
    }

    public function duplicatePreventive(Request $request)
    {

        $targetOrder = Order::find($request->id);


        if ($targetOrder) {
            Log::debug('Duplicating order: ' . $targetOrder);

            $newOrder = new Order();

            $newOrder->creator_id = Session::get('admin_id', 3);
            $newOrder->customer_id = $targetOrder->customer_id;
            $newOrder->reference_text = $targetOrder->reference_text;
            $newOrder->preventive_number = OrderController::RetrieveNextOrderId();
            $newOrder->creation_date = $targetOrder->creation_date;
            $newOrder->validity_end_date = $targetOrder->validity_end_date;
            $newOrder->item_list = $targetOrder->item_list;
            $newOrder->taxable = $targetOrder->taxable;
            $newOrder->taxes = $targetOrder->taxes;
            $newOrder->discount = $targetOrder->discount;
            $newOrder->total = $targetOrder->total;
            $newOrder->created_at = date('Y-m-d H:i:s');
            $newOrder->accepted_by_customer = 0;

            Log::debug('New order: ' . $newOrder);
            if ($newOrder->save()) {
                Log::debug('Saved.');
                return redirect()->route('edit-preventive', $newOrder->id);
            } else {
                Log::debug('Could not save new order. Going back to index.');
                return redirect()->route('orders-table')->with('message', 'Non è stato possibile salvare il duplicato.');
            }
        } else {
            Log::debug('The order that was supposed to be duplicated was not found. Going back to index.');
            return redirect()->route('orders-table')->with('message', 'Il preventivo non è stato duplicato perché non è stato trovato.');
        }
    }
}
