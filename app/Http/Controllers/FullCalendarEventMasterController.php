<?php

namespace App\Http\Controllers;

use App\Mail\CalendarEmail;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;
use Symfony\Component\Console\Input\Input;

class FullCalendarEventMasterController extends Controller
{

    public function index()
    {
        if (request()->ajax()) {

            $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
            $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');

            $data = Event::whereDate('start', '>=', $start)->whereDate('end',   '<=', $end)->get(['id', 'title', 'color', 'start', 'end']);
            return Response::json($data);
        }
        return view('fullcalender');
    }



    public function create(Request $request)
    {
        $newEvent = new Event();
        $newEvent->title = $request->title;
        $newEvent->start = $request->start;
        $newEvent->end = $request->end;
        $newEvent->color = $request->color;
        $newEvent->save();

        return Response::json($newEvent->id);
    }


    public function update(Request $request)
    {
        $where = array('id' => $request->id);
        $updateArr = ['title' => $request->title, 'start' => $request->start, 'end' => $request->end];
        $event  = Event::where($where)->update($updateArr);

        return Response::json($event);
    }


    public function destroy(Request $request)
    {
+
        $event = Event::where('id', $request->id)->delete();

        return Response::json($event);
    }

    public function SendEmailNotification(Request $request)
    {
        if (request()->ajax()) {
            $email = $request->email;

            $mailData = [
                'title' => $request->message,
                'start' => $request->start_time,
                'end' => $request->end_time,
            ];

            Mail::to($email)->send(new CalendarEmail($mailData));

            return response()->json([
                'success' => 'Email has been sent.'
            ], 200);

        }
    }

    public function sendEmail()
    {
        $email = 'positronx@gmail.com';

        $mailData = [
            'title' => 'Demo Email',
            'url' => 'https://www.positronx.io'
        ];

        Mail::to($email)->send(new CalendarEmail($mailData));

        return response()->json([
            'message' => 'Email has been sent.'
        ], Response::HTTP_OK);
    }

    public static function random_color()
    {
        return "#" .
            FullCalendarEventMasterController::random_color_part() .
            FullCalendarEventMasterController::random_color_part() .
            FullCalendarEventMasterController::random_color_part();
    }

    public static function random_color_part()
    {
        return str_pad(dechex(mt_rand(75, 240)), 2, '0', STR_PAD_LEFT);
    }
}
