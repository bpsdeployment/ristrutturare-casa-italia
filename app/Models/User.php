<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property integer $id
 * @property string $mail
 * @property string $password_hash
 * @property string $name
 * @property string $surname
 * @property string $address
 * @property string $city
 * @property string $region
 * @property string $CAP
 * @property string $phone
 * @property string $additional_info
 * @property integer $external_submission_id
 * @property float  $total_spent
 * @property string $email_verified_at
 * @property string $remember_token
 * @property string $reference_admin_id
 * @property string $created_at
 * @property string $updated_at
 */


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mail', 'name', 'surname', 'address', 'city','region', 'CAP','phone','external_submission_id', 'additional_info', 'total_spent', 'email_verified_at','reference_admin_id', 'remember_token', 'created_at', 'updated_at'];

    public function routeNotificationForMail($notification)
    {
        // Return email address only
        return $this->mail;
    }

    public function getAuthPassword()
    {
        return $this->password_hash;
    }
}
