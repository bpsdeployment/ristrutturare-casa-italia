<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $order_id
 * @property string $mq
 * @property string $n_bathroom
 * @property string $kitchen
 * @property string $house_installations
 * @property string $conditioner
 * @property string $countertop
 * @property string $created_at
 * @property string $updated_at
 */
class OrderAdditionalInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_additional_info';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['order_id', 'mq', 'n_bathroom', 'kitchen', 'house_installations', 'conditioner', 'countertop', 'created_at', 'updated_at'];
}
