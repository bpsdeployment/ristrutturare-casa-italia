<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $mail
 * @property string $password_hash
 * @property string $name
 * @property string $surname
 * @property string $invitation_token
 * @property boolean $edit_products
 * @property boolean $edit_categories
 * @property boolean $edit_first_page_products
 * @property boolean $c_products_auth
 * @property boolean $r_products_auth
 * @property boolean $u_products_auth
 * @property boolean $d_products_auth
 * @property boolean $c_categories_auth
 * @property boolean $r_categories_auth
 * @property boolean $u_categories_auth
 * @property boolean $d_categories_auth
 * @property boolean $u_first_page_auth
 * @property string $created_at
 * @property string $updated_at
 */
class Admin extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mail', 'password_hash', 'name', 'surname', 
    'invitation_token','selling_commission_perc', 
    'c_products_auth', 'r_products_auth', 'u_products_auth', 'd_products_auth', 
    'c_categories_auth', 'r_categories_auth', 'u_categories_auth', 'd_categories_auth', 
    'edit_products', 'edit_first_page_products','edit_categories','edit_first_page_products',
    'u_first_page_auth', 'created_at', 'updated_at'];
}
