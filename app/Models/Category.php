<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $mail
 * @property string $thumbnail_path
 * @property int $product_count
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'thumbnail_path', 'description', 'product_count', 'is_first_page', 'created_at', 'updated_at'];

}
