<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $user_id
 * @property boolean $edit_products
 * @property boolean $edit_categories
 * @property boolean $reply_contact_requests
 * @property string $created_at
 * @property string $updated_at
 */
class Authorization extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'edit_products', 'edit_categories', 'reply_contact_requests', 'created_at', 'updated_at'];

}
