<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $creator_id
 * @property string $customer_id
 * @property string $reference_text
 * @property string $preventive_number
 * @property string $creation_date
 * @property string $validity_end_date
 * @property float $taxable
 * @property float $taxes
 * @property float $discount
 * @property float $total
 * @property boolean $accepted_by_customer
 * @property string $acceptance_date
 * @property string $item_list
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['creator_id', 'customer_id', 'reference_text','preventive_number', 'creation_date', 'validity_end_date', 'taxable', 'taxes', 'discount','total','accepted_by_customer','acceptance_date', 'item_list', 'created_at', 'updated_at'];


}
