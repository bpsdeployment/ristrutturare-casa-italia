<?php

namespace App\Models;

use Gloudemans\Shoppingcart\CanBeBought;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use willvincent\Rateable\Rateable;

/**
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $description
 * @property string $photos_folder
 * @property float $buy_price
 * @property float $listing_price
 * @property float $weight
 * @property float $dimensions_x
 * @property float $dimensions_y
 * @property float $dimensions_z
 * @property string $available_colors
 * @property int $need_installation
 * @property string $additional_infos
 * @property int $max_discount
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Rateable;
    use CanBeBought;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'type', 'description', 'photos_folder', 'buy_price', 'listing_price', 'need_installation', 'measure_unit_id','material_type_id', 'weight', 'dimensions_x', 'dimensions_y', 'dimensions_z', 'available_colors', 'additional_infos', 'max_discount', 'is_first_page', 'created_at', 'updated_at'];

}
