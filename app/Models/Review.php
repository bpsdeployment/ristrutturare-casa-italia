<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $reviewer_name
 * @property string $reviewer_profile_photo_path
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 */
class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'reviewer_name', 'reviewer_profile_photo_path', 'text', 'created_at', 'updated_at'];

}
