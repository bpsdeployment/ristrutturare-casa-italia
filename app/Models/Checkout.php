<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $user_id
 * @property string $item_list
 * @property float $total_price
 * @property string $delivery_address
 * @property string $billing_address
 * @property string $phone_number
 * @property boolean $order_completed
 * @property string $created_at
 * @property string $updated_at
 */
class Checkout extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'item_list', 'total_price', 'delivery_address', 'billing_address', 'phone_number', 'order_completed', 'created_at', 'updated_at'];
}
