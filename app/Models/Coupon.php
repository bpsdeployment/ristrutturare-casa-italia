<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property int $n_uses
 * @property string $due_date
 * @property int $discount_value
 * @property boolean $free_shipping
 * @property string $code
 * @property string $created_at
 * @property string $updated_at
 */
class Coupon extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'n_uses', 'due_date', 'discount_value', 'free_shipping', 'code', 'created_at', 'updated_at'];

}
