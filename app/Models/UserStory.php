<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $user_id
 * @property string $type
 * @property string $note
 * @property string $contact_datetime
 * @property string $created_at
 * @property string $updated_at
 */
class UserStory extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'type', 'note', 'contact_datetime', 'created_at', 'updated_at'];
}
