<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('type');
            $table->text('description');
            $table->string('photos_folder');
            $table->float('buy_price');
            $table->float('listing_price');
            $table->float('weight')->nullable();
            $table->float('dimensions_x')->nullable();
            $table->float('dimensions_y')->nullable();
            $table->float('dimensions_z')->nullable();
            $table->tinyInteger('need_installation');
            $table->integer('measure_unit_id')->nullable();
            $table->integer('material_type_id')->nullable();
            $table->text('additional_infos')->nullable();
            $table->integer('max_discount', false, true)->default(0);
            $table->integer('units_sold')->default(0);
            $table->boolean('is_first_page')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
