<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('creator_id');
            $table->string('customer_id');
            $table->string('reference_text');
            $table->string('preventive_number');
            $table->date('creation_date');
            $table->date('validity_end_date');
            $table->float('taxable');
            $table->float('taxes');
            $table->float('discount');
            $table->float('total');
            $table->tinyInteger('accepted_by_customer')->default(0);
            $table->dateTime('acceptance_date')->nullable();
            $table->json('item_list');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
