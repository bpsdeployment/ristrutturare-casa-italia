<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFrontpageInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frontpage_infos', function (Blueprint $table) {
            $table->id();
            $table->text('title_homepage')->nullable();
            $table->text('subtitle_homepage')->nullable();
            $table->text('homepage_text')->nullable();
            $table->text('footer_text')->nullable();
            $table->text('support_mail')->nullable();
            $table->text('support_phone')->nullable();
            $table->text('privacy_info_link')->nullable();
            $table->text('t_t_link')->nullable();
            $table->text('cookie_policy_link')->nullable();
            $table->timestamps();
        });
        DB::table('frontpage_infos')->insert(
            ['title_homepage' => 'Titolo',
            'subtitle_homepage' => 'Sottotitolo',
            'homepage_text' => 'testo pagina',
            'footer_text' => 'testo footer',
            'support_mail' => 'support@mail.com',
            'support_phone' => '123456789',
            'privacy_info_link' => 'www.google.com',
            't_t_link' => 'www.google.com',
            'cookie_policy_link' => 'www.google.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frontpage_infos');
    }
}
