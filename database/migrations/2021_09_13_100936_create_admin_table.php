<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('mail')->unique();
            $table->string('password_hash')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('invitation_token');
            $table->integer('selling_commission_perc');
            $table->boolean('edit_products')->default(false);
            $table->boolean('edit_categories')->default(false);
            $table->boolean('edit_first_page_products')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
