<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('mail')->unique()->nullable();
            $table->string('name')->default("")->nullable();
            $table->string('surname')->default("")->nullable();
            $table->string('address_r1')->nullable();
            $table->string('address_r2')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('CAP')->nullable();
            $table->string('phone')->nullable();
            $table->json('additional_info')->nullable();
            $table->integer('external_submission_id')->nullable();
            $table->string('reference_admin_id')->nullable();
            $table->float('total_spent')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
