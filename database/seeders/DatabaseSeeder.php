<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $preventive_number = 4343;

        for ($i = 0; $i < 25; $i++) {

            $faker = Faker::create();
            //Create a bunch of amdins
            /*
            DB::table('admins')->insert([
            'name' => $faker->firstName(),
            'surname' => $faker->lastName(),
            'invitation_token' => Str::random(20),
            'mail' => $faker->unique()->safeEmail
            ]);   # code...
            }
            $admins = DB::table('admins')->get();
            $admin_id = 0;
            $counter = 0;

            while (true) {
                $admin_id = random_int(0, 50);
                foreach ($admins as $admin) {
                    if ($admin_id == $admin->id) {
                        echo 'found';
                        break;
                    }
                }
                if ($counter >50) break;
                $counter++;
            }

            //Create a bunch of clients
            DB::table('users')->insert([
                'name' => $faker->firstName(),
                'surname' => $faker->lastName(),
                'reference_admin_id' => $admin_id,
                'mail' => $faker->unique()->safeEmail(),
                'address_r1' => $faker->address(),
            ]);
            */
            //Create a bunch of orders 

            $admins = DB::table('admins')->get();
            $rand = random_int(0, $admins->count() - 1);
            $admin = $admins[$rand];
            echo 'creator_id: ' . $admin->id . "\n";

            $users = DB::table('users')->get();
            $rand = random_int(0, $users->count() - 1);
            $user = $users[$rand];
            echo 'customer_id: ' . $user->id . "\n";

            $products = DB::table('products')->get();

            $item_list = '[';
            $num_prod = random_int(1, 5);
            $taxable = 0;

            for ($i = 0; $i < $num_prod; $i++) {

                $rand = random_int(0, $products->count() - 1);
                $product = $products[$rand];

                $qnt = random_int(1, 10);
                $taxable += $qnt * $product->listing_price;
                echo 'Taxable: '. $taxable . "\n";
                $total = $taxable;

                $item_list .= '{"id":"' . $product->id . '","description":"' . $product->description . '","qty":"' . $qnt . '","UM":"' . $product->listing_price . '","discount":"0","IVA":"0"}';
                if ($i < $num_prod - 1) $item_list .= ' , ';
            }
            
            $item_list .= ']';

            echo $item_list . "\n";

            $random_year = random_int(2008, 2022);
            $random_month = random_int(1, 12);
            $random_day = random_int(1, 28);
            echo 'Creation date: ' . $random_year . '-' . $random_month . '-' . ($random_day > 10 ? $random_day - 10 : $random_day) . "\n";
            echo 'Scadenza date: ' . date($random_year . '-' . $random_month . '-' . $random_day) . "\n" . "\n";

            $reference_text = Str::random(10);
            
            DB::table('orders')->insert([
                'creator_id' => $admin->id,
                'customer_id' => $user->id,
                'reference_text' => $reference_text,
                'created_at' => date($random_year . '-' . $random_month . '-' . ($random_day > 10 ? $random_day - 10 : $random_day)),
                'creation_date' => date($random_year . '-' . $random_month . '-' . ($random_day > 10 ? $random_day - 10 : $random_day)),
                'validity_end_date' => date($random_year . '-' . $random_month . '-' . $random_day),
                'taxable' => $taxable,
                'taxes' => 0, 'discount' => 0, 'total' => $total,
                'item_list' => $item_list,
                'preventive_number' => $preventive_number.'/A',
                'accepted_by_customer' => 1

            ]);
        
            $preventive_number++;



            /*
            [{"id":"40","description":"OP_AT_ D066502. DIMENSIONI 60X60cm, spessore 9,5 mm. FINITURA GRIGIO CHIARO. Gres Porcellanato digitale smaltato migliorato nella definizione dei dettagli grafici gra
                zie alla tecnologia digitale HD. La societ\u00e0 sta attuando un percorso di crescita sostenibile per una produzione ecologica a risparmio energetico e riciclo. Realizzati interamente in
                 Italia. Specifiche tecniche: Pavimento Matt, Resistenza allo scivolamento R 10\/11, resistenza al gelo, resistenza attacco chimico, resistenza all'abrasione PEI IV. Fondo nat. MADE IN ITA
                 LY","qty":"1","UM":"47.02","discount":"0","IVA":"0"}]


            DB::table('users')->insert([
                'name' => $faker->firstName(),
                'surname' => $faker->lastName(),
                'reference_admin_id' => $admin_id,
                'mail' => $faker->unique()->safeEmail(),
                'address_r1' => $faker->address(),
            ]);*/
        }
    }
}
            /*
        $faker = Faker::create();
        $first_page_item_count = 0;
        $is_first_page = true;
        $category_name_array = array();
        foreach( range(1,4) as $index) {
            $folder_name = "dummycategory_".$index;
            $dirPath = public_path().'/images/categories_thumbnail/' . $folder_name;
            $DBFilePath = "/images/categories_thumbnail/" . $folder_name .".png";
            $targetFilePath = public_path() . "/images/categories_thumbnail/" . $folder_name .".png";
            File::makeDirectory($dirPath, $mode = 0777, true, true);
            $faker->image($dirPath, 500, 510);
            $files = array_values(array_diff(scandir($dirPath), array('.', '..')));
            rename($dirPath . "/" . $files[0], $targetFilePath);
            rmdir($dirPath);
            $category_name = $faker->name();
            array_push($category_name_array, $category_name);
            DB::table('categories')->insert([
                'name' => $category_name,
                'description' => $faker->sentence(20),
                'thumbnail_path' => $DBFilePath,
                'is_first_page' => true,
                'product_count' => 0,
                'created_at' => $faker->date()." ".$faker->time()
            ]);
        }

    	foreach (range(1,10) as $index) {
            $folder_name = "dummyproduct_".$index;
            $path = public_path().'/images/products_images/' . $folder_name;
            File::makeDirectory($path, $mode = 0777, true, true);
            $last_category = $faker->randomElement($category_name_array);
            $faker->image($path, 500, 510);
            DB::table('products')->insert([
                'name' => $faker->name(),
                'type' => $last_category,
                'description' => $faker->sentence(20),
                'photos_folder' => $folder_name,
                'price' => $faker->numberBetween(0,100),
                'weight' => $faker->numberBetween(0,100),
                'dimensions_x' => $faker->numberBetween(0,100),
                'dimensions_y' => $faker->numberBetween(0,100),
                'dimensions_z' => $faker->numberBetween(0,100),
                'need_installation' => $faker->numberBetween(0,1),
                //'available_colors' => $faker->word(),
                'max_discount' => $faker->numberBetween(0,100),
                'units_sold' => $faker->numberBetween(0,1000),
                'is_first_page' => $is_first_page,
                'created_at' => $faker->date()." ".$faker->time()
            ]);
            $first_page_item_count++;
            if($first_page_item_count == 8)
            {
                $is_first_page = false;
            }
            $category = Category::where("name", $last_category)->first();
            $category->product_count++;
            $category->save();
        }*/
