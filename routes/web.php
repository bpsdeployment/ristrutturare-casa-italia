<?php

namespace App\Models;

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//WEB PAGES ROUTES
Route::get('/private-home', function () {
    $categories = Category::where('is_first_page', true)->get();
    $products = Product::where('is_first_page', true)->get();
    return view('index', ['categories' => $categories, 'products' => $products]);
})->middleware('auth.admin')->name('index');

Route::get('/', 'App\Http\Controllers\FirstPageController@ShowHomepage')->middleware('user.customer')->name('landing');
Route::get('/login', 'App\Http\Controllers\FirstPageController@ShowLogin')->middleware('user.customer')->name('login');
Route::get('/cart', function () {return view('cart');})->name('cart');

//CATALOG AND FILTER ROUTES
Route::get('/product-page/{product_id}', 'App\Http\Controllers\ProductController@show')->name('showProduct');
Route::get('/catalog', 'App\Http\Controllers\CatalogController@index')->name('products');
Route::post('/catalog/reset-filters', 'App\Http\Controllers\CatalogController@reset')->name('resetFilters');
Route::get('/catalog/filter/category', 'App\Http\Controllers\CatalogController@filter')->name('filterByCategory');
Route::get('/catalog/search', 'App\Http\Controllers\CatalogController@search')->name('livesearch');
Route::get('/catalog/price_asc', 'App\Http\Controllers\CatalogController@sortBy')->name('asc');
Route::get('/catalog/sort-by-newest', 'App\Http\Controllers\CatalogController@sortByNewest')->name('sortByNewest');
Route::get('/catalog/sort-by-discount', 'App\Http\Controllers\CatalogController@sortByDiscount')->name('sortByDiscount');
Route::get('/catalog/sort-by-priceHTL', 'App\Http\Controllers\CatalogController@sortByPriceHTL')->name('sortByPriceHTL');
Route::get('/catalog/sort-by-priceLTH', 'App\Http\Controllers\CatalogController@sortByPriceLTH')->name('sortByPriceLTH');
Route::get('/catalog/change_page', 'App\Http\Controllers\CatalogController@changePage')->name('change_page');

//ADMIN ROUTES
Route::get('/admin-page', 'App\Http\Controllers\AdminController@RenderAdminPage')->middleware('auth.admin')->name('admin.page');
Route::get('/admin-page/admin-list/fill-admin-table', 'App\Http\Controllers\AdminController@fillTable')->middleware('auth.admin', 'sitemaster')->name('fillAdminTable');
Route::get('/admin-page/admin-list', function(){return view('admin-view');})->middleware('auth.admin', 'sitemaster')->name('admin-view');
Route::get('/admin-page/edit-auth/{id}', 'App\Http\Controllers\AdminController@ShowEditAuthPage')->middleware('auth.admin', 'sitemaster')->name('showEditAuthPage');
Route::get('/admin-page/admin-details/{id}', 'App\Http\Controllers\AdminController@ShowAdminDetails')->middleware('auth.admin', 'sitemaster')->name('show-admin-details');
Route::post('/admin-page/set-commission/{id}', 'App\Http\Controllers\AdminController@SetSellingCommissionPercentage')->middleware('auth.admin', 'sitemaster')->name('update-sell-commission-perc');
Route::get('/admin-page/edit-admin-data/{id}', 'App\Http\Controllers\AdminController@ShowEditAdminDataPage')->middleware('auth.admin', 'sitemaster')->name('edit-admin-data');

Route::get('/admin-page/add-admin', 'App\Http\Controllers\AdminController@AddAdmin')->middleware('auth.admin')->name('add-admin');
Route::post('/admin-page/store-admin', 'App\Http\Controllers\AdminController@StoreNewAdmin')->middleware('auth.admin')->name('store-new-admin');

Route::post('/admin-page/save-admin-data/{id}', 'App\Http\Controllers\AdminController@EditAdminData')->middleware('auth.admin')->name('save-edited-admin-data');
//Delete admin
Route::get('/admin-page/admin-list/delete/{id}', 'App\Http\Controllers\AdminController@deleteAdmin')->middleware('auth.admin', 'sitemaster')->name('delete-admin');




//USER ROUTES
Route::get('/admin-page/user-list/fill-user-table', 'App\Http\Controllers\UserController@fillTable')->middleware('auth.admin')->name('fillUserTable');
Route::get('/admin-page/user-list', function () {return view('user-view');})->middleware('auth.admin')->name('user-view');
Route::get('/admin-page/add-user', 'App\Http\Controllers\UserController@RenderAddUserPage')->middleware('auth.admin')->name('add-customer');
Route::post('/admin-page/store-user', 'App\Http\Controllers\UserController@StoreNewCustomer')->middleware('auth.admin')->name('store-new-customer');
Route::get('/admin-page/edit-user-data/{id}', 'App\Http\Controllers\UserController@ShowEditUserDataPage')->middleware('auth.admin', 'op.has.this.user')->name('edit-user-data');
Route::post('/admin-page/save-user-data/{id}', 'App\Http\Controllers\UserController@EditCustomerData')->middleware('auth.admin', 'op.has.this.user')->name('save-edited-customer-data');
Route::get('/admin-page/details/{id}', 'App\Http\Controllers\UserController@showUserDetails')->middleware('auth.admin', 'op.has.this.user')->name('show-user-details');
Route::get('/admin-page/details/{id}/insert-note', 'App\Http\Controllers\UserController@addUserStory')->middleware('auth.admin', 'op.has.this.user')->name('insert-user-story');
Route::post('/admin-page/details/save-note', 'App\Http\Controllers\UserStoryController@store')->middleware('auth.admin', 'op.has.this.user')->name('save-user-story');
Route::post('/admin-page/store-customer', 'App\Http\Controllers\UserController@store')->middleware('user.customer')->name('store.customer');
Route::get('/admin-page/sync-with-sg', 'App\Http\Controllers\UserController@GetNewUsersFromSiteGround')->middleware('auth.admin', 'sitemaster')->name('retrieve-customers-from-siteground');
//Delete user
Route::get('/admin-page/user-list/delete/{id}', 'App\Http\Controllers\UserController@deleteUser')->middleware('auth.admin', 'sitemaster')->name('delete-user');


//Admin invitation routes
Route::get('/admin-page/invite-admin', 'App\Http\Controllers\AdminController@inviteAdmin')->middleware('auth.admin', 'sitemaster')->name('invite-admin');
Route::post('/admin-page/invite-admin/send-invite', 'App\Http\Controllers\AdminController@sendInvitation')->middleware('auth.admin', 'sitemaster')->name('send-invitation-admin');
Route::get('/admin-invitation/{token}', 'App\Http\Controllers\AdminController@showAdminRegistrationPage')->name('admin-invitation');//->middleware('user.customer')->name('admin-invitation');
Route::post('/admin-invitation/complete-registration', 'App\Http\Controllers\AdminController@completeAdminRegistration')->name('complete-admin-registration');
//Admin manage autorization
Route::get('/admin-page/manage-autorization', 'App\Http\Controllers\AdminController@manageAutorization')->middleware('auth.admin', 'sitemaster')->name('manage-autorization');
Route::get('/admin-page/update-autorization', 'App\Http\Controllers\AdminController@updateAdminOut')->middleware('auth.admin', 'sitemaster')->name('update-admin-out');

//PRODUCT MANAGING ROUTES
Route::get('/admin-page/insert-product', 'App\Http\Controllers\ProductController@DisplayInsertProductPage')->middleware('auth.admin',  'has.product.role')->name('insert-product');
Route::get('/admin-page/select-product', 'App\Http\Controllers\ProductController@selectProductPage')->middleware('auth.admin',  'has.product.role')->name('select-product');
Route::post('/admin-page/insert-product/store', 'App\Http\Controllers\ProductController@store')->middleware('auth.admin', 'has.product.role')->name('storeProduct');
Route::get('/admin-page/insert-service', 'App\Http\Controllers\ProductController@DisplayInsertServicePage')->middleware('auth.admin',  'has.product.role')->name('insert-service');
Route::post('/admin-page/insert-service/store', 'App\Http\Controllers\ProductController@StoreService')->middleware('auth.admin', 'has.product.role')->name('store-service');
Route::get('/admin-page/product-list', function () {return view('productslist');})->middleware('auth.admin',   'has.product.role')->name('products-table');
Route::get('/admin-page/edit-products/fill-table', 'App\Http\Controllers\ProductController@index')->middleware('auth.admin', 'has.product.role')->name('fillTable');
Route::get('/admin-page/edit-product/delete/{id}', 'App\Http\Controllers\ProductController@destroy')->middleware('auth.admin', 'has.product.role')->name('deleteProduct');
Route::get('/admin-page/edit-product/update/{id}', 'App\Http\Controllers\ProductController@showEditProductPage')->middleware('auth.admin', 'has.product.role')->name('showProductEditPage');
Route::post('/admin-page/edit-product/update/confirmation', 'App\Http\Controllers\ProductController@edit')->middleware('auth.admin', 'has.product.role')->name('editProduct');
Route::post('/admin-page/edit-service/update/confirmation', 'App\Http\Controllers\ProductController@EditService')->middleware('auth.admin', 'has.product.role')->name('edit-service');
Route::get('/admin-page/edit-product/update/delete-image/{folder}/{file}', 'App\Http\Controllers\ProductController@deleteProductImage')->middleware('auth.admin', 'has.product.role')->name('deleteImage');
Route::get('/admin-page/edit-product/undo/{id}', 'App\Http\Controllers\ProductController@undoProductDelete')->middleware('auth.admin', 'has.product.role')->name('undoDeleteProduct');
Route::get('/admin-view/measure-units', 'App\Http\Controllers\ProductController@ShowMeasureUnitsPage')->middleware('auth.admin', 'has.product.role')->name('measure-unit-view');
Route::get('/admin-page/measure-units/fill-table', 'App\Http\Controllers\ProductController@FillMeasureUnitTable')->middleware('auth.admin', 'has.product.role')->name('fill-measure-unit-table');
Route::post('/admin-page/measure-units/store-unit', 'App\Http\Controllers\ProductController@StoreMeasureUnit')->middleware('auth.admin', 'has.product.role')->name('store-measure-unit');
Route::get('/admin-page/measure-units/delete-unit/{id}', 'App\Http\Controllers\ProductController@DeleteMeasureUnit')->middleware('auth.admin', 'has.product.role')->name('delete-measure-unit');
Route::get('/admin-view/material-types', 'App\Http\Controllers\ProductController@ShowMaterialTypesPage')->middleware('auth.admin', 'has.product.role')->name('material-types-view');
Route::post('/admin-page/material-types/store-material-type', 'App\Http\Controllers\ProductController@StoreMaterialTypes')->middleware('auth.admin', 'has.product.role')->name('store-material-types');
Route::get('/admin-page/material-types/fill-table', 'App\Http\Controllers\ProductController@FillMaterialTypesTable')->middleware('auth.admin', 'has.product.role')->name('fill-material-types-table');
Route::get('/admin-page/material-types/delete-type/{id}', 'App\Http\Controllers\ProductController@DeleteMaterialType')->middleware('auth.admin', 'has.product.role')->name('delete-material-type');

Route::post('/admin-page/bulk-edit/listing-price', 'App\Http\Controllers\ProductController@bulkProductListingPriceEdit')->middleware('auth.admin', 'has.product.role')->name('bulk-edit-product-price');

Route::get('/admin-page/change-price-multiple-product', 'App\Http\Controllers\ProductController@showPriceChangeMultiplePage')->middleware('auth.admin', 'has.product.role')->name('change-price-multiple');
//CATEGORIES MANAGING ROUTES
//Categories table view
Route::get('/admin-page/categories-list', function () {return view('categorieslist');})->middleware('auth.admin', 'has.category.role')->name('categories-table');
Route::get('/admin-page/product-list/fill-table', 'App\Http\Controllers\CategoryController@index')->middleware('auth.admin', 'has.category.role')->name('fillTableCategories');
Route::get('/admin-page/insert-category', function () {return view('insertcategory');})->middleware('auth.admin', 'has.category.role')->name('insert-category');
Route::get('/admin-page/edit-categories/update/{id}', 'App\Http\Controllers\CategoryController@showCategoryEditPage')->middleware('auth.admin', 'has.category.role')->name('showCategoryEditPage');
Route::get('/admin-page/edit-categories/delete/{id}', 'App\Http\Controllers\CategoryController@destroy')->middleware('auth.admin', 'has.category.role')->name('deleteCategory');
Route::post('/admin-page/edit-categories/update/confirmation', 'App\Http\Controllers\CategoryController@edit')->middleware('auth.admin', 'has.category.role')->name('editCategory');
Route::post('/admin-page/insert-category/store', 'App\Http\Controllers\CategoryController@store')->middleware('auth.admin', 'has.category.role')->name('storeCategory');

//ORDER MANAGING ROUTES
Route::get('/admin-page/order-list', function () {return view('orderlist');})->middleware('auth.admin')->name('orders-table');
Route::get('/admin-page/order-list/fill-table', 'App\Http\Controllers\OrderController@index')->middleware('auth.admin')->name('fillTableOrders');
Route::get('/admin-page/order-details/{id}', 'App\Http\Controllers\OrderController@showDetailPageAdmin')->middleware('auth.admin', 'op.has.this.order')->name('manage.order.admin');
Route::get('/admin-page/insert-order', 'App\Http\Controllers\OrderController@ShowInsertPreventivePage')->middleware('auth.admin')->name('insert-preventive');
Route::get('/admin-page/edit-order/{id}', 'App\Http\Controllers\OrderController@ShowEditPreventivePage')->middleware('auth.admin', 'op.has.this.order')->name('edit-preventive');
Route::post('/admin-page/edit-order/update/{id}', 'App\Http\Controllers\OrderController@UpdatePreventive')->middleware('auth.admin', 'op.has.this.order')->name('update-preventive');
Route::get('/admin-page/set-order-accepted/{id}', 'App\Http\Controllers\OrderController@SetOrderAccepted')->middleware('auth.admin', 'op.has.this.order')->name('accept-preventive');
Route::get('/admin-page/insert-order/autocomplete-search', 'App\Http\Controllers\OrderController@SelectLiveSearch')->middleware('auth.admin');
Route::get('/admin-page/insert-order/productname-search', 'App\Http\Controllers\OrderController@SelectProductNameLiveSearch')->middleware('auth.admin');
Route::post('/admin-page/insert-order/store-preventive', 'App\Http\Controllers\OrderController@StorePreventive')->middleware('auth.admin')->name('store-preventive');
Route::get('/admin-page/download-preventive/{id}', 'App\Http\Controllers\OrderController@DownloadPreventive')->middleware('auth.admin', 'op.has.this.order')->name('download-preventive');
Route::get('/admin-page/orders-stats', 'App\Http\Controllers\OrderController@RenderPreventiveStatisticsPage')->middleware('auth.admin', 'sitemaster')->name('preventive-charts');
//Open print dialog with current preventive
Route::get('/admin-page/print-preventive/{id}', 'App\Http\Controllers\OrderController@PrintPreventive')->name('print-preventive');
//Add user from modal in create-preventive view
Route::post('/admin-page/save-new-user-from-preventive', 'App\Http\Controllers\UserController@storeNewUserFromPreventiveModal')->middleware('auth.admin')->name('save-new-user-from-preventive');
//Duplicate preventive
Route::get('/admin-page/duplicate-preventive/{id}', 'App\Http\Controllers\OrderController@duplicatePreventive')->middleware('auth.admin')->middleware('auth.admin', 'op.has.this.order')->name('duplicate-preventive');




//LOGIN AND LOGOUT ROUTES
Route::post('/authenticate', 'App\Http\Controllers\AdminController@authenticate')->middleware('user.customer')->name('authenticate');
Route::get('logout', 'App\Http\Controllers\AdminController@logout')->middleware('auth.admin')->name('logout');

//PASSWORD RESET ROUTES
//Route used to get the mail of user who lost password
Route::get('/forgot-password', function () {return view('forgot-password');})->middleware('guest')->name('password.request');
//Route used to send the password reset link
Route::post('/reset-link', 'App\Http\Controllers\PasswordResetController@submitForgetPasswordForm')->middleware('guest')->name('password.email');
Route::get('/reset-password/{token}', function ($token) {return view('reset-password', ['token' => $token]);})->middleware('guest', 'token.exists')->name('password.reset');
Route::post('/reset-password-action', 'App\Http\Controllers\PasswordResetController@passwordReset')->middleware('guest')->name('password.reset.action');

//CARTS ROUTES
//ADD CART
Route::post('/add-cart', 'App\Http\Controllers\CartController@store')->middleware('auth.admin')->name('addToCart');
Route::get('/empty-cart', 'App\Http\Controllers\CartController@destroy')->middleware('auth.admin')->name('emptyCart');
Route::post('/delete-item', 'App\Http\Controllers\CartController@deleteItem')->middleware('auth.admin')->name('deleteItem');
Route::post('/update-qty', 'App\Http\Controllers\CartController@UpdateQuantity')->middleware('auth.admin')->name('UpdateQty');
Route::post('/cart/fill-preventive', 'App\Http\Controllers\OrderController@FillPreventiveFromCart')->middleware('auth.admin')->name('fill-preventive-rows');
//EDIT FIRST PAGE ROUTES
//TODO has first page role
Route::get('/admin-page/edit-first-page', 'App\Http\Controllers\FirstPageController@index')->middleware('auth.admin')->name('editFirstPage');
Route::post('/admin-page/swap-categories', 'App\Http\Controllers\FirstPageController@swapCategories')->middleware('auth.admin')->name('showNewFirstPageCategory');
Route::post('/admin-page/swap-products', 'App\Http\Controllers\FirstPageController@swapProducts')->middleware('auth.admin')->name('showNewFirstPageProducts');
Route::post('/admin-page/update-firstpage', 'App\Http\Controllers\FirstPageController@UpdateFirstPageText')->middleware('auth.admin')->name('save-homepage-info');

//Calendar
//fullcalender
Route::get('/admin-page/calendar','App\Http\Controllers\FullCalendarEventMasterController@index')->middleware('auth.admin')->name('calendar-page');
Route::post('/fullcalendareventmaster/create','App\Http\Controllers\FullCalendarEventMasterController@create')->middleware('auth.admin');
Route::post('/fullcalendareventmaster/update','App\Http\Controllers\FullCalendarEventMasterController@update')->middleware('auth.admin');
Route::post('/fullcalendareventmaster/delete','App\Http\Controllers\FullCalendarEventMasterController@destroy')->middleware('auth.admin');
Route::post('/fullcalendareventmaster/send','App\Http\Controllers\FullCalendarEventMasterController@SendEmailNotification')->middleware('auth.admin')->name('send-notification-calendar');
